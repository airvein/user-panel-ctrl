#include "inc/os_tasks.h"
#include "cmsis_os.h"
#include "fatfs.h"
#include "lwip.h"
#include "app_touchgfx.h"
#include "communication/interface/hangar/inc/hangar_communication.h"
#include "logic/hangar_mqtt_req/hangar_timestamp/inc/hangar_req_timestamp.h"
#include "procedures/inc/app_procedure_ctrl.h"
#include "commands/inc/app_mqtt_commands.h"
#include "debug/log_modules/log_modules.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_TASK_INITIALIZING(task_name) \
    (log_info_m(log_controller, "OS [ %s-TASK ] initializing...", task_name))

#define LOG_TASK_INITIALIZED(task_name) \
    (log_info_m(log_controller, "OS [ %s-TASK ] initiallized!", task_name))

#define ASSERT_TASK(task_handle, task_name) \
    do{\
       ASSERT_LOG_ERROR_M(task_handle==NULL, log_controller, \
        "OS_ERR [ %s TASK is NULL]", task_name);\
        if(task_handle == NULL)\
        {\
            while(1);\
        }\
    }while(0);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static osThreadId gui_task_handle;
static osThreadId hangar_comm_task_handle;
static osThreadId procedure_task_handle;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void gui_task_function(void const *arg);
static void hangar_communication_task_function(void const *arg);
static void cmd_procedure_task_function(void const *arg);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void os_tasks_start(void)
{
    MX_LWIP_Init();
    hangar_communication_init();
    app_procedure_ctrl_init();
    app_mqtt_commands_init();

   /* gui task init */
    osThreadDef(gui_task, gui_task_function, osPriorityNormal, 0, 4096);
    gui_task_handle = osThreadCreate(osThread(gui_task), NULL);
    ASSERT_TASK(gui_task_handle, "GUI");

   /* communication task init */
   osThreadDef(hangar_comm_task, hangar_communication_task_function,
               osPriorityNormal, 0, 1024);
   hangar_comm_task_handle = osThreadCreate(osThread(hangar_comm_task), NULL);
   ASSERT_TASK(hangar_comm_task_handle, "HANGAR-COMM");

    /* procedure control task init */
    osThreadDef(ss, cmd_procedure_task_function,
                 osPriorityNormal, 0, (4096));
    procedure_task_handle = osThreadCreate(osThread(ss), NULL);
    ASSERT_TASK(procedure_task_handle, "PROCEDURE");

    /* start os task scheduler */
    osKernelStart();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void gui_task_function(void const *arg)
{
    LOG_TASK_INITIALIZING("GUI");

    MX_TouchGFX_Process();

    while(1)
    {
        osDelay(10);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void hangar_communication_task_function(void const *arg)
{
    LOG_TASK_INITIALIZED("HANGAR-COMM");
    while(1)
    {
        hangar_communication_subscribe_all_msg();
        hangar_req_timestamp_send_init_update();
        osDelay(100);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void cmd_procedure_task_function(void const *arg)
{
    LOG_TASK_INITIALIZED("PROCEDURE");
    while(1)
    {
        app_mqtt_commands_handler();
        app_procedure_ctrl_handler();
        osDelay(10);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
