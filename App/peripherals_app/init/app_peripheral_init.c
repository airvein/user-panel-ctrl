#include "../../../App/peripherals_app/init/inc/app_peripheral_init.h"

#include "../../../App/settings_app/general/settings_app_plc_io.h"
#include "peripherals_core/plc_module/inc/plc_module.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define GET_PLC_OUT(out_n)\
    do{ \
            if(ev_plc_module == NULL)\
            {\
                return 0;\
            }\
            return plc_module_get_output_state(ev_plc_module, out_n);\
    }while(0);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
