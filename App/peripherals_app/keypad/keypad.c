#include "inc/keypad.h"
#include "inc/keypad_config.h"
#include "stm32f7xx_hal.h"
#include "debug/log_modules/log_modules.h"
#include <stdlib.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define	_KEYPAD_DELAY(x)			HAL_Delay(x)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static keypad_t keypad;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void keypad_init(void)
{
	GPIO_InitTypeDef gpio;

	keypad.column_size = sizeof(_KEYPAD_COLUMN_GPIO_PIN)/2;
	keypad.row_size = sizeof(_KEYPAD_ROW_GPIO_PIN)/2;

	for(uint8_t	i = 0 ; i < keypad.column_size ; i++)
	{
		gpio.Mode = GPIO_MODE_OUTPUT_PP;
		gpio.Pull = GPIO_NOPULL;
		gpio.Speed = GPIO_SPEED_FREQ_LOW;
		gpio.Pin = _KEYPAD_COLUMN_GPIO_PIN[i];
		HAL_GPIO_Init((GPIO_TypeDef*)_KEYPAD_COLUMN_GPIO_PORT[i],&gpio);

		HAL_GPIO_WritePin((GPIO_TypeDef*)_KEYPAD_COLUMN_GPIO_PORT[i],
						   _KEYPAD_COLUMN_GPIO_PIN[i],GPIO_PIN_SET);
	}

	for(uint8_t	i = 0 ; i < keypad.row_size ; i++)
	{
		gpio.Mode = GPIO_MODE_INPUT;
		gpio.Pull = GPIO_PULLUP;
		gpio.Speed = GPIO_SPEED_FREQ_LOW;
		gpio.Pin = _KEYPAD_ROW_GPIO_PIN[i];
		HAL_GPIO_Init((GPIO_TypeDef*)_KEYPAD_ROW_GPIO_PORT[i],&gpio);
	}

	log_info_m(log_controller, "Keypad initialized!");
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint16_t keypad_scan(void)
{
	uint16_t key = 0;

	for(uint8_t c = 0 ; c < keypad.column_size ; c++)
	{
		for(uint8_t	i = 0 ; i < keypad.column_size ; i++)
		{
			HAL_GPIO_WritePin((GPIO_TypeDef*)_KEYPAD_COLUMN_GPIO_PORT[i],
							   _KEYPAD_COLUMN_GPIO_PIN[i],GPIO_PIN_SET);
		}
		HAL_GPIO_WritePin((GPIO_TypeDef*)_KEYPAD_COLUMN_GPIO_PORT[c],
						   _KEYPAD_COLUMN_GPIO_PIN[c],GPIO_PIN_RESET);

		for(uint8_t r = 0 ; r < keypad.row_size ; r++)
		{
			if(HAL_GPIO_ReadPin((GPIO_TypeDef*)_KEYPAD_ROW_GPIO_PORT[r],
								 _KEYPAD_ROW_GPIO_PIN[r]) == GPIO_PIN_RESET)
			{
				_KEYPAD_DELAY(_KEYPAD_DEBOUNCE_TIME_MS);
				if(HAL_GPIO_ReadPin((GPIO_TypeDef*)_KEYPAD_ROW_GPIO_PORT[r],
								 _KEYPAD_ROW_GPIO_PIN[r]) == GPIO_PIN_RESET)
				{
					key |= 1 << c;
					key |= 1 << (r + 8);

					// log_debug_g(" [ %d ] ", key);
					return key;

				}
			}
		}
	}
	return key;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint16_t keypad_wait_for_key()
{
	uint16_t keyRead;

	keyRead = keypad_scan();
	if(keyRead != keypad.last_key)
	{
		keypad.last_key = keyRead;
		return keyRead;
	}
	return 0;
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

char keypad_wait_for_key_get_char()
{
	switch(keypad_wait_for_key())
	{
		case 0x0802:
			return '0';
		case 0x0101:
			return '1';
		case 0x0102:
			return '2';
		case 0x0104:
			return '3';
		case 0x0201:
			return '4';
		case 0x0202:
			return '5';
		case 0x0204:
			return '6';
		case 0x0401:
			return '7';
		case 0x0402:
			return '8';
		case 0x0404:
			return '9';
		case 0x0801:
			return '*';
		case 0x0804:
			return '#';

		default:
			return 'N';
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
