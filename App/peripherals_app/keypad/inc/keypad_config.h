#ifndef	_KEYPADCONFIG_H
#define	_KEYPADCONFIG_H

#include "stm32f7xx_hal.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define _KEYPAD_DEBOUNCE_TIME_MS		1

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

const GPIO_TypeDef* _KEYPAD_COLUMN_GPIO_PORT[] =
{
		GPIOG,
		GPIOA,
		GPIOB
};

const uint16_t _KEYPAD_COLUMN_GPIO_PIN[] =
{
		GPIO_PIN_6,
		GPIO_PIN_15,
		GPIO_PIN_15
};

const GPIO_TypeDef* _KEYPAD_ROW_GPIO_PORT[] =
{
		GPIOG,
		GPIOB,
		GPIOI,
		GPIOI
};

const uint16_t _KEYPAD_ROW_GPIO_PIN[] =
{
		GPIO_PIN_7,
		GPIO_PIN_4,
		GPIO_PIN_0,
		GPIO_PIN_3
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif	/* _KEYPADCONFIG_H */
