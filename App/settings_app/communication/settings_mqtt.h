#ifndef __SETTINGS_MQTT_H
#define __SETTINGS_MQTT_H

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define MQTT_MODULE_NAME				"user_panel_ctrl"
#define MQTT_MODULE_USER				"user_module"
#define MQTT_MODULE_PSWD				"pswd_module"

#define _MAC_ADDR1						0x00
#define _MAC_ADDR2						0x80
#define _MAC_ADDR3						0xE2
#define _MAC_ADDR4						0x00
#define _MAC_ADDR5						0x00
#define _MAC_ADDR6						0x05

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define _MODULE_IP_ADDR1 				192
#define _MODULE_IP_ADDR2 				168
#define _MODULE_IP_ADDR3 				137
#define _MODULE_IP_ADDR4 				115

#define MODULE_IP_ADDR					_MODULE_IP_ADDR1, _MODULE_IP_ADDR2, \
										_MODULE_IP_ADDR3, _MODULE_IP_ADDR4

#define _NETMASK1						255
#define _NETMASK2						255
#define _NETMASK3						255
#define _NETMASK4						0

#define _NET_GATEWAY1					0
#define _NET_GATEWAY2					0
#define _NET_GATEWAY3					0
#define _NET_GATEWAY4					0

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define _BROKER_ADDR1					192
#define _BROKER_ADDR2					168
#define _BROKER_ADDR3					137
#define _BROKER_ADDR4					1

#define HANGAR_MQTT_BROKER_IP_ADDR		BROKER_ADDR1,BROKER_ADDR2, \
										BROKER_ADDR3,BROKER_ADDR4

#define HANGAR_MQTT_BROKER_PORT			1883

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


#endif /* __SETTINGS_MQTT_H */