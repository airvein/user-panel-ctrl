#ifndef __SETTINGS_APP_PROCEDURE_H
#define __SETTINGS_APP_PROCEDURE_H



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define APP_PROC_CARGO_PIN_NAME                     "cargo_pin"
#define APP_PROC_CARGO_PIN_ID                       1
#define APP_PROC_CARGO_PIN_ACT_GET                  "get"
#define APP_PROC_CARGO_PIN_SIZE                     6

#define APP_PROC_VIEW_NAME                          "view"
#define APP_PROC_VIEW_ID                            2
#define APP_PROC_VIEW_ACT_CARGO_TAKE_OUT            "cargo_take_out"
#define APP_PROC_VIEW_ACT_CHECKING_CARGO            "checking_cargo"
#define APP_PROC_VIEW_ACT_SERVICE                   "service_mode"
#define APP_PROC_VIEW_ACT_HANGAR_ERR                "hangar_breakdown"
#define APP_PROC_VIEW_ACT_PREP_MISSION              "preparing_mission"
#define APP_PROC_VIEW_ACT_CARGO_INCORRECT           "cargo_incorrect"
#define APP_PROC_VIEW_ACT_CARGO_CORRECT             "cargo_correct"
#define APP_PROC_VIEW_ACT_CARGO_PIN_OK              "cargo_pin_correct"
#define APP_PROC_VIEW_ACT_CARGO_PIN_ERR             "cargo_pin_incorrect"
#define APP_PROC_VIEW_ACT_WIN_CLOSE                 "window_closing"
#define APP_PROC_VIEW_ACT_WIN_OPEN                  "window_opening"
#define APP_PROC_VIEW_ACT_THANK_YOU                 "thank_you"

typedef enum
{
    USER_CONFIRM_FALSE,
    USER_CONFIRM_TRUE,
    USER_CONFIRM_TIMEOUT
    
} user_confirmation_t;

#define APP_PROC_CONFIRM_VIEW_NAME                  "confirmation_view"
#define APP_PROC_CONFIRM_VIEW_ID                    3
#define APP_PROC_CONFIRM_VIEW_ACT_CARGO_PUT         "conf_cargo_put"
#define APP_PROC_CONFIRM_VIEW_ACT_CARGO_TAKE_OUT    "conf_cargo_take_out"
#define APP_PROC_CONFIRM_VIEW_ACT_HOME              "conf_home"

#define APP_PROC_GET_STATES_NAME                    "get_states"
#define APP_PROC_GET_STATES_ID                      4
#define APP_PROC_GET_STATES_ACT_GET                 "get"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_APP_PROCEDURE_H */