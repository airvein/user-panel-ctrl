#ifndef __SETTINGS_USB_CONF_H
#define __SETTINGS_USB_CONF_H



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define USBD_VID                            0x7501
#define USBD_LANGID_STRING                  1033
#define USBD_MANUFACTURER_STRING            "Cervi Robotics"
#define USBD_PID_FS                         0x5E06
#define USBD_PRODUCT_STRING_FS              "User panel controller"
#define USBD_CONFIGURATION_STRING_FS        "CDC Config"
#define USBD_INTERFACE_STRING_FS            "CDC Interface"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_USB_CONF_H */