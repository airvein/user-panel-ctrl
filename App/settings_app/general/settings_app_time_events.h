#ifndef __SETTINGS_APP_TIME_EVENTS_H
#define __SETTINGS_APP_TIME_EVENTS_H



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define APP_TEVT_PUBLISH_STATES_MQTT_ID         0
#define APP_TEVT_PUBLISH_STATES_MQTT_SEC        300



#endif /* __SETTINGS_APP_TIME_EVENTS_H */