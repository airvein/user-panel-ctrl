#ifndef __SETTINGS_APP_TIMEOUTS_H
#define __SETTINGS_APP_TIMEOUTS_H



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define TIMEOUT_INTERVAL_MS                     100
#define TIMEOUT_SECOND(sec)                     ((1000/TIMEOUT_INTERVAL_MS)*sec)

#define APP_TOUT_AUTH_PIN_ID                    1          
#define APP_TOUT_AUTH_PIN_TIM_SEC               TIMEOUT_SECOND(60)

#define APP_TOUT_CONF_CARGO_ID                  2          
#define APP_TOUT_CONF_CARGO_TIM_SEC             TIMEOUT_SECOND(60)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_APP_TIMEOUTS_H */