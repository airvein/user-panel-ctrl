#ifndef __SETTINGS_HANGAR_REQ_COMMON_H
#define __SETTINGS_HANGAR_REQ_COMMON_H

#include "../../../App/settings_app/communication/settings_mqtt.h"
#include "communication/protocols/mqtt/inc/mqtt_msg.h"
#include "communication/protocols/mqtt/inc/mqtt_request.h"
#include "communication/interface/hangar/inc/hangar_mqtt_client.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ENABLE_REQ      1
#define DISABLE_REQ     0

#define REQ_ACK_STR     "ack"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define REQ_CLIM_CTRL_GET_STATES        DISABLE_REQ
#define REQ_ROOF_CTRL_GET_STATES        DISABLE_REQ
#define REQ_CARGO_CTRL_GET_STATES       DISABLE_REQ
#define REQ_PMS_CTRL_GET_STATES         DISABLE_REQ

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#if REQ_CLIM_CTRL_GET_STATES
#include "logic/hangar_mqtt_req/get_states/inc/climate_ctrl_req_get_states.h"
#endif

#if REQ_ROOF_CTRL_GET_STATES
#include "logic/hangar_mqtt_req/get_states/inc/roof_ctrl_req_get_states.h"
#endif

#if REQ_CARGO_CTRL_GET_STATES
#include "logic/hangar_mqtt_req/get_states/inc/cargo_ctrl_req_get_states.h"
#endif

#if REQ_PMS_CTRL_GET_STATES
#include "logic/hangar_mqtt_req/get_states/inc/pms_ctrl_req_get_states.h"
#endif


#endif /* __SETTINGS_HANGAR_REQ_COMMON_H */