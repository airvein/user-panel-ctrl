#ifndef __SETTINGS_SD_H
#define __SETTINGS_SD_H



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// #define ENABLE_SD_CARD

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ONE_MINUTE              60
#define TEN_MINUTES             600
#define HALF_HOUR               1800
#define ONE_HOUR                3600
#define ONE_DAY                 86400

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define NEW_FILE_EVERY          ONE_MINUTE        

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_SD_H */