#include "../../App/commands/inc/app_usb_commands.h"

#include "debug/log_modules/log_modules.h"
#include "communication_interface/service/inc/service_usb_msg.h"
#include "communication_interface/hangar/inc/hangar_mqtt_msg.h"




// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CMD_USB_ARRAY_SIZE  1

static cmd_procedure_t *cmd_usb_array[CMD_USB_ARRAY_SIZE] = {0};

static cmd_procedure_ctrl_t *sv_cmd_ctrl_usb = NULL;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_usb_commands_init(void)
{
    static uint16_t cmd_usb_num = 0;

    if(cmd_usb_num != CMD_USB_ARRAY_SIZE)
    {
        log_error_m(log_controller,
                    "Not equal amount of usb cmd initialization "
                    "/ cmd_usb array size! [%s : %s ]",
                    __FILE__, __func__);
    }
    else
    {
        sv_cmd_ctrl_usb = cmd_procedure_ctrl_create();
        cmd_procedure_ctrl_init(sv_cmd_ctrl_usb, cmd_usb_array,
                                CMD_USB_ARRAY_SIZE);
        log_info_m(log_controller,
                   "Application usb commands initialize succesfull!");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_usb_commands_handler(void)
{
    cmd_procedure_ctrl_handler(sv_cmd_ctrl_usb);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|