#ifndef __APP_USB_COMMANDS_H
#define __APP_USB_COMMANDS_H

#include "control/cmd_procedure/inc/cmd_procedure_ctrl.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_usb_commands_init(void);

void app_usb_commands_handler(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __APP_USB_COMMANDS_H */