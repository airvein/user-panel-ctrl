#ifndef __APP_MQTT_COMMANDS_H
#define __APP_MQTT_COMMANDS_H

#include "control/cmd_procedure/inc/cmd_procedure_ctrl.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_mqtt_commands_init(void);

void app_mqtt_commands_handler(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
                    /* app mqtt commands call functions */
void app_mqtt_cmd_cargo_pin_call(char *mqtt_payload);
void app_mqtt_cmd_view_call(char *mqtt_payload);
void app_mqtt_cmd_confirm_view_call(char *mqtt_payload);
void app_mqtt_cmd_get_states_call(char *mqtt_payload);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __APP_MQTT_COMMANDS_H */      