#include "../../App/commands/inc/app_mqtt_commands.h"
#include "debug/log_modules/log_modules.h"
#include "communication_interface/hangar/inc/hangar_mqtt_msg.h"
#include "settings_app/general/settings_app_procedure.h"
#include "procedures/inc/cargo_pin_procedure.h"
#include "procedures/inc/view_procedure.h"
#include "procedures/inc/confirmation_view_procedure.h"
#include "procedures/inc/get_states_procedure.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CMD_LINK_ACT(cmd, proc, act_name) \
    do{\
        cmd_procedure_link_parameter_ch_arr_to_action_id(cmd, act_name,\
            procedure_get_action_id(proc, act_name));\
    }while(0);


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CMD_MQTT_ARRAY_SIZE 4

static cmd_procedure_t *cmd_mqtt_array[CMD_MQTT_ARRAY_SIZE] = { 0 };

static cmd_procedure_ctrl_t sv_cmd_ctrl_mqtt = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static cmd_procedure_t sv_cmd_cargo_pin = { 0 };
static cmd_procedure_t sv_cmd_view = { 0 };
static cmd_procedure_t sv_cmd_confirm_view = { 0 };
static cmd_procedure_t sv_cmd_get_states = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void cmd_cargo_pin_init(void);
static void cmd_view_init(void);
static void cmd_confirm_view_init(void);
static void cmd_get_states_init(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_mqtt_commands_init(void)
{
    static uint16_t cmd_mqtt_num = 0;

    cmd_cargo_pin_init();
    cmd_mqtt_array[cmd_mqtt_num++] = &sv_cmd_cargo_pin;

    cmd_view_init();
    cmd_mqtt_array[cmd_mqtt_num++] = &sv_cmd_view;

    cmd_confirm_view_init();
    cmd_mqtt_array[cmd_mqtt_num++] = &sv_cmd_confirm_view;

    cmd_get_states_init();
    cmd_mqtt_array[cmd_mqtt_num++] = &sv_cmd_get_states;

    if(cmd_mqtt_num != CMD_MQTT_ARRAY_SIZE)
    {
        log_error_m(log_controller, 
                    "Not equal amount of mqtt cmd initalization "
                    "/ cmd_mqtt array size! [ %s : %s ]",
                    __FILE__, __func__);
    }
    else
    {
        cmd_procedure_ctrl_init(&sv_cmd_ctrl_mqtt, cmd_mqtt_array, 
                                CMD_MQTT_ARRAY_SIZE);
        log_info_m(log_controller, 
                    "Application mqtt commands initalize succesfull!");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_mqtt_commands_handler(void)
{
    cmd_procedure_ctrl_handler(&sv_cmd_ctrl_mqtt);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_mqtt_cmd_cargo_pin_call(char *mqtt_payload)
{
    cmd_procedure_call_char_arr(&sv_cmd_cargo_pin, mqtt_payload);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_mqtt_cmd_view_call(char *mqtt_payload)
{
    cmd_procedure_call_char_arr(&sv_cmd_view, mqtt_payload);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_mqtt_cmd_confirm_view_call(char *mqtt_payload)
{
    cmd_procedure_call_char_arr(&sv_cmd_confirm_view, mqtt_payload);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_mqtt_cmd_get_states_call(char *mqtt_payload)
{
    cmd_procedure_call_char_arr(&sv_cmd_get_states, mqtt_payload);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void cmd_cargo_pin_init(void)
{
    cmd_procedure_init(&sv_cmd_cargo_pin, CMD_PARAM_CHAR_ARRAY, 
                    &ev_procedure_cargo_pin, "cmd-auth-pin");
    CMD_LINK_ACT(&sv_cmd_cargo_pin, &ev_procedure_cargo_pin, 
                APP_PROC_CARGO_PIN_ACT_GET);
    cmd_procedure_register_send_response_cb(&sv_cmd_cargo_pin, 
                                            mqtt_cmd_cargo_pin_response_cb);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void cmd_view_init(void)
{
    cmd_procedure_init(&sv_cmd_view, CMD_PARAM_CHAR_ARRAY, &ev_procedure_view,
                        "cmd-view");
    CMD_LINK_ACT(&sv_cmd_view, &ev_procedure_view, 
                APP_PROC_VIEW_ACT_CARGO_TAKE_OUT);
    CMD_LINK_ACT(&sv_cmd_view, &ev_procedure_view, 
                APP_PROC_VIEW_ACT_CHECKING_CARGO);
    CMD_LINK_ACT(&sv_cmd_view, &ev_procedure_view, APP_PROC_VIEW_ACT_SERVICE);
    CMD_LINK_ACT(&sv_cmd_view, &ev_procedure_view, 
                APP_PROC_VIEW_ACT_HANGAR_ERR);
    CMD_LINK_ACT(&sv_cmd_view, &ev_procedure_view, 
                APP_PROC_VIEW_ACT_PREP_MISSION);
    CMD_LINK_ACT(&sv_cmd_view, &ev_procedure_view, 
                APP_PROC_VIEW_ACT_CARGO_INCORRECT);
    CMD_LINK_ACT(&sv_cmd_view, &ev_procedure_view, 
                APP_PROC_VIEW_ACT_CARGO_CORRECT);
    CMD_LINK_ACT(&sv_cmd_view, &ev_procedure_view, 
                APP_PROC_VIEW_ACT_CARGO_PIN_OK);
    CMD_LINK_ACT(&sv_cmd_view, &ev_procedure_view, 
                APP_PROC_VIEW_ACT_CARGO_PIN_ERR);
    CMD_LINK_ACT(&sv_cmd_view, &ev_procedure_view, 
                APP_PROC_VIEW_ACT_WIN_CLOSE);
    CMD_LINK_ACT(&sv_cmd_view, &ev_procedure_view, 
                APP_PROC_VIEW_ACT_WIN_OPEN);
    CMD_LINK_ACT(&sv_cmd_view, &ev_procedure_view, 
                APP_PROC_VIEW_ACT_THANK_YOU);

    cmd_procedure_register_send_response_cb(&sv_cmd_view, 
                                            mqtt_cmd_view_response_cb);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void cmd_confirm_view_init(void)
{
    cmd_procedure_init(&sv_cmd_confirm_view, CMD_PARAM_CHAR_ARRAY, 
                        &ev_procedure_confirmation_view, "cmd-confirm-view");
    CMD_LINK_ACT(&sv_cmd_confirm_view, &ev_procedure_confirmation_view, 
                APP_PROC_CONFIRM_VIEW_ACT_HOME);
    CMD_LINK_ACT(&sv_cmd_confirm_view, &ev_procedure_confirmation_view, 
                APP_PROC_CONFIRM_VIEW_ACT_CARGO_PUT);
    CMD_LINK_ACT(&sv_cmd_confirm_view, &ev_procedure_confirmation_view, 
                APP_PROC_CONFIRM_VIEW_ACT_CARGO_TAKE_OUT);
    cmd_procedure_register_send_response_cb(&sv_cmd_confirm_view, 
                                        mqtt_cmd_confirm_view_response_cb);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void cmd_get_states_init(void)
{
    cmd_procedure_init(&sv_cmd_get_states, CMD_PARAM_CHAR_ARRAY,
                        &ev_procedure_get_states, "cmd-get-states");
    CMD_LINK_ACT(&sv_cmd_get_states, &ev_procedure_get_states, 
                APP_PROC_GET_STATES_ACT_GET);    
    cmd_procedure_register_send_response_cb(&sv_cmd_get_states,
                                        mqtt_cmd_get_states_response_cb);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|