/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  * @author			    : Mateusz Strojek
  ******************************************************************************
  *
  * COPYRIGHT(c) 2020 Cervi Robotics
  */

#include <application/inc/main.h>
#include <application/inc/mcu_peripherals_init.h>


#include "debug/log_modules/log_modules.h"
#include "peripherals_core/rtc/inc/rtc.h"
#include "peripherals_core/heartbeat_led/inc/heartbeat_led.h"
#include "os_tasks/inc/os_tasks.h"
#include "peripherals_app/keypad/inc/keypad.h"
#include "app_time/inc/app_timeouts.h"
#include "time/timeout/inc/timeout.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void start_system_log(void)
{
	log_info_m(log_controller, " ");
	log_info_m(log_controller, "---------------------------------------");
	log_info_m(log_controller, "------------ SYSTEM START -------------");
	log_info_m(log_controller, "---------------------------------------");
	log_info_m(log_controller, " ");
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

int main(void)
{
  HAL_Init();
  mcu_peripheral_init();
  
  heartbeat_led_init();
  init_log_modules();
  log_set_timestamp_var_ptr(&ev_system_timestamp);
  start_system_log();
  app_timeout_init();
  keypad_init();

  os_tasks_start();
 
  while (1)
  {

  }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if (htim->Instance == TIM6)
  {
    HAL_IncTick();
  }
  timeout_timer_period_elapsed(htim);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void Error_Handler(void)
{

}
