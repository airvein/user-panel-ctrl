#include "inc/gui_interface.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   F U N C T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void frontend_application_set_screen(uint8_t screen_enum);
void update_pin(int pin_cnt);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint8_t current_screen_id = SCREEN_STARTED_LOGO;

char *screen_str[] = 
{
    [SCREEN_HOME] = APP_PROC_CONFIRM_VIEW_ACT_HOME,
    [SCREEN_CONF_CARGO_TAKE_OUT] = APP_PROC_CONFIRM_VIEW_ACT_CARGO_TAKE_OUT,
    [SCREEN_CONF_CARGO_PUT] = APP_PROC_CONFIRM_VIEW_ACT_CARGO_PUT,
    [SCREEN_CARGO_TAKE_OUT] = APP_PROC_VIEW_ACT_CARGO_TAKE_OUT,
    [SCREEN_ENTER_CARGO_PIN] = "enter_cargo_pin",
    [SCREEN_CHECKING_CARGO] = APP_PROC_VIEW_ACT_CHECKING_CARGO,
    [SCREEN_SERVICE_MODE] = APP_PROC_VIEW_ACT_SERVICE,
    [SCREEN_HANGAR_BREAKDOWN] = APP_PROC_VIEW_ACT_HANGAR_ERR,
    [SCREEN_PREPARING_MISSION] = APP_PROC_VIEW_ACT_PREP_MISSION,
    [SCREEN_CARGO_INCORRECT] = APP_PROC_VIEW_ACT_CARGO_INCORRECT,
    [SCREEN_CARGO_CORRECT] = APP_PROC_VIEW_ACT_CARGO_CORRECT,
    [SCREEN_CARGO_PIN_INCORRECT] = APP_PROC_VIEW_ACT_CARGO_PIN_ERR,
    [SCREEN_CARGO_PIN_CORRECT] = APP_PROC_VIEW_ACT_CARGO_PIN_OK,
    [SCREEN_WINDOW_OPENING] = APP_PROC_VIEW_ACT_WIN_OPEN,
    [SCREEN_WINDOW_CLOSING] = APP_PROC_VIEW_ACT_WIN_CLOSE,
    [SCREEN_THANK_YOU] = APP_PROC_VIEW_ACT_THANK_YOU,
    [SCREEN_CARGO_PIN_CHECK] = "cargo_pin_check",
    [SCREEN_CARGO_PIN_TOUT] = "cargo_pin_tout",
    [SCREEN_STARTED_LOGO] = "started_logo"
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void gui_frontend_set_screen(uint8_t screen_enum)
{
    current_screen_id = screen_enum;
    frontend_application_set_screen(screen_enum);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void gui_update_pin(int pin_len)
{
    if(current_screen_id == SCREEN_ENTER_CARGO_PIN)
    {
        update_pin(pin_len);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

char *gui_frontend_get_screen(void)
{
    return screen_str[current_screen_id];
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|