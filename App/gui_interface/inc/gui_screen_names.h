#ifndef __GUI_SCREEN_NAMES_H
#define __GUI_SCREEN_NAMES_H



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


enum 
{   
    SCREEN_HOME,
    SCREEN_CONF_CARGO_TAKE_OUT,
    SCREEN_CONF_CARGO_PUT,
    SCREEN_CARGO_TAKE_OUT,
    SCREEN_ENTER_CARGO_PIN,
    SCREEN_CHECKING_CARGO,
    SCREEN_SERVICE_MODE,
    SCREEN_HANGAR_BREAKDOWN,
    SCREEN_PREPARING_MISSION,
    SCREEN_CARGO_INCORRECT,
    SCREEN_CARGO_CORRECT,
    SCREEN_CARGO_PIN_INCORRECT,
    SCREEN_CARGO_PIN_CORRECT,
    SCREEN_WINDOW_OPENING,
    SCREEN_WINDOW_CLOSING,
    SCREEN_THANK_YOU,
    SCREEN_CARGO_PIN_CHECK,
    SCREEN_CARGO_PIN_TOUT,
    SCREEN_STARTED_LOGO
}; 

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __GUI_SCREEN_NAMES_H */