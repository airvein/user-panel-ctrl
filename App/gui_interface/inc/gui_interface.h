#ifndef __GUI_INTERFACE_H
#define __GUI_INTERFACE_H

#include <stdint.h>
#include "settings_app/general/settings_app_procedure.h"
#include "gui_screen_names.h"


    
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void gui_frontend_set_screen(uint8_t screen_enum);
char *gui_frontend_get_screen(void);

void gui_update_pin(int pin_len);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __GUI_INTERFACE_H */