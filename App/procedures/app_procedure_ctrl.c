#include "../../App/procedures/inc/app_procedure_ctrl.h"
#include "../../App/settings_app/general/settings_app_procedure.h"
#include "control/procedure/inc/procedure_ctrl.h"
#include "debug/log_modules/log_modules.h"
#include "../../App/procedures/inc/get_states_procedure.h"

#include "inc/cargo_pin_procedure.h"
#include "inc/view_procedure.h"
#include "inc/confirmation_view_procedure.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define PROCEDURE_ARRAY_SIZE    4

static procedure_t *procedure_array[PROCEDURE_ARRAY_SIZE] = {0};

static procedure_ctrl_t sv_precedure_ctrl = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_procedure_ctrl_init(void)
{
    static uint16_t procedure_num = 0;

    view_procedure_init();
    procedure_array[procedure_num++] = &ev_procedure_view;

    cargo_pin_procedure_init();
    procedure_array[procedure_num++] = &ev_procedure_cargo_pin;

    confirmation_view_procedure_init();
    procedure_array[procedure_num++] = &ev_procedure_confirmation_view;

    get_states_procedure_init();
    procedure_array[procedure_num++] = &ev_procedure_get_states;

    if(procedure_num != PROCEDURE_ARRAY_SIZE || procedure_num == 0)
    {
        log_error_m(log_controller, 
                    "Not equal amount of procedure initalization "
                    "/ procedure array size! [ %s : %s ]",
                    __FILE__, __func__);
        while(1) ;
    }
    else
    {
        procedure_ctrl_init(&sv_precedure_ctrl, procedure_array, 
                            PROCEDURE_ARRAY_SIZE);
        log_info_m(log_controller, 
                    "Application procedure initialize succesfull!");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_procedure_ctrl_handler(void)
{
    procedure_ctrl_handler(&sv_precedure_ctrl);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|