#include "inc/view_procedure.h"
#include "settings_app/general/settings_app_procedure.h"
#include "debug/log_modules/log_modules.h"
#include "communication_interface/hangar/inc/hangar_mqtt_msg.h"
#include "gui_interface/inc/gui_interface.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ACTION_CALLBACKS(exec_cb) \
                        ((struct action_callbacks_s)\
                        {\
                            .feasibility_check = PROCEDURE_NO_CLBK,\
                            .execute = exec_cb,\
                            .evaluate = PROCEDURE_NO_CLBK,\
                            .done = view_done_cb,\
                            .error = PROCEDURE_NO_CLBK\
                        })

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

procedure_t ev_procedure_view = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t cargo_take_out_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size);
static procedure_retval_t checking_cargo_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size);
static procedure_retval_t service_mode_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size);
static procedure_retval_t hangar_breakdown_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size);
static procedure_retval_t preparing_mission_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size);
static procedure_retval_t cargo_correct_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size);
static procedure_retval_t cargo_incorrect_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size);
static procedure_retval_t cargo_pin_correct_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size);
static procedure_retval_t cargo_pin_incorrect_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size);
static procedure_retval_t window_open_exec_cb(uint32_t *arg, uint8_t arg_size);
static procedure_retval_t window_close_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size);
static procedure_retval_t thank_you_exec_cb(uint32_t *arg, uint8_t arg_size);

static procedure_retval_t view_done_cb(uint32_t *arg, uint8_t arg_size);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void view_procedure_init(void)
{
    procedure_init(&ev_procedure_view, APP_PROC_VIEW_NAME, APP_PROC_VIEW_ID);

    procedure_add_action(&ev_procedure_view, APP_PROC_VIEW_ACT_CARGO_TAKE_OUT,
                        ACTION_CALLBACKS(cargo_take_out_exec_cb));
    procedure_add_action(&ev_procedure_view, APP_PROC_VIEW_ACT_CHECKING_CARGO,
                        ACTION_CALLBACKS(checking_cargo_exec_cb));
    procedure_add_action(&ev_procedure_view, APP_PROC_VIEW_ACT_SERVICE,
                        ACTION_CALLBACKS(service_mode_exec_cb));
    procedure_add_action(&ev_procedure_view, APP_PROC_VIEW_ACT_HANGAR_ERR,
                        ACTION_CALLBACKS(hangar_breakdown_exec_cb));
    procedure_add_action(&ev_procedure_view, APP_PROC_VIEW_ACT_PREP_MISSION,
                        ACTION_CALLBACKS(preparing_mission_exec_cb));
    procedure_add_action(&ev_procedure_view, APP_PROC_VIEW_ACT_CARGO_INCORRECT,
                        ACTION_CALLBACKS(cargo_incorrect_exec_cb));
    procedure_add_action(&ev_procedure_view, APP_PROC_VIEW_ACT_CARGO_CORRECT,
                        ACTION_CALLBACKS(cargo_correct_exec_cb));
    procedure_add_action(&ev_procedure_view, APP_PROC_VIEW_ACT_CARGO_PIN_OK,
                        ACTION_CALLBACKS(cargo_pin_correct_exec_cb));
    procedure_add_action(&ev_procedure_view, APP_PROC_VIEW_ACT_CARGO_PIN_ERR,
                        ACTION_CALLBACKS(cargo_pin_incorrect_exec_cb));
    procedure_add_action(&ev_procedure_view, APP_PROC_VIEW_ACT_WIN_OPEN,
                        ACTION_CALLBACKS(window_open_exec_cb));
    procedure_add_action(&ev_procedure_view, APP_PROC_VIEW_ACT_WIN_CLOSE,
                        ACTION_CALLBACKS(window_close_exec_cb));
    procedure_add_action(&ev_procedure_view, APP_PROC_VIEW_ACT_THANK_YOU,
                        ACTION_CALLBACKS(thank_you_exec_cb));

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t cargo_take_out_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size)
{
    gui_frontend_set_screen(SCREEN_CARGO_TAKE_OUT);

    return PROCEDURE_RETVAL_DONE;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t checking_cargo_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size)
{
    gui_frontend_set_screen(SCREEN_CHECKING_CARGO);

    return PROCEDURE_RETVAL_DONE;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t service_mode_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size)
{
    gui_frontend_set_screen(SCREEN_SERVICE_MODE);

    return PROCEDURE_RETVAL_DONE;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t hangar_breakdown_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size)
                                            
{
    gui_frontend_set_screen(SCREEN_HANGAR_BREAKDOWN);

    return PROCEDURE_RETVAL_DONE;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t preparing_mission_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size)
{
    gui_frontend_set_screen(SCREEN_PREPARING_MISSION);

    return PROCEDURE_RETVAL_DONE;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t cargo_correct_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size)
                                            
{
    gui_frontend_set_screen(SCREEN_CARGO_CORRECT);

    return PROCEDURE_RETVAL_DONE;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t cargo_incorrect_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size)
{
    gui_frontend_set_screen(SCREEN_CARGO_INCORRECT);

    return PROCEDURE_RETVAL_DONE;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t cargo_pin_correct_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size)
{
    gui_frontend_set_screen(SCREEN_CARGO_PIN_CORRECT);

    return PROCEDURE_RETVAL_DONE;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t cargo_pin_incorrect_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size)
                                            
{
    gui_frontend_set_screen(SCREEN_CARGO_PIN_INCORRECT);

    return PROCEDURE_RETVAL_DONE;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t window_open_exec_cb(uint32_t *arg, uint8_t arg_size)
{
    gui_frontend_set_screen(SCREEN_WINDOW_OPENING);

    return PROCEDURE_RETVAL_DONE;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t window_close_exec_cb(uint32_t *arg, 
                                            uint8_t arg_size)
{
    gui_frontend_set_screen(SCREEN_WINDOW_CLOSING);

    return PROCEDURE_RETVAL_DONE;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t thank_you_exec_cb(uint32_t *arg, uint8_t arg_size)
{
    gui_frontend_set_screen(SCREEN_THANK_YOU);

    return PROCEDURE_RETVAL_DONE;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t view_done_cb(uint32_t *arg, uint8_t arg_size)
{
    hangar_pub_state_view(gui_frontend_get_screen());

    return PROCEDURE_RETVAL_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|