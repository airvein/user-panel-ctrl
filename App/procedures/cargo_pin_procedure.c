#include "inc/cargo_pin_procedure.h"
#include "settings_app/general/settings_app_procedure.h"
#include "debug/log_modules/log_modules.h"
#include "app_time/inc/app_timeouts.h"
#include "time/timeout/inc/timeout.h"
#include "peripherals_app/keypad/inc/keypad.h"
#include "communication_interface/hangar/inc/hangar_mqtt_msg.h"
#include "gui_interface/inc/gui_interface.h"
#include "stm32f7xx.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ACTION_CALLBACKS(feas_cb, exec_cb, eval_cb, done_cb, err_cb) \
                        ((struct action_callbacks_s)\
                        {\
                            .feasibility_check = feas_cb,\
                            .execute = exec_cb,\
                            .evaluate = eval_cb,\
                            .done = done_cb,\
                            .error = err_cb\
                        })

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

procedure_t ev_procedure_cargo_pin = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define NONE_INPUT_CHAR     'N'
#define PIN_SIZE            APP_PROC_CARGO_PIN_SIZE

static char pin_input_arr[PIN_SIZE] = { 0 };
static uint8_t pin_input_id = 0;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static action_resp_t      pin_get_feas_cb(uint32_t *arg, uint8_t arg_size);
static procedure_retval_t pin_get_exec_cb(uint32_t *arg, uint8_t arg_size);
static procedure_retval_t pin_get_eval_cb(uint32_t *arg, uint8_t arg_size);
static procedure_retval_t pin_get_done_cb(uint32_t *arg, uint8_t arg_size);
static procedure_retval_t pin_get_err_cb(uint32_t *arg, uint8_t arg_size);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void cargo_pin_procedure_init(void)
{
    procedure_init(&ev_procedure_cargo_pin, APP_PROC_CARGO_PIN_NAME, 
                    APP_PROC_CARGO_PIN_ID);

    procedure_add_action(&ev_procedure_cargo_pin, APP_PROC_CARGO_PIN_ACT_GET,
                        ACTION_CALLBACKS(pin_get_feas_cb,
                                         pin_get_exec_cb,
                                         pin_get_eval_cb,
                                         pin_get_done_cb,
                                         pin_get_err_cb)
    );
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static action_resp_t pin_get_feas_cb(uint32_t *arg, uint8_t arg_size)
{
    return ACTION_ACCEPTED;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t pin_get_exec_cb(uint32_t *arg, uint8_t arg_size)
{
    memset(pin_input_arr, 0, PIN_SIZE);
    pin_input_id = 0;
    gui_frontend_set_screen(SCREEN_ENTER_CARGO_PIN);
    hangar_pub_state_view(gui_frontend_get_screen());

    // timeout_reset(&ev_tout_auth_pin);
    timeout_start(&ev_tout_auth_pin);

    return PROCEDURE_RETVAL_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
#define USER_BTN_   GPIOI,GPIO_PIN_11
static procedure_retval_t pin_get_eval_cb(uint32_t *arg, uint8_t arg_size)
{
    char user_input = keypad_wait_for_key_get_char();

    if(user_input != NONE_INPUT_CHAR)
    {
        log_debug_m(log_controller, 
                    "Input char from keypad [ %c ] [ %d ]!", 
                    user_input, pin_input_id);

        if(user_input == '*')
        {            
            if(pin_input_id > 0)
            {
                log_debug_m(log_controller, "Delete one char of pin");
                pin_input_id--;
            }
        }
        else if(user_input == '#')
        {
            log_debug_m(log_controller, "Clear pin");
            pin_input_id = 0;
        }
        else
        {
            pin_input_arr[pin_input_id++] = user_input;
        }
        gui_update_pin(pin_input_id);
    }

    if(pin_input_id == PIN_SIZE)
    {
        log_debug_m(log_controller, "Pin [ %d, %d, %d, %d ]", 
                    pin_input_arr[0], pin_input_arr[1],
                    pin_input_arr[2], pin_input_arr[3]);

        timeout_reset(&ev_tout_auth_pin);
        return PROCEDURE_RETVAL_DONE;
    }

    if(timeout_get_state(&ev_tout_auth_pin) == TIMEOUT_STATE_TRIGGERED)
    {
        timeout_reset(&ev_tout_auth_pin);
        return PROCEDURE_RETVAL_ERROR;
    }
    
    return PROCEDURE_RETVAL_PENDING;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t pin_get_done_cb(uint32_t *arg, uint8_t arg_size)
{
    gui_frontend_set_screen(SCREEN_CARGO_PIN_CHECK);
    hangar_pub_state_cargo_pin(pin_input_arr);
    hangar_pub_state_view(gui_frontend_get_screen());
    
    return PROCEDURE_RETVAL_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t pin_get_err_cb(uint32_t *arg, uint8_t arg_size)
{
    gui_frontend_set_screen(SCREEN_CARGO_PIN_TOUT);
    hangar_pub_state_cargo_pin("tout");
    hangar_pub_state_view(gui_frontend_get_screen());

    return PROCEDURE_RETVAL_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|