#include "../../App/procedures/inc/get_states_procedure.h"

#include "settings_app/general/settings_app_procedure.h"
#include "debug/log_modules/log_modules.h"

#include "communication/protocols/mqtt/inc/mqtt_msg.h"

#include "communication_interface/hangar/inc/hangar_mqtt_msg.h"
#include "peripherals_core/plc_module/inc/plc_module.h"
#include "communication/interface/hangar/inc/hangar_communication.h"
#include "../../App/settings_app/general/settings_app_procedure.h"
#include "gui_interface/inc/gui_interface.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ACTION_CALLBACKS(feas_cb, exec_cb, eval_cb, done_cb, err_cb) \
    ((struct action_callbacks_s)\
    {\
        .feasibility_check = feas_cb,\
        .execute = exec_cb,\
        .evaluate = eval_cb,\
        .done = done_cb,\
        .error = err_cb\
    })

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define PUBLISH_SINGLE_STATE_HANDLER(seq_id, state_val,\
                                     pub_state_func_cb, get_pubs_state_cb)\
    {\
        if(sv_publish_states_mqtt_id == seq_id)\
        {\
            if(sv_publish_states_switch == false)\
            {\
                pub_state_func_cb(state_val);\
        /* TODO: sprawdzic czy mqtt_msg->state == MSG_STATE_PUBLISHING */\
                sv_publish_states_switch = true;\
            }\
            if(sv_publish_states_switch)\
            {\
                if(get_pubs_state_cb() == MSG_STATE_PUBLISHED)\
                {\
                    sv_publish_states_mqtt_id++;\
                    sv_publish_states_switch = false;\
                    return PROCEDURE_RETVAL_PENDING;\
                }\
            }\
        }\
    }
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

procedure_t ev_procedure_get_states = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static uint8_t sv_publish_states_mqtt_id = 0;
static bool sv_publish_states_switch = false;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

/* get all action callbacks */
static procedure_retval_t get_states_all_exec_cb(uint32_t* arg,
                                                 uint8_t arg_size);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void get_states_procedure_init(void)
{
    procedure_init(&ev_procedure_get_states, APP_PROC_GET_STATES_NAME,
                   APP_PROC_GET_STATES_ID);
    /* get all action */
    procedure_add_action(&ev_procedure_get_states, APP_PROC_GET_STATES_ACT_GET,
                         ACTION_CALLBACKS(PROCEDURE_NO_CLBK,
                                          get_states_all_exec_cb,
                                          PROCEDURE_NO_CLBK,
                                          PROCEDURE_NO_CLBK,
                                          PROCEDURE_NO_CLBK)
    );
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t get_states_all_exec_cb(uint32_t* arg, 
                                                 uint8_t arg_size)
{
    if(hangar_mqtt_is_connected() == true)
    {
        PUBLISH_SINGLE_STATE_HANDLER(0, gui_frontend_get_screen(),
                                    hangar_pub_state_view, 
                                    hangar_get_pub_state_view);

        if(sv_publish_states_mqtt_id == 1)
        {
            sv_publish_states_mqtt_id = 0;
            return PROCEDURE_RETVAL_DONE;
        }  
        return PROCEDURE_RETVAL_PENDING;
    }
    return PROCEDURE_RETVAL_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|