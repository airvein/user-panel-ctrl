#include "inc/confirmation_view_procedure.h"
#include "settings_app/general/settings_app_procedure.h"
#include "debug/log_modules/log_modules.h"
#include "app_time/inc/app_timeouts.h"
#include "time/timeout/inc/timeout.h"
#include "peripherals_app/keypad/inc/keypad.h"
#include "communication_interface/hangar/inc/hangar_mqtt_msg.h"
#include "gui_interface/inc/gui_interface.h"
#include "stm32f7xx.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ACTION_CALLBACKS(feas_cb, exec_cb, eval_cb, done_cb, err_cb) \
    ((struct action_callbacks_s)\
    {\
        .feasibility_check = feas_cb,\
        .execute = exec_cb,\
        .evaluate = eval_cb,\
        .done = done_cb,\
        .error = err_cb\
    })

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define USER_CONFIRM_BUTTON     '#'

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

procedure_t ev_procedure_confirmation_view = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
/* home */
static procedure_retval_t home_exec_cb(uint32_t *arg, uint8_t arg_size);
static procedure_retval_t home_eval_cb(uint32_t *arg, uint8_t arg_size);

/* cargo take_out */
static procedure_retval_t cargo_out_exec_cb(uint32_t *arg, uint8_t arg_size);
/* cargo put */
static procedure_retval_t cargo_in_exec_cb(uint32_t *arg, uint8_t arg_size);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
/* common */
static procedure_retval_t tout_eval_cb(uint32_t *arg, uint8_t arg_size);
static procedure_retval_t confirm_done_cb(uint32_t *arg, uint8_t arg_size);
static procedure_retval_t confirm_err_cb(uint32_t *arg, uint8_t arg_size);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void confirmation_view_procedure_init(void)
{
    procedure_init(&ev_procedure_confirmation_view, 
                    APP_PROC_CONFIRM_VIEW_NAME, 
                    APP_PROC_CONFIRM_VIEW_ID);

    procedure_add_action(&ev_procedure_confirmation_view, 
                        APP_PROC_CONFIRM_VIEW_ACT_HOME,
                        ACTION_CALLBACKS(PROCEDURE_NO_CLBK,
                                         home_exec_cb,
                                         home_eval_cb,
                                         confirm_done_cb,
                                         PROCEDURE_NO_CLBK)
                        );

    procedure_add_action(&ev_procedure_confirmation_view, 
                        APP_PROC_CONFIRM_VIEW_ACT_CARGO_TAKE_OUT,
                        ACTION_CALLBACKS(PROCEDURE_NO_CLBK,
                                         cargo_out_exec_cb,
                                         tout_eval_cb,
                                         confirm_done_cb,
                                         confirm_err_cb)
                        );

    procedure_add_action(&ev_procedure_confirmation_view, 
                        APP_PROC_CONFIRM_VIEW_ACT_CARGO_PUT,
                        ACTION_CALLBACKS(PROCEDURE_NO_CLBK,
                                         cargo_in_exec_cb,
                                         tout_eval_cb,
                                         confirm_done_cb,
                                         confirm_err_cb)
                        );
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t home_exec_cb(uint32_t *arg, uint8_t arg_size)
{
    gui_frontend_set_screen(SCREEN_HOME);
    hangar_pub_state_view(gui_frontend_get_screen());

    return PROCEDURE_RETVAL_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t home_eval_cb(uint32_t *arg, uint8_t arg_size)
{
    char user_input = keypad_wait_for_key_get_char();

    if(user_input == USER_CONFIRM_BUTTON)
    {
        log_info_m(log_controller, 
                    "User start cargo procedure!");
        return PROCEDURE_RETVAL_DONE;
    }
    
    return PROCEDURE_RETVAL_PENDING;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t cargo_out_exec_cb(uint32_t *arg, uint8_t arg_size)
{
    timeout_reset(&ev_tout_confirm_cargo);
    gui_frontend_set_screen(SCREEN_CONF_CARGO_TAKE_OUT);
    hangar_pub_state_view(gui_frontend_get_screen());
    timeout_start(&ev_tout_confirm_cargo);

    return PROCEDURE_RETVAL_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t cargo_in_exec_cb(uint32_t *arg, uint8_t arg_size)
{
    timeout_reset(&ev_tout_confirm_cargo);
    gui_frontend_set_screen(SCREEN_CONF_CARGO_PUT);
    hangar_pub_state_view(gui_frontend_get_screen());
    timeout_start(&ev_tout_confirm_cargo);

    return PROCEDURE_RETVAL_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t tout_eval_cb(uint32_t *arg, uint8_t arg_size)
{
    char user_input = keypad_wait_for_key_get_char();

    if(user_input == USER_CONFIRM_BUTTON)
    {
        log_info_m(log_controller, 
                    "User confirm next stage cargo procedure!");
        return PROCEDURE_RETVAL_DONE;
    }

    if(timeout_get_state(&ev_tout_confirm_cargo) == TIMEOUT_STATE_TRIGGERED)
    {
        timeout_reset(&ev_tout_confirm_cargo);
        return PROCEDURE_RETVAL_ERROR;
    }
    
    return PROCEDURE_RETVAL_PENDING;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t confirm_done_cb(uint32_t *arg, uint8_t arg_size)
{
    hangar_pub_state_user_confirm(USER_CONFIRM_TRUE);
    timeout_reset(&ev_tout_confirm_cargo);
    
    return PROCEDURE_RETVAL_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static procedure_retval_t confirm_err_cb(uint32_t *arg, uint8_t arg_size)
{
    hangar_pub_state_user_confirm(USER_CONFIRM_TIMEOUT);
    timeout_reset(&ev_tout_confirm_cargo);

    return PROCEDURE_RETVAL_DONE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|