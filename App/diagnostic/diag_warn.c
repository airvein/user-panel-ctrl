#include "../../App/diagnostic/inc/diag_warn.h"
#include "inc/app_diagnostic.h"
#include "communication_interface/hangar/inc/hangar_mqtt_msg.h"
#include "../../App/diagnostic/inc/app_diagnostic.h"
#include "../../App/settings_app/general/settings_app_diag_codes.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define NO_STATE_HANDLER    NULL

#define DIAG_INIT(ptr, name, check_state_cb,\
                  change_state_cb,\
                  state_handler_cb)\
    do{\
        ptr = diagnostic_create();\
        diagnostic_init(ptr, name, check_state_cb,\
                        change_state_cb, state_handler_cb);\
        ev_diag_array[ev_diag_num++] = ptr;\
    }while(0);

#define DIAG_CSTATE (uint8_t)diag->current_state

#define FEAS_OK 0

#define PUB_MQTT_WARN_CODE(warning_code)\
    do{\
        if(diag->current_state != FEAS_OK)\
        {\
            char warn_code[20];\
            sprintf(warn_code, "0x%08lx,0x%04x",\
                    warning_code, diag->current_state);\
            hangar_pub_diag_warning(warn_code);\
        }\
    }while(0);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void diagnostic_warning_init(void)
{
                             
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|