#include "../../App/diagnostic/inc/app_diagnostic.h"

#include "debug/diagnostic/inc/diagnostic_ctrl.h"
#include "debug/log_modules/log_modules.h"

#include <stdlib.h>
#include "../../App/diagnostic/inc/diag_err.h"
#include "../../App/diagnostic/inc/diag_state.h"
#include "../../App/diagnostic/inc/diag_warn.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint16_t ev_diag_num = 0;

diagnostic_t *ev_diag_array[DIAG_ARRAY_SIZE] = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static diagnostic_ctrl_t *sv_diag_ctrl = NULL;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_diagnostics_init(void)
{ 
    diagnostic_state_init();
    diagnostic_warning_init();
    diagnostic_error_init();

    if(ev_diag_num != DIAG_ARRAY_SIZE)
    {
        log_error_m(log_controller, 
                    "Not equal amount of diagnostic initalization "
                    "/ diagnostic array size! [ %s : %s ]",
                    __FILE__, __func__);
    }
    else
    {
        sv_diag_ctrl = diagnostic_ctrl_create();
        diagnostic_ctrl_init(sv_diag_ctrl, ev_diag_array, ev_diag_num);

        log_info_m(log_controller, 
                  "Application diagnostics initalize succesfull!");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_diagnostics_handler(void)
{
    // diagnostic_ctrl_handler(sv_diag_ctrl); 
    /* TODO: uncomment */
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|