#include "../../App/app_time/inc/app_time_events.h"

#include "time/time_event/inc/time_event.h"
#include "debug/log_modules/log_modules.h"
#include "communication/protocols/mqtt/inc/mqtt_msg.h"

#include "communication_interface/hangar/inc/hangar_mqtt_msg.h"
#include "peripherals_core/plc_module/inc/plc_module.h"
#include "communication/interface/hangar/inc/hangar_communication.h"

#include "../../App/commands/inc/app_mqtt_commands.h"
#include "../../App/peripherals_app/init/inc/app_peripheral_init.h"
#include "../../App/settings_app/general/settings_app_procedure.h"
#include "../../App/settings_app/general/settings_app_time_events.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define PUBLISH_SINGLE_STATE_HANDLER(seq_id, state_val,\
                                     pub_state_func_cb, get_pubs_state_cb)\
    do{\
        if(sv_publish_states_mqtt_id == seq_id)\
        {\
            if(sv_publish_states_switch == false)\
            {\
                pub_state_func_cb(state_val);\
        /* TODO: sprawdzic czy mqtt_msg->state == MSG_STATE_PUBLISHING */\
                sv_publish_states_switch = true;\
            }\
            if(sv_publish_states_switch)\
            {\
                if(get_pubs_state_cb() == MSG_STATE_PUBLISHED)\
                {\
                    sv_publish_states_mqtt_id++;\
                    sv_publish_states_switch = false;\
                    return TIME_EVT_RET_PENDING;\
                }\
            }\
        }\
    }while(0);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define TIME_EVENT_ARRAY_SIZE 1

static time_event_t *sv_time_event_array[TIME_EVENT_ARRAY_SIZE] = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_time_events_init(void)
{
    static uint16_t tevt_num = 0;

    if(tevt_num != TIME_EVENT_ARRAY_SIZE)
    {
        log_error_m(log_controller, 
                    "Not equal amount of time events initalization "
                    "/ time events array size! [ %s : %s ]",
                    __FILE__, __func__);
    }
    else
    {
        time_event_register_events_array(sv_time_event_array, tevt_num);
        time_event_initialize_timer();

        log_info_m(log_controller, 
                  "Application time events initalize succesfull!");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_time_events_exec_handler(void)
{
    // time_event_exec_handler(); /* TODO: ucomment */
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
