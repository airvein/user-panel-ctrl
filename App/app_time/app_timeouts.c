#include "../../App/app_time/inc/app_timeouts.h"

#include "../../App/settings_app/general/settings_app_timeouts.h"
#include "debug/log_modules/log_modules.h"
#include "logic/hangar_ctrl_reset/inc/hangar_ctrl_reset.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define TIMEOUT_INIT(tout, id, int_m) \
    do {\
        timeout_init(tout, id, int_m);\
        sv_tout_array[tout_num++] = tout;\
    } while (0);


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

timeout_t ev_tout_auth_pin = { 0 };
timeout_t ev_tout_confirm_cargo = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define TOUT_ARRAY_SIZE 3

static timeout_t *sv_tout_array[TOUT_ARRAY_SIZE] = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_timeout_init(void)
{
    uint16_t tout_num  = 0;

    TIMEOUT_INIT(&ev_tout_auth_pin, APP_TOUT_AUTH_PIN_ID, 
                APP_TOUT_AUTH_PIN_TIM_SEC);
    TIMEOUT_INIT(&ev_tout_confirm_cargo, APP_TOUT_CONF_CARGO_ID, 
                APP_TOUT_CONF_CARGO_TIM_SEC);
    
    hangar_ctrl_reset_tout_init();
    sv_tout_array[tout_num++] = &ev_tout_ctrl_reset;    

    if(tout_num != TOUT_ARRAY_SIZE || tout_num == 0)
    {
        log_error_m(log_controller, 
                    "Not equal amount of timeout initalization "
                    "/ timeout array size! [ %s : %s ]",
                    __FILE__, __func__);
        while(1);
    }
    else
    {
        timeout_register_timeouts_array(sv_tout_array, tout_num);
        timeout_initialize_timer();

        log_info_m(log_controller, 
                "Application timeout initalize succesfull!");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
