#ifndef __APP_TIME_EVENTS_H
#define __APP_TIME_EVENTS_H



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void app_time_events_init(void);

void app_time_events_exec_handler(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __APP_TIME_EVENTS_H */