#include "inc/service_usb_req.h"
#include "debug/log_modules/log_modules.h"

#include <stdlib.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define PRESSURE_QUE     "Pressure is ok? Continue?"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

usb_request_ctrl_t *service_usb_req_ctrl = NULL;

usb_request_t *ev_presure_req = NULL;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define USB_REQ_ARRAY_SIZE  1

static usb_request_t *service_usb_req_array[USB_REQ_ARRAY_SIZE];

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void usb_requests_init(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void service_usb_req_init(void)
{
    usb_requests_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void usb_requests_init(void)
{
    uint16_t req_num = 0;

    ev_presure_req = usb_request_create();
    usb_request_init(ev_presure_req, PRESSURE_QUE);
    service_usb_req_array[req_num++] = ev_presure_req;

    if(req_num != USB_REQ_ARRAY_SIZE)
    {
        log_error_m(log_controller, 
            "Not equal amount of hangar usb req initalization "
            "/ requests array size! [ %s : %s ]",
            __FILE__, __func__);
    }
    else
    {
        service_usb_req_ctrl = usb_request_ctrl_create();
        usb_request_ctrl_init(service_usb_req_ctrl,
                              service_usb_req_array, req_num);
        log_info_m(log_controller, 
                   "Application hangar usb requests initalize succesfull!");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
