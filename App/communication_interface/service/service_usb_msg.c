#include "inc/service_usb_msg.h"
#include "communication/protocols/usb/inc/usb_msg.h"
#include "debug/log_modules/log_modules.h"
#include <stdlib.h>
//#include "usbd_cdc_if.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define USB_MSG_INIT(msg_ptr, command, clbk) \
					 do{\
					 msg_ptr = usb_msg_create();\
					 usb_msg_init(msg_ptr, command, clbk);\
					 service_usb_msg_array[msg_num++] = msg_ptr;\
					 }while(0);\

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

usb_msg_ctrl_t *service_usb_msg_ctrl = NULL;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
#define MSG_USB_ARRAY_SIZE	1
static usb_msg_t *service_usb_msg_array[MSG_USB_ARRAY_SIZE];

/* commands */
static usb_msg_t *sv_service_usb_cmd_get_states = NULL;

//static char *action_resp_str[3] = { "ack", "refuse", "invalid_value" };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool service_usb_cmd_get_states_clbk(usb_msg_t *msg);
//static void action_response_cmd_usb(action_resp_t action_resp,
//						   			uint32_t order_id);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void usb_messages_init()
{
	uint16_t msg_num = 0;

	/* commands */
	USB_MSG_INIT(sv_service_usb_cmd_get_states, "get_states",
				 service_usb_cmd_get_states_clbk);

	/* init usb msg ctrl */
	if(msg_num != MSG_USB_ARRAY_SIZE)
	{
		log_error_m(log_controller,
					"Not equal amount of declared usb msg array!");
	}
	else
	{
		service_usb_msg_ctrl = usb_msg_ctrl_create();
		usb_msg_ctrl_init(service_usb_msg_ctrl, service_usb_msg_array,
						  msg_num);
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void service_usb_msg_init(void)
{
	usb_messages_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool service_usb_cmd_get_states_clbk(usb_msg_t *msg)
{
	//TODO
	return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

//static void action_response_cmd_usb(action_resp_t action_resp,
//						   uint32_t order_id)
//{
//	if(action_resp >= ACTION_ACCEPTED && action_resp <= ACTION_SKIPPED)
//	{
//		char resp_payload[50];
//		uint16_t len = sprintf(resp_payload, "%s/%ld",
//							  action_resp_str[action_resp-1],
//							  order_id);
//		CDC_Transmit_FS((uint8_t*)resp_payload, len);
//	}
//	else
//	{
//		char resp_payload[50];
//		uint16_t len = sprintf(resp_payload, "%s/%ld",
//						"Invalid action_response value",
//						order_id);
//		CDC_Transmit_FS((uint8_t*)resp_payload, len);
//	}
//}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
