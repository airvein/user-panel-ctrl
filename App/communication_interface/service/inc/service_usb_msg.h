#ifndef __SERVICE_USB_MSG_H
#define __SERVICE_USB_MSG_H

#include "communication/protocols/usb/inc/usb_msg.h"
#include "communication/protocols/usb/inc/usb_msg_controller.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern usb_msg_ctrl_t *service_usb_msg_ctrl;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void service_usb_msg_init();

#endif /* __SERVICE_USB_MSG_H */
