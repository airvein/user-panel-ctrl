#ifndef __HANGAR_MQTT_MSG_H
#define __HANGAR_MQTT_MSG_H

#include "communication/protocols/mqtt/inc/mqtt_msg.h"
#include "communication/protocols/mqtt/inc/mqtt_msg_controller.h"
#include "control/procedure/inc/procedure.h"
#include "settings_app/general/settings_app_procedure.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern mqtt_msg_ctrl_t hangar_mqtt_msg_ctrl;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_mqtt_msg_init(void);

bool hangar_pub_state_cargo_pin(char pin[APP_PROC_CARGO_PIN_SIZE]);
msg_state_t hangar_get_pub_state_cargo_pin(void);

bool hangar_pub_state_view(char *view_str);
msg_state_t hangar_get_pub_state_view(void);

bool hangar_pub_state_user_confirm(user_confirmation_t value);
msg_state_t hangar_get_pub_state_user_confirm(void);

void mqtt_cmd_cargo_pin_response_cb(action_resp_t action_resp, 
								uint32_t order_id);
void mqtt_cmd_view_response_cb(action_resp_t action_resp, 
								uint32_t order_id);
void mqtt_cmd_confirm_view_response_cb(action_resp_t action_resp, 
										uint32_t order_id);

void mqtt_cmd_get_states_response_cb(action_resp_t action_resp,
										uint32_t order_id);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __HANGAR_MQTT_MSG_H */
