#include "inc/hangar_mqtt_msg.h"
#include "communication/interface/hangar/inc/hangar_mqtt_client.h"
#include "control/procedure/inc/procedure.h"
#include "debug/log_modules/log_modules.h"
#include "logic/hangar_heartbeat/inc/hangar_heartbeat.h"
#include "logic/hangar_ctrl_reset/inc/hangar_ctrl_reset.h"
#include "commands/inc/app_mqtt_commands.h"
#include "gui_interface/inc/gui_interface.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define SUB_MSG_INIT(msg_ptr, topic, clbk)	\
	{\
		mqtt_msg_init(msg_ptr, hangar_mqtt_client, QOS_2,\
					topic, clbk);\
		hangar_mqtt_msg_array[msg_num++] = msg_ptr;\
	}

#define PUB_MSG_INIT(msg_ptr, topic)	\
	{\
		mqtt_msg_init(msg_ptr, hangar_mqtt_client, QOS_2,\
					topic, mqtt_msg_no_sub_cb);\
	}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define CMD_STR								MQTT_MODULE_NAME"/cmd/"
#define CMD_CALL_STR						"/call"
#define CMD_RESP_STR						"/resp"

#define STATE_STR							MQTT_MODULE_NAME"/state/"
#define DIAG_STR							MQTT_MODULE_NAME"/diagnostics/"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define TOPIC_SUB_CMD_CARGO_PIN				CMD_STR"cargo_pin"CMD_CALL_STR
#define TOPIC_SUB_CMD_VIEW					CMD_STR"view"CMD_CALL_STR
#define TOPIC_SUB_CMD_CONF_VIEW				CMD_STR"confirmation_view"CMD_CALL_STR
#define TOPIC_SUB_CMD_GET_STATES			CMD_STR"get_states"CMD_CALL_STR

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define TOPIC_PUB_CMD_CARGO_PIN_RESP		CMD_STR"cargo_pin"CMD_RESP_STR
#define TOPIC_PUB_CMD_VIEW_RESP				CMD_STR"view"CMD_RESP_STR
#define TOPIC_PUB_CMD_CONF_VIEW_RESP		CMD_STR"confirmation_view"CMD_RESP_STR
#define TOPIC_PUB_CMD_GET_STATES_RESP		CMD_STR"get_states"CMD_RESP_STR

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define TOPIC_PUB_STATE_CARGO_PIN			STATE_STR"cargo_pin"
#define TOPIC_PUB_STATE_VIEW				STATE_STR"view"
#define TOPIC_PUB_STATE_USER_CONF			STATE_STR"user_confirmation"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define TOPIC_PUB_DIAG_WARNINGS				DIAG_STR"warnings"
#define TOPIC_PUB_DIAG_ERRORS				DIAG_STR"errors"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_msg_ctrl_t hangar_mqtt_msg_ctrl = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
/* subscribe messages array for mqtt msg controller */
#define MSG_ARRAY_SIZE	6
static mqtt_msg_t *hangar_mqtt_msg_array[MSG_ARRAY_SIZE];

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
/* subscribe messages - commands */
static mqtt_msg_t sv_sub_cmd_cargo_pin = { 0 };
static mqtt_msg_t sv_sub_cmd_view = { 0 };
static mqtt_msg_t sv_sub_cmd_conf_view = { 0 };
static mqtt_msg_t sv_sub_cmd_get_states_view = { 0 };
/* commands response */
static mqtt_msg_t sv_pub_cmd_cargo_pin_resp = { 0 };
static mqtt_msg_t sv_pub_cmd_view_resp = { 0 };
static mqtt_msg_t sv_pub_cmd_conf_view_resp = { 0 };
static mqtt_msg_t sv_pub_cmd_get_states_resp = { 0 };
/* states */
static mqtt_msg_t sv_pub_state_cargo_pin = { 0 };
static mqtt_msg_t sv_pub_state_view = { 0 };
static mqtt_msg_t sv_pub_state_user_conf = { 0 };
/* diagnostics */
static mqtt_msg_t sv_pub_diagnostic_warnings = { 0 };
static mqtt_msg_t sv_pub_diagnostic_errors = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static char *action_resp_str[3] = { "ack", "refuse", "invalid_value" };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void action_response_mqtt_publish(mqtt_msg_t *msg,
										action_resp_t action_resp,
										uint32_t order_id);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_sub_cmd_cargo_pin_clbk(mqtt_msg_t *msg);
static bool hangar_sub_cmd_view_clbk(mqtt_msg_t *msg);
static bool hangar_sub_cmd_conf_view_clbk(mqtt_msg_t *msg);
static bool hangar_sub_cmd_get_states_clbk(mqtt_msg_t *msg);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void messages_init()
{
	uint16_t msg_num = 0;

	/* subscribe messages - commands [ 3 ]*/
	SUB_MSG_INIT(&sv_sub_cmd_cargo_pin, TOPIC_SUB_CMD_CARGO_PIN, 
				hangar_sub_cmd_cargo_pin_clbk);
	SUB_MSG_INIT(&sv_sub_cmd_view, TOPIC_SUB_CMD_VIEW, 
				hangar_sub_cmd_view_clbk);
	SUB_MSG_INIT(&sv_sub_cmd_conf_view, TOPIC_SUB_CMD_CONF_VIEW, 
				hangar_sub_cmd_conf_view_clbk);
	SUB_MSG_INIT(&sv_sub_cmd_get_states_view, TOPIC_SUB_CMD_GET_STATES, 
				hangar_sub_cmd_get_states_clbk);

	hangar_heartbeat_msg_init();
	hangar_mqtt_msg_array[msg_num++] = &ev_msg_hangar_heartbeat;	
	/* publish messages - commands response[ 2 ]*/
	PUB_MSG_INIT(&sv_pub_cmd_cargo_pin_resp, TOPIC_PUB_CMD_CARGO_PIN_RESP);
	PUB_MSG_INIT(&sv_pub_cmd_view_resp, TOPIC_PUB_CMD_VIEW_RESP);
	PUB_MSG_INIT(&sv_pub_cmd_conf_view_resp, TOPIC_PUB_CMD_CONF_VIEW_RESP);
	PUB_MSG_INIT(&sv_pub_cmd_get_states_resp, TOPIC_PUB_CMD_GET_STATES_RESP);
	/* publish messages - states[ 2 ]*/
	PUB_MSG_INIT(&sv_pub_state_cargo_pin, TOPIC_PUB_STATE_CARGO_PIN);
	PUB_MSG_INIT(&sv_pub_state_view, TOPIC_PUB_STATE_VIEW);
	PUB_MSG_INIT(&sv_pub_state_user_conf, TOPIC_PUB_STATE_USER_CONF);
	/* publish messages - diagnostics[ 2 ]*/
	PUB_MSG_INIT(&sv_pub_diagnostic_warnings, TOPIC_PUB_DIAG_WARNINGS);
	PUB_MSG_INIT(&sv_pub_diagnostic_errors, TOPIC_PUB_DIAG_ERRORS);	

	hangar_ctrl_reset_msg_init();
	hangar_mqtt_msg_array[msg_num++] = &ev_msg_hangar_ctrl_reset;

	if(msg_num != MSG_ARRAY_SIZE || msg_num == 0)
	{
		log_error_m(log_controller, 
					"Not equal amount of declared mqtt msg array!");
		while(1);
	}
	else
	{
    	mqtt_msg_ctrl_init(&hangar_mqtt_msg_ctrl, hangar_mqtt_msg_array,
						   msg_num);
		log_info_m(log_controller, 
                  "Application hangar mqtt requests initalize succesfull!");
	}	
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_mqtt_msg_init(void)
{
	messages_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_pub_state_cargo_pin(char pin[APP_PROC_CARGO_PIN_SIZE])
{
	char pin_str[APP_PROC_CARGO_PIN_SIZE*2] = { 0 };

	if(strcmp(pin, "tout") == 0)
	{
		sprintf(pin_str, "timeout");
	}
	else
	{
		for(int pin_id = 0; pin_id < APP_PROC_CARGO_PIN_SIZE; pin_id++)
    	{
        	if(pin_id == (APP_PROC_CARGO_PIN_SIZE-1))
        	{
				sprintf(pin_str+strlen(pin_str), "%c", pin[pin_id]);
			}
			else
			{
				sprintf(pin_str+strlen(pin_str), "%c,", pin[pin_id]);
			}
    	}
	}
	
	return mqtt_msg_publish(&sv_pub_state_cargo_pin, pin_str);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

msg_state_t hangar_get_pub_state_auth_pin(void)
{
	return sv_pub_state_cargo_pin.state;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_pub_state_view(char *view_str)
{
	return mqtt_msg_publish(&sv_pub_state_view, view_str);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

msg_state_t hangar_get_pub_state_view(void)
{
	return sv_pub_state_view.state;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_pub_state_user_confirm(user_confirmation_t value)
{	
	char *user_confirm_str[] = 
	{ 
		[USER_CONFIRM_FALSE] = "false",
	 	[USER_CONFIRM_TRUE] = "true", 
		[USER_CONFIRM_TIMEOUT] = "timeout" 
	};
	
	if(value >= USER_CONFIRM_FALSE && value <= USER_CONFIRM_TIMEOUT)
	{
		return mqtt_msg_publish(&sv_pub_state_user_conf, 
								user_confirm_str[value]);
	}

	return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

msg_state_t hangar_get_pub_state_user_confirm(void)
{
	return sv_pub_state_user_conf.state;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void mqtt_cmd_cargo_pin_response_cb(action_resp_t action_resp, 
								uint32_t order_id)
{
	action_response_mqtt_publish(&sv_pub_cmd_cargo_pin_resp, action_resp, 
								 order_id);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void mqtt_cmd_view_response_cb(action_resp_t action_resp, 
								uint32_t order_id)
{
	action_response_mqtt_publish(&sv_pub_cmd_view_resp, action_resp, order_id);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void mqtt_cmd_confirm_view_response_cb(action_resp_t action_resp, 
										uint32_t order_id)
{
	action_response_mqtt_publish(&sv_pub_cmd_conf_view_resp, 
								action_resp, order_id);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void mqtt_cmd_get_states_response_cb(action_resp_t action_resp,
										uint32_t order_id)
{
	action_response_mqtt_publish(&sv_pub_cmd_get_states_resp, 
								action_resp, order_id);
}	

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_sub_cmd_cargo_pin_clbk(mqtt_msg_t *msg)
{
	app_mqtt_cmd_cargo_pin_call(msg->payload);
	return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_sub_cmd_view_clbk(mqtt_msg_t *msg)
{
	app_mqtt_cmd_view_call(msg->payload);
	return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_sub_cmd_conf_view_clbk(mqtt_msg_t *msg)
{
	app_mqtt_cmd_confirm_view_call(msg->payload);
	return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_sub_cmd_get_states_clbk(mqtt_msg_t *msg)
{
	app_mqtt_cmd_get_states_call(msg->payload);	
	return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void action_response_mqtt_publish(mqtt_msg_t *msg,
										action_resp_t action_resp,
										uint32_t order_id)
{
	if(action_resp >= ACTION_ACCEPTED && action_resp <= ACTION_SKIPPED)
	{
		char resp_payload[50];
		sprintf(resp_payload, "%s/%ld", action_resp_str[action_resp-1],
									    order_id);
		mqtt_msg_publish(msg, resp_payload);
	}
	else
	{
		log_warning_m(log_controller, "Invalid action_response value "
					  "for action response mqtt [ %s ] publish!",
					  msg->topic_path);
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|