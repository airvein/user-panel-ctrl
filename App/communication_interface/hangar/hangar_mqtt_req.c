#include "inc/hangar_mqtt_req.h"
#include "communication/interface/hangar/inc/hangar_mqtt_client.h"
#include "logic/hangar_mqtt_req/hangar_timestamp/inc/hangar_req_timestamp.h"
#include "debug/log_modules/log_modules.h"
#include "peripherals_core/rtc/inc/rtc.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define HANGAR_REQ_INIT(ptr, name, req_topic, resp_topic, resp_topic_cb)\
	do{\
		mqtt_request_init(ptr, name, hangar_mqtt_client);\
		mqtt_request_req_msg_init(ptr, req_topic);\
		mqtt_request_resp_msg_init(ptr, resp_topic, resp_topic_cb);\
		hangar_mqtt_req_array[req_num++] = ptr;\
	}while(0);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_request_ctrl_t hangar_mqtt_req_ctrl = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define MQTT_REQ_ARRAY_SIZE 1

static mqtt_request_t *hangar_mqtt_req_array[MQTT_REQ_ARRAY_SIZE];

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void requests_init(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_mqtt_req_init(void)
{
	requests_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void requests_init(void)
{
	uint16_t req_num = 0;

	hangar_req_timestamp_init();
	hangar_mqtt_req_array[req_num++] = &ev_mqtt_req_timestamp;

	if(req_num != MQTT_REQ_ARRAY_SIZE || req_num == 0)
	{
		log_error_m(log_controller, 
                    "Not equal amount of hangar mqtt req initalization "
                    "/ requests array size! [ %s : %s ]",
                    __FILE__, __func__);
		while(1);
	}
	else
	{
		mqtt_request_ctrl_init(&hangar_mqtt_req_ctrl, 
							   hangar_mqtt_req_array, req_num);

		log_info_m(log_controller, 
                  "Application hangar mqtt requests initalize succesfull!");
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|