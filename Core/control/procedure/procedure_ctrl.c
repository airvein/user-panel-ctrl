#include "control/procedure/inc/procedure_ctrl.h"
#include "debug/log_modules/log_modules.h"

#include <stdlib.h>
#include <string.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_procedure_ctrl, "%s : %s", __FILE__,\
                                    __func__))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(procedure_ctrl_t *procedure_ctrl,
                                       procedure_t **procedure_array,
                                       uint16_t procedure_array_size);

static bool is_procedure_ctrl_init(procedure_ctrl_t *procedure_ctrl);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

procedure_ctrl_t *procedure_ctrl_create(void)
{
    return (procedure_ctrl_t*)malloc(sizeof(procedure_ctrl_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool procedure_ctrl_init(procedure_ctrl_t *procedure_ctrl,
                             procedure_t **procedure_array, 
                             uint16_t procedure_array_size)
{
    LOG_DEBUG_FUNC();
    if(is_correct_init_parameters(procedure_ctrl, procedure_array, 
                                  procedure_array_size) == false)
    {
        log_error_m(log_procedure_ctrl,
                    "Incorrect init parameters!");
        return false;
    }

    procedure_ctrl->procedure_array = procedure_array;
    procedure_ctrl->procedure_num = procedure_array_size;

    procedure_ctrl->is_init = true;

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void procedure_ctrl_handler(procedure_ctrl_t *procedure_ctrl)
{
    if(is_procedure_ctrl_init(procedure_ctrl) == true)
    {
        for(int proc_id = 0; proc_id < procedure_ctrl->procedure_num;
        		proc_id++)
        {
            procedure_handler(procedure_ctrl->procedure_array[proc_id]);
        }
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(procedure_ctrl_t *procedure_ctrl,
                                       procedure_t **procedure_array,
                                       uint16_t procedure_array_size)
{
    return procedure_ctrl != NULL && procedure_array != NULL 
            && procedure_array_size > 0;
}                                        

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_procedure_ctrl_init(procedure_ctrl_t *procedure_ctrl)
{
    return procedure_ctrl != NULL && procedure_ctrl->is_init == true;
}
