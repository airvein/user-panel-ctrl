#include "inc/hangar_heartbeat.h"
#include "communication/interface/hangar/inc/hangar_mqtt_client.h"
#include "debug/log_modules/log_modules.h"
#include <string.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_msg_t ev_msg_hangar_heartbeat = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static mqtt_msg_t sv_msg_hangar_heartbeat_resp = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_heartbeat_response_callback(mqtt_msg_t *mqtt_msg);

static bool is_correct_heartbeat_request(char *mqtt_payload);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_heartbeat_msg_init(void)
{
    mqtt_msg_init(&ev_msg_hangar_heartbeat, hangar_mqtt_client, QOS_2,
                  HANGAR_HEARTBEAT_MQTT_TOPIC_SUB, 
                  hangar_heartbeat_response_callback);

    mqtt_msg_init(&sv_msg_hangar_heartbeat_resp, hangar_mqtt_client, QOS_2,
                  HANGAR_HEARTBEAT_MQTT_TOPIC_PUB,
                  mqtt_msg_no_sub_cb);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_heartbeat_response_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, "hangar heartbeat response");

    if(is_correct_heartbeat_request(mqtt_msg->payload) == true)
    {
        mqtt_msg_publish(&sv_msg_hangar_heartbeat_resp, 
                         HANGAR_HEARTBEAT_RESPONSE_PAYLOAD);
    }
    else
    {
        log_warning_m(log_controller, "Incorrect heartbeat req payload");
    }
    
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_heartbeat_request(char *mqtt_payload)
{
    return strcmp(mqtt_payload, "tick") == 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|