#include "inc/hangar_get_plc_io.h"
#include "communication/interface/hangar/inc/hangar_mqtt_client.h"
#include "debug/log_modules/log_modules.h"

#include "peripherals_core/plc_module/inc/plc_module.h"

#include <string.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_msg_t *ev_msg_hangar_get_plc_io = NULL;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static mqtt_msg_t *sv_msg_hangar_get_plc_io_resp = NULL;
static mqtt_msg_t *sv_msg_hangar_input_read = NULL;
static mqtt_msg_t *sv_msg_hangar_output_read = NULL;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_get_plc_io_response_callback(mqtt_msg_t *mqtt_msg);

static bool is_correct_get_plc_io_request(char *mqtt_payload);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_get_plc_io_msg_init()
{
    ev_msg_hangar_get_plc_io = mqtt_msg_create();
    mqtt_msg_init(ev_msg_hangar_get_plc_io, hangar_mqtt_client, QOS_2,
                  HANGAR_GET_PLC_IO_MQTT_TOPIC_SUB,
                  hangar_get_plc_io_response_callback);

    sv_msg_hangar_get_plc_io_resp = mqtt_msg_create();
    mqtt_msg_init(sv_msg_hangar_get_plc_io_resp, hangar_mqtt_client, QOS_2,
                  HANGAR_GET_PLC_IO_MQTT_TOPIC_PUB_ACK,
                  mqtt_msg_no_sub_cb);

    sv_msg_hangar_input_read = mqtt_msg_create();
    mqtt_msg_init(sv_msg_hangar_input_read, hangar_mqtt_client, QOS_2,
                  HANGAR_GET_PLC_IO_MQTT_TOPIC_PUB_INPUTS,
                  mqtt_msg_no_sub_cb);

    sv_msg_hangar_output_read = mqtt_msg_create();
    mqtt_msg_init(sv_msg_hangar_output_read, hangar_mqtt_client, QOS_2,
                  HANGAR_GET_PLC_IO_MQTT_TOPIC_PUB_OUTPUTS,
                  mqtt_msg_no_sub_cb);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_get_plc_io_response_callback(mqtt_msg_t *mqtt_msg)
{   
    log_debug_m(log_controller, "hangar get plc io resp");
    
    if(is_correct_get_plc_io_request(mqtt_msg->payload) == true)
    {
        char ack_resp[20];
        char io_plc[20] = {0};

        sprintf(ack_resp, "ack/%ld", mqtt_msg->order_id);
        mqtt_msg_publish(sv_msg_hangar_get_plc_io_resp, ack_resp);
        
        if(ev_plc_module != NULL)
        {
            if(ev_plc_module->state == PLC_STATE_OK)
            {
                for(int in = 0; in < 8; in++)
                {
                    if(in < 7)
                    {
                        sprintf(io_plc+strlen(io_plc), "%d | ", 
                        plc_module_get_input_state(ev_plc_module, in));
                    }
                    else
                    {
                        sprintf(io_plc+strlen(io_plc), "%d", 
                        plc_module_get_input_state(ev_plc_module, in));
                    }   
                    
                }
                mqtt_msg_publish(sv_msg_hangar_input_read, io_plc);
                memset(io_plc, 0, 20);

                for(int out = 0; out < 8; out++)
                {
                    if(out < 7)
                    {
                        sprintf(io_plc+strlen(io_plc), "%d | ", 
                        plc_module_get_output_state(ev_plc_module, out));
                    }
                    else
                    {
                        sprintf(io_plc+strlen(io_plc), "%d", 
                        plc_module_get_output_state(ev_plc_module, out));
                    }                    
                }
                mqtt_msg_publish(sv_msg_hangar_output_read, io_plc);
            }
            else
            {
                sprintf(io_plc, "state_err_%d", ev_plc_module->state);
                mqtt_msg_publish(sv_msg_hangar_output_read, io_plc);
            }
        }
        else
        {
            sprintf(io_plc, "module_not_init");
            mqtt_msg_publish(sv_msg_hangar_output_read, io_plc);
        }
    }
    else
    {
        log_warning_m(log_controller, "Incorrect get_plc_io request!");
    }    
    
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_get_plc_io_request(char *mqtt_payload)
{
    return strcmp(mqtt_payload, "get") == 0;
}