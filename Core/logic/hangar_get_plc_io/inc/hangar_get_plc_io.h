#ifndef __HANGAR_GET_PLC_IO_H
#define __HANGAR_GET_PLC_IO_H

#include "communication/protocols/mqtt/inc/mqtt_msg.h"
#include "settings_app/communication/settings_mqtt.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define HANGAR_GET_PLC_IO_MQTT_TOPIC_SUB            \
                                MQTT_MODULE_NAME"/cmd/get_plc_io/call"

#define HANGAR_GET_PLC_IO_MQTT_TOPIC_PUB_ACK        \
                                MQTT_MODULE_NAME"/cmd/get_plc_io/resp"

#define HANGAR_GET_PLC_IO_MQTT_TOPIC_PUB_INPUTS     \
                                MQTT_MODULE_NAME"/plc/input/read"

#define HANGAR_GET_PLC_IO_MQTT_TOPIC_PUB_OUTPUTS    \
                                MQTT_MODULE_NAME"/plc/output/read"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern mqtt_msg_t *ev_msg_hangar_get_plc_io;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_get_plc_io_msg_init();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __HANGAR_GET_PLC_IO_H */