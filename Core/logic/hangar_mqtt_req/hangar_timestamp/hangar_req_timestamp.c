#include "inc/hangar_req_timestamp.h"
#include "communication/interface/hangar/inc/hangar_mqtt_client.h"
#include "peripherals_core/rtc/inc/rtc.h"
#include "debug/log_modules/log_modules.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define TIMESTAMP_REQ_TOPIC     "time/get"
#define TIMESTAMP_REQ_PAYLOAD   "get"
#define TIMESTAMP_RESP_TOPIC    "time/val"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_request_t ev_mqtt_req_timestamp = { 0 };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool send_req = false;
static bool timestamp_update = false;
static bool timestamp_unsubscribe = false;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_req_mqtt_timestamp_cb(mqtt_msg_t *msg);
static bool is_request_response_msg_subscribed(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_timestamp_init(void)
{
    mqtt_request_init(&ev_mqtt_req_timestamp, "hangar-timestamp", 
					  hangar_mqtt_client);
	mqtt_request_req_msg_init(&ev_mqtt_req_timestamp, TIMESTAMP_REQ_TOPIC);
	mqtt_request_resp_msg_init(&ev_mqtt_req_timestamp, TIMESTAMP_RESP_TOPIC,
							   hangar_req_mqtt_timestamp_cb);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_timestamp_is_update(void)
{
	return timestamp_update;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_timestamp_send_init_update(void)
{
	if(send_req == false && hangar_mqtt_is_connected() == true
		&& is_request_response_msg_subscribed() == true)
	{
		mqtt_request_send_req(&ev_mqtt_req_timestamp, TIMESTAMP_REQ_PAYLOAD);
		send_req = true;
	}
	else if(timestamp_unsubscribe == false && timestamp_update == true)
	{
		mqtt_msg_unsubscribe(&ev_mqtt_req_timestamp.resp_msg);
		timestamp_unsubscribe = true;
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_req_mqtt_timestamp_cb(mqtt_msg_t *msg)
{
	if(msg == NULL)
	{
		log_error_m(log_mqtt_request, 
					"Incoming req response cb with msg resp null object");
		return false;
	}

	log_debug_m(log_mqtt_request, 
				"Incoming req response on [ %s ] with [ %s ] payload",
				msg->topic_path, msg->payload);
	
	uint32_t tmp_ts = atoi(msg->payload);
	if(tmp_ts > 0)
	{
		log_info_m(log_controller, 
					"Update system timestamp [ %d ] => [ %d ] succesfull!",
					ev_system_timestamp, tmp_ts);
		ev_system_timestamp = tmp_ts;
		timestamp_update = true;
	}
	else
	{
		log_warning_m(log_controller, "Update system timestamp value error!");
	}
	return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_request_response_msg_subscribed(void)
{
	return ev_mqtt_req_timestamp.resp_msg.state == MSG_STATE_SUBSCRIBED;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|