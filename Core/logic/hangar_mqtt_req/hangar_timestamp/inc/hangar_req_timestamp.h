#ifndef __HANGAR_REQ_TIMESTAMP_H
#define __HANGAR_REQ_TIMESTAMP_H

#include "communication/protocols/mqtt/inc/mqtt_request.h"
#include "settings_app/communication/settings_mqtt.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern mqtt_request_t ev_mqtt_req_timestamp;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_timestamp_init(void);

bool hangar_req_timestamp_is_update(void);

void hangar_req_timestamp_send_init_update(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __HANGAR_REQ_TIMESTAMP_H */