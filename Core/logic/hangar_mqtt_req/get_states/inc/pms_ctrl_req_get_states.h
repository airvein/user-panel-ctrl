#ifndef __PMS_CTRL_REQ_GET_STATES_H
#define __PMS_CTRL_REQ_GET_STATES_H

#include "settings_app/logic/settings_hangar_req_common.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define REQ_PMS_NAME            "pms-ctrl-get-states"

#define REQ_PMS_CTRL_GET_STATES_REQ_TOPIC \
                                "pms_ctrl/cmd/get_states/call"

#define REQ_PMS_CTRL_GET_STATES_REQ_PAYLOAD \
                                "get"

#define REQ_PMS_CTRL_GET_STATES_RESP_TOPIC \
                                "pms_ctrl/cmd/get_states/resp"                                

#define PMS_CTRL_STATE_DISABLE    0
#define PMS_CTRL_STATE_ENABLE     1

#define PMS_CTRL_PRESSURE_MQTT_TOPIC    "pms_ctrl/state/pressure"
#define PMS_CTRL_CHARG_PWR_MQTT_TOPIC   "pms_ctrl/state/chargers_power"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern mqtt_request_t *req_pms_ctrl_get_states; 

extern mqtt_msg_t *msg_pms_ctrl_state_pressure;
extern mqtt_msg_t *msg_pms_ctrl_state_chargers_power;

extern uint8_t ev_pms_ctrl_pressure;
extern uint8_t ev_pms_ctrl_chargers_power;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_pms_ctrl_get_states_init(void);

void hangar_req_pms_ctrl_get_states_send(void);

bool hangar_req_pms_ctrl_is_pressure_update(void);

bool hangar_req_pms_ctrl_is_chargers_power_update(void);

bool hangar_req_pms_ctrl_is_all_states_update(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __PMS_CTRL_REQ_GET_STATES_H */