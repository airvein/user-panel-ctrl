#ifndef __ROOF_CTRL_REQ_GET_STATES_H
#define __ROOF_CTRL_REQ_GET_STATES_H

#include "settings_app/logic/settings_hangar_req_common.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define REQ_ROOF_NAME           "roof_ctrl-get-states"

#define REQ_ROOF_CTRL_GET_STATES_REQ_TOPIC \
                                "roof_ctrl/cmd/get_states/call"

#define REQ_ROOF_CTRL_GET_STATES_REQ_PAYLOAD \
                                "get"

#define REQ_ROOF_CTRL_GET_STATES_RESP_TOPIC \
                                "roof_ctrl/cmd/get_states/resp"                                

#define ROOF_CTRL_ROOF_CLOSE    0
#define ROOF_CTRL_ROOF_IDLE     1
#define ROOF_CTRL_ROOF_OPEN     2

#define ROOF_CTRL_ROOF_MQTT_TOPIC   "roof_ctrl/state/roof"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern mqtt_request_t *req_roof_ctrl_get_states; 

extern mqtt_msg_t *msg_roof_ctrl_state_roof;

extern uint8_t ev_roof_ctrl_roof;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_roof_ctrl_get_states_init(void);

void hangar_req_roof_ctrl_get_states_send(void);

bool hangar_req_roof_ctrl_is_roof_update(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __ROOF_CTRL_REQ_GET_STATES_H */