#ifndef __CLIMATE_CTRL_REQ_GET_STATES_H
#define __CLIMATE_CTRL_REQ_GET_STATES_H

#include "settings_app/logic/settings_hangar_req_common.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define REQ_CLIM_NAME               "clim-ctrl-get-states"

#define REQ_CLIM_CTRL_GET_STATES_REQ_TOPIC  \
                                    "climate_ctrl/cmd/get_states/call"
                                    
#define REQ_CLIM_CTRL_GET_STATES_REQ_PAYLOAD \
                                    "get"

#define REQ_CLIM_CTRL_GET_STATES_RESP_TOPIC \
                                    "climate_ctrl/cmd/get_states/resp"

#define CLIM_CTRL_TYPE_MANUAL               0
#define CLIM_CTRL_TYPE_AUTO                 1

#define CLIM_CTRL_STATE_DISABLE             0
#define CLIM_CTRL_STATE_ENABLE              1

#define CLIM_CTRL_TYPE_MQTT_TOPIC           "climate_ctrl/state/type"
#define CLIM_CTRL_AC_MQTT_TOPIC             "climate_ctrl/state/ac"
#define CLIM_CTRL_HANG_HEATER_MQTT_TOPIC    "climate_ctrl/state/hangar_heater"
#define CLIM_CTRL_ROOF_HEATER_MQTT_TOPIC    "climate_ctrl/state/roof_heater"
#define CLIM_CTRL_FAN_MQTT_TOPIC            "climate_ctrl/state/fan"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern mqtt_request_t *req_clim_ctrl_get_states; 

extern mqtt_msg_t *msg_clim_ctrl_state_type;
extern mqtt_msg_t *msg_clim_ctrl_state_ac;
extern mqtt_msg_t *msg_clim_ctrl_state_hang_heater;
extern mqtt_msg_t *msg_clim_ctrl_state_roof_heater;
extern mqtt_msg_t *msg_clim_ctrl_state_fan;

extern uint8_t ev_clim_ctrl_type;     
extern uint8_t ev_clim_ctrl_ac;       
extern uint8_t ev_clim_ctrl_hang_heat;
extern uint8_t ev_clim_ctrl_roof_heat;
extern uint8_t ev_clim_ctrl_fan;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_clim_ctrl_get_states_init(void);

void hangar_req_clim_ctrl_get_states_send(void);

bool hangar_req_clim_ctrl_is_type_update(void);

bool hangar_req_clim_ctrl_is_ac_update(void);

bool hangar_req_clim_ctrl_is_hangar_heater_update(void);

bool hangar_req_clim_ctrl_is_roof_heater_update(void);

bool hangar_req_clim_ctrl_is_fan_update(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __CLIMATE_CTRL_REQ_GET_STATES_H */