#ifndef __CARGO_CTRL_REQ_GET_STATES_H
#define __CARGO_CTRL_REQ_GET_STATES_H

#include "settings_app/logic/settings_hangar_req_common.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define REQ_CARGO_NAME           "cargo_ctrl-get-states"

#define REQ_CARGO_CTRL_GET_STATES_REQ_TOPIC \
                                "cargo_ctrl/cmd/get_states/call"

#define REQ_CARGO_CTRL_GET_STATES_REQ_PAYLOAD \
                                "get"

#define REQ_CARGO_CTRL_GET_STATES_RESP_TOPIC \
                                "cargo_ctrl/cmd/get_states/resp"                                

#define CARGO_CTRL_WINDOW_CLOSE     0
#define CARGO_CTRL_WINDOW_IDLE      1
#define CARGO_CTRL_WINDOW_OPEN      2

#define CARGO_CTRL_CARGO_SHIFT_BASE       0
#define CARGO_CTRL_CARGO_SHIFT_IDLE       1
#define CARGO_CTRL_CARGO_SHIFT_WORK       2

#define CARGO_CTRL_WINDOW_MQTT_TOPIC        "cargo_ctrl/state/window"
#define CARGO_CTRL_CARGO_SHIFT_MQTT_TOPIC   "cargo_ctrl/state/cargo_shift"
#define CARGO_CTRL_CARGO_WEIGHT_MQTT_TOPIC  "cargo_ctrl/state/cargo_weight"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern mqtt_request_t *req_cargo_ctrl_get_states; 

extern mqtt_msg_t *msg_cargo_ctrl_state_window;
extern mqtt_msg_t *msg_cargo_ctrl_state_cargo_shift;
extern mqtt_msg_t *msg_cargo_ctrl_state_cargo_weight;

extern uint8_t ev_cargo_ctrl_window;
extern uint8_t ev_cargo_ctrl_cargo_shift;
extern uint8_t ev_cargo_ctrl_cargo_weight; /* todo: typ zmiennej dla wagi*/

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_cargo_ctrl_get_states_init(void);

void hangar_req_cargo_ctrl_get_states_send(void);

bool hangar_req_cargo_ctrl_is_window_update(void);

bool hangar_req_cargo_ctrl_is_cargo_shift_update(void);

bool hangar_req_cargo_ctrl_is_cargo_weight_update(void);

bool hangar_req_cargo_ctrl_is_all_states_update(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __CARGO_CTRL_REQ_GET_STATES_H */