#include "inc/cargo_ctrl_req_get_states.h"

#include "debug/log_modules/log_modules.h"

#include <string.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_request_t *req_cargo_ctrl_get_states = NULL; 

mqtt_msg_t *msg_cargo_ctrl_state_window = NULL;
mqtt_msg_t *msg_cargo_ctrl_state_cargo_shift = NULL;
mqtt_msg_t *msg_cargo_ctrl_state_cargo_weight = NULL;

uint8_t ev_cargo_ctrl_window = CARGO_CTRL_WINDOW_CLOSE;
uint8_t ev_cargo_ctrl_cargo_shift = CARGO_CTRL_CARGO_SHIFT_BASE;
uint8_t ev_cargo_ctrl_cargo_weight = 0; /* todo: typ zmiennej dla wagi*/

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

enum 
{    
    WAIT_FOR_UPDATE = 0,  

    WINDOW_UPDATE = 1,
    CARGO_SHIFT_UPDATE = 2,
    CARGO_WEIGHT_UPDATE = 4,

    ALL_UPDATE = WINDOW_UPDATE | CARGO_SHIFT_UPDATE | CARGO_WEIGHT_UPDATE
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool request_ack = false;

static uint8_t states_update_flag = WAIT_FOR_UPDATE;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void req_cargo_ctrl_get_states_init(void);

static bool req_cargo_ctrl_response_callback(mqtt_msg_t *mqtt_msg);

static bool is_request_ack(char *resp_payload);


static void msg_cargo_ctrl_state_window_init(void);

static bool msg_cargo_ctrl_state_window_callback(mqtt_msg_t *mqtt_msg);

static void update_window_state(char *msg_payload);


static void msg_cargo_ctrl_state_cargo_shift_init(void);

static bool msg_cargo_ctrl_state_cargo_shift_callback(mqtt_msg_t *mqtt_msg);

static void update_cargo_shift_state(char *msg_payload);


static void msg_cargo_ctrl_state_cargo_weight_init(void);

static bool msg_cargo_ctrl_state_cargo_weight_callback(mqtt_msg_t *mqtt_msg);

static void update_cargo_weight_state(char *msg_payload);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_cargo_ctrl_get_states_init(void)
{
    req_cargo_ctrl_get_states_init();

    msg_cargo_ctrl_state_window_init();
    msg_cargo_ctrl_state_cargo_shift_init();
    msg_cargo_ctrl_state_cargo_weight_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_cargo_ctrl_get_states_send(void)
{
    request_ack = false;
    states_update_flag = WAIT_FOR_UPDATE;

    mqtt_request_send_req(req_cargo_ctrl_get_states, 
                          REQ_CARGO_CTRL_GET_STATES_REQ_PAYLOAD);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_cargo_ctrl_is_window_update(void)
{
    return request_ack == true && states_update_flag & WINDOW_UPDATE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_cargo_ctrl_is_cargo_shift_update(void)
{
    return request_ack == true && states_update_flag & CARGO_SHIFT_UPDATE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_cargo_ctrl_is_cargo_weight_update(void)
{
    return request_ack == true && states_update_flag & CARGO_WEIGHT_UPDATE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_cargo_ctrl_is_all_states_update(void)
{
    return request_ack == true && states_update_flag == ALL_UPDATE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void req_cargo_ctrl_get_states_init(void)
{
    req_cargo_ctrl_get_states = mqtt_request_create();
    
    mqtt_request_init(req_cargo_ctrl_get_states, REQ_CARGO_NAME, 
                      hangar_mqtt_client);
    
    mqtt_request_req_msg_init(req_cargo_ctrl_get_states, 
                              REQ_CARGO_CTRL_GET_STATES_REQ_TOPIC);
    
    mqtt_request_resp_msg_init(req_cargo_ctrl_get_states,
                               REQ_CARGO_CTRL_GET_STATES_RESP_TOPIC,
                               req_cargo_ctrl_response_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool req_cargo_ctrl_response_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "cargo_ctrl [ get_states ] request resp [ %s ]",
                mqtt_msg->payload);

    if(is_request_ack(mqtt_msg->payload) == true)
    {
        request_ack = true;
    }

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_request_ack(char *resp_payload)
{
    return strstr(resp_payload, REQ_ACK_STR);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void msg_cargo_ctrl_state_window_init(void)
{
    msg_cargo_ctrl_state_window = mqtt_msg_create();
    mqtt_msg_init(msg_cargo_ctrl_state_window, hangar_mqtt_client, QOS_2, 
                  CARGO_CTRL_WINDOW_MQTT_TOPIC, 
                  msg_cargo_ctrl_state_window_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool msg_cargo_ctrl_state_window_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "[ %s ] update payload [ %s ]",
                mqtt_msg->topic_path,
                mqtt_msg->payload);

    update_window_state(mqtt_msg->payload);    

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void update_window_state(char *msg_payload)
{
    if(strcmp(msg_payload, "open") == 0)
    {
        log_debug_m(log_controller, 
                    "cargo_ctrl [ window ] update to open");
        ev_cargo_ctrl_window = CARGO_CTRL_WINDOW_OPEN;
        states_update_flag = states_update_flag | WINDOW_UPDATE;
    }
    else if(strcmp(msg_payload, "idle") == 0)
    {
        log_debug_m(log_controller, 
                    "cargo_ctrl [ window ] update to idle");
        ev_cargo_ctrl_window = CARGO_CTRL_WINDOW_IDLE;
        states_update_flag = states_update_flag | WINDOW_UPDATE;
    }
    else if(strcmp(msg_payload, "close") == 0)
    {
        log_debug_m(log_controller, 
                    "cargo_ctrl [ window ] update to close");
        ev_cargo_ctrl_window = CARGO_CTRL_WINDOW_CLOSE;
        states_update_flag = states_update_flag | WINDOW_UPDATE;
    }
    else
    {
        log_warning_m(log_controller, 
                      "cargo_ctrl [ window ] update invalid value");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void msg_cargo_ctrl_state_cargo_shift_init(void)
{
    msg_cargo_ctrl_state_cargo_shift = mqtt_msg_create();
    mqtt_msg_init(msg_cargo_ctrl_state_cargo_shift, hangar_mqtt_client, QOS_2, 
                  CARGO_CTRL_CARGO_SHIFT_MQTT_TOPIC, 
                  msg_cargo_ctrl_state_cargo_shift_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool msg_cargo_ctrl_state_cargo_shift_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "[ %s ] update payload [ %s ]",
                mqtt_msg->topic_path,
                mqtt_msg->payload);

    update_cargo_shift_state(mqtt_msg->payload);    

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void update_cargo_shift_state(char *msg_payload)
{
    if(strcmp(msg_payload, "base") == 0)
    {
        log_debug_m(log_controller, 
                    "cargo_ctrl [ cargo_shift ] update to base");
        ev_cargo_ctrl_cargo_shift = CARGO_CTRL_CARGO_SHIFT_BASE;
        states_update_flag = states_update_flag | CARGO_SHIFT_UPDATE;
    }
    else if(strcmp(msg_payload, "idle") == 0)
    {
        log_debug_m(log_controller, 
                    "cargo_ctrl [ cargo_shift ] update to idle");
        ev_cargo_ctrl_cargo_shift = CARGO_CTRL_CARGO_SHIFT_IDLE;
        states_update_flag = states_update_flag | CARGO_SHIFT_UPDATE;
    }
    else if(strcmp(msg_payload, "work") == 0)
    {
        log_debug_m(log_controller, 
                    "cargo_ctrl [ cargo_shift ] update to work");
        ev_cargo_ctrl_cargo_shift = CARGO_CTRL_CARGO_SHIFT_WORK;
        states_update_flag = states_update_flag | CARGO_SHIFT_UPDATE;
    }
    else
    {
        log_warning_m(log_controller, 
                      "cargo_ctrl [ cargo_shift ] update invalid value");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void msg_cargo_ctrl_state_cargo_weight_init(void)
{
    msg_cargo_ctrl_state_cargo_weight = mqtt_msg_create();
    mqtt_msg_init(msg_cargo_ctrl_state_cargo_weight, hangar_mqtt_client, QOS_2,
                  CARGO_CTRL_CARGO_WEIGHT_MQTT_TOPIC, 
                  msg_cargo_ctrl_state_cargo_weight_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool msg_cargo_ctrl_state_cargo_weight_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "[ %s ] update payload [ %s ]",
                mqtt_msg->topic_path,
                mqtt_msg->payload);

    update_cargo_weight_state(mqtt_msg->payload);    

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void update_cargo_weight_state(char *msg_payload)
{
    /* todo: convert string to float/uint */

    states_update_flag = states_update_flag | CARGO_WEIGHT_UPDATE;
}
