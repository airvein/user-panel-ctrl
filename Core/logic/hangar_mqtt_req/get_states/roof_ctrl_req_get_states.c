#include "inc/roof_ctrl_req_get_states.h"

#include "debug/log_modules/log_modules.h"

#include <string.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_request_t *req_roof_ctrl_get_states = NULL;

mqtt_msg_t *msg_roof_ctrl_state_roof = NULL;

uint8_t ev_roof_ctrl_roof = ROOF_CTRL_ROOF_CLOSE;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

enum 
{    
    WAIT_FOR_UPDATE = 0,  

    ROOF_UPDATE = 1
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool request_ack = false;

static uint8_t states_update_flag = WAIT_FOR_UPDATE;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void req_roof_ctrl_get_states_init(void);

static bool req_roof_ctrl_response_callback(mqtt_msg_t *mqtt_msg);

static bool is_request_ack(char *resp_payload);


static void msg_roof_ctrl_state_roof_init(void);

static bool msg_roof_ctrl_state_roof_callback(mqtt_msg_t *mqtt_msg);

static void update_roof_state(char *msg_payload);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_roof_ctrl_get_states_init(void)
{
    req_roof_ctrl_get_states_init();
    msg_roof_ctrl_state_roof_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_roof_ctrl_get_states_send(void)
{
    request_ack = false;
    states_update_flag = WAIT_FOR_UPDATE;

    mqtt_request_send_req(req_roof_ctrl_get_states, 
                          REQ_ROOF_CTRL_GET_STATES_REQ_PAYLOAD);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_roof_ctrl_is_roof_update(void)
{
    return request_ack == true && states_update_flag == ROOF_UPDATE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void req_roof_ctrl_get_states_init(void)
{
    req_roof_ctrl_get_states = mqtt_request_create();
    
    mqtt_request_init(req_roof_ctrl_get_states, REQ_ROOF_NAME, 
                      hangar_mqtt_client);
    
    mqtt_request_req_msg_init(req_roof_ctrl_get_states, 
                              REQ_ROOF_CTRL_GET_STATES_REQ_TOPIC);
    
    mqtt_request_resp_msg_init(req_roof_ctrl_get_states,
                               REQ_ROOF_CTRL_GET_STATES_RESP_TOPIC,
                               req_roof_ctrl_response_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool req_roof_ctrl_response_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "roof_ctrl [ get_states ] request resp [ %s ]",
                mqtt_msg->payload);

    if(is_request_ack(mqtt_msg->payload) == true)
    {
        request_ack = true;
    }

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_request_ack(char *resp_payload)
{
    return strstr(resp_payload, REQ_ACK_STR);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void msg_roof_ctrl_state_roof_init(void)
{
    msg_roof_ctrl_state_roof = mqtt_msg_create();
    mqtt_msg_init(msg_roof_ctrl_state_roof, hangar_mqtt_client, QOS_2, 
                  ROOF_CTRL_ROOF_MQTT_TOPIC, 
                  msg_roof_ctrl_state_roof_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool msg_roof_ctrl_state_roof_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "[ %s ] update payload [ %s ]",
                mqtt_msg->topic_path,
                mqtt_msg->payload);

    update_roof_state(mqtt_msg->payload);    

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void update_roof_state(char *msg_payload)
{
    if(strcmp(msg_payload, "open") == 0)
    {
        log_debug_m(log_controller, 
                    "roof_ctrl [ roof ] update to open");
        ev_roof_ctrl_roof = ROOF_CTRL_ROOF_OPEN;
        states_update_flag = states_update_flag | ROOF_UPDATE;
    }
    else if(strcmp(msg_payload, "idle") == 0)
    {
        log_debug_m(log_controller, 
                    "roof_ctrl [ roof ] update to idle");
        ev_roof_ctrl_roof = ROOF_CTRL_ROOF_IDLE;
        states_update_flag = states_update_flag | ROOF_UPDATE;
    }
    else if(strcmp(msg_payload, "close") == 0)
    {
        log_debug_m(log_controller, 
                    "roof_ctrl [ roof ] update to close");
        ev_roof_ctrl_roof = ROOF_CTRL_ROOF_CLOSE;
        states_update_flag = states_update_flag | ROOF_UPDATE;
    }
    else
    {
        log_warning_m(log_controller, 
                      "roof_ctrl [ roof ] update invalid value");
    }    
}