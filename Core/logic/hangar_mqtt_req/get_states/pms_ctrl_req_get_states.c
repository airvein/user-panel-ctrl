#include "inc/pms_ctrl_req_get_states.h"

#include "debug/log_modules/log_modules.h"

#include <string.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_request_t *req_pms_ctrl_get_states = NULL;

mqtt_msg_t *msg_pms_ctrl_state_pressure = NULL;
mqtt_msg_t *msg_pms_ctrl_state_chargers_power = NULL;

uint8_t ev_pms_ctrl_pressure = PMS_CTRL_STATE_DISABLE;
uint8_t ev_pms_ctrl_chargers_power = PMS_CTRL_STATE_DISABLE;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

enum 
{    
    WAIT_FOR_UPDATE = 0,  

    PRESSURE_UPDATE = 1,
    CHARGERS_POWER_UPDATE = 2,

    ALL_UPDATE = PRESSURE_UPDATE | CHARGERS_POWER_UPDATE
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool request_ack = false;

static uint8_t states_update_flag = WAIT_FOR_UPDATE;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void req_pms_ctrl_get_states_init(void);

static bool req_pms_ctrl_response_callback(mqtt_msg_t *mqtt_msg);

static bool is_request_ack(char *resp_payload);


static void msg_pms_ctrl_state_pressure_init(void);

static bool msg_pms_ctrl_state_pressure_callback(mqtt_msg_t *mqtt_msg);

static void update_enable_disable_state(char *msg_payload, uint8_t *state_ptr,
                                        char *name, uint8_t update_flag);

static void msg_pms_ctrl_state_chargers_power_init(void);

static bool msg_pms_ctrl_state_chargers_power_callback(mqtt_msg_t *mqtt_msg);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_pms_ctrl_get_states_init(void)
{
    req_pms_ctrl_get_states_init();
    
    msg_pms_ctrl_state_pressure_init();
    msg_pms_ctrl_state_chargers_power_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_pms_ctrl_get_states_send(void)
{
    request_ack = false;
    states_update_flag = WAIT_FOR_UPDATE;

    mqtt_request_send_req(req_pms_ctrl_get_states, 
                          REQ_PMS_CTRL_GET_STATES_REQ_PAYLOAD);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_pms_ctrl_is_pressure_update(void)
{
    return request_ack == true && states_update_flag & PRESSURE_UPDATE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_pms_ctrl_is_chargers_power_update(void)
{
    return request_ack == true && states_update_flag & CHARGERS_POWER_UPDATE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_pms_ctrl_is_all_states_update(void)
{
    return request_ack == true && states_update_flag == ALL_UPDATE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void req_pms_ctrl_get_states_init(void)
{
    req_pms_ctrl_get_states = mqtt_request_create();
    
    mqtt_request_init(req_pms_ctrl_get_states, REQ_PMS_NAME, 
                      hangar_mqtt_client);
    
    mqtt_request_req_msg_init(req_pms_ctrl_get_states, 
                              REQ_PMS_CTRL_GET_STATES_REQ_TOPIC);
    
    mqtt_request_resp_msg_init(req_pms_ctrl_get_states,
                               REQ_PMS_CTRL_GET_STATES_RESP_TOPIC,
                               req_pms_ctrl_response_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool req_pms_ctrl_response_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "pms_ctrl [ get_states ] request resp [ %s ]",
                mqtt_msg->payload);

    if(is_request_ack(mqtt_msg->payload) == true)
    {
        request_ack = true;
    }

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_request_ack(char *resp_payload)
{
    return strstr(resp_payload, REQ_ACK_STR);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void msg_pms_ctrl_state_pressure_init(void)
{
    msg_pms_ctrl_state_pressure = mqtt_msg_create();
    mqtt_msg_init(msg_pms_ctrl_state_pressure, hangar_mqtt_client, QOS_2, 
                  PMS_CTRL_PRESSURE_MQTT_TOPIC, 
                  msg_pms_ctrl_state_pressure_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool msg_pms_ctrl_state_pressure_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "[ %s ] update payload [ %s ]",
                mqtt_msg->topic_path,
                mqtt_msg->payload);

    update_enable_disable_state(mqtt_msg->payload, &ev_pms_ctrl_pressure, 
                                "pressure", PRESSURE_UPDATE);

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void update_enable_disable_state(char *msg_payload, uint8_t *state_ptr,
                                        char *name, uint8_t update_flag)
{
    if(strcmp(msg_payload, "enable") == 0)
    {
        log_debug_m(log_controller, 
                    "pms_ctrl [ %s ] update to enable",
                    name);
        *state_ptr = PMS_CTRL_STATE_ENABLE;
        states_update_flag = states_update_flag | update_flag;
    }
    else if(strcmp(msg_payload, "disable") == 0)
    {
        log_debug_m(log_controller, 
                    "pms_ctrl [ %s ] update to disable",
                    name);
        *state_ptr = PMS_CTRL_STATE_DISABLE;
        states_update_flag = states_update_flag | update_flag;
    }
    else
    {
        log_warning_m(log_controller, 
                      "pms_ctrl [ %s] update invalid value",
                      name);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void msg_pms_ctrl_state_chargers_power_init(void)
{
    msg_pms_ctrl_state_chargers_power = mqtt_msg_create();
    mqtt_msg_init(msg_pms_ctrl_state_chargers_power, hangar_mqtt_client, QOS_2, 
                  PMS_CTRL_CHARG_PWR_MQTT_TOPIC, 
                  msg_pms_ctrl_state_chargers_power_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool msg_pms_ctrl_state_chargers_power_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "[ %s ] update payload [ %s ]",
                mqtt_msg->topic_path,
                mqtt_msg->payload);

    update_enable_disable_state(mqtt_msg->payload, &ev_pms_ctrl_chargers_power, 
                                "chargers_power", CHARGERS_POWER_UPDATE);

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|