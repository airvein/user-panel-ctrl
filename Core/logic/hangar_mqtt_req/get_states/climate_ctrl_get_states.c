#include "inc/climate_ctrl_req_get_states.h"

#include "debug/log_modules/log_modules.h"

#include <string.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_request_t *req_clim_ctrl_get_states = NULL;
 
mqtt_msg_t *msg_clim_ctrl_state_type = NULL;
mqtt_msg_t *msg_clim_ctrl_state_ac = NULL;
mqtt_msg_t *msg_clim_ctrl_state_hang_heater = NULL;
mqtt_msg_t *msg_clim_ctrl_state_roof_heater = NULL;
mqtt_msg_t *msg_clim_ctrl_state_fan = NULL;

uint8_t ev_clim_ctrl_type           = CLIM_CTRL_TYPE_MANUAL;
uint8_t ev_clim_ctrl_ac             = CLIM_CTRL_STATE_DISABLE;
uint8_t ev_clim_ctrl_hang_heat      = CLIM_CTRL_STATE_DISABLE;
uint8_t ev_clim_ctrl_roof_heat      = CLIM_CTRL_STATE_DISABLE;
uint8_t ev_clim_ctrl_fan            = CLIM_CTRL_STATE_DISABLE;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

enum 
{    
    WAIT_FOR_UPDATE = 0,  

    TYPE_UPDATE = 1, 
    AC_UPDATE = 2, 
    HANG_HEAT_UPDATE = 4, 
    ROOF_HEAT_UPDATE = 8,
    FAN_UPDATE = 16,
    
    ALL_UPDATE = 
        TYPE_UPDATE | AC_UPDATE | HANG_HEAT_UPDATE 
        | ROOF_HEAT_UPDATE | FAN_UPDATE
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool request_ack = false;

static uint8_t states_update_flag = WAIT_FOR_UPDATE;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void req_clim_ctrl_get_states_init(void);

static bool req_clim_ctrl_response_callback(mqtt_msg_t *mqtt_msg);

static bool is_request_ack(char *resp_payload);


static void msg_clim_ctrl_state_type_init(void);

static bool msg_clim_ctrl_state_type_callback(mqtt_msg_t *mqtt_msg);

static void update_type_state(char *msg_payload);


static void update_enable_disable_state(char *msg_payload, uint8_t *state_ptr,
                                        char *name, uint8_t update_flag);


static void msg_clim_ctrl_state_ac_init(void);

static bool msg_clim_ctrl_state_ac_callback(mqtt_msg_t *mqtt_msg);


static void msg_clim_ctrl_state_hang_heat_init(void);

static bool msg_clim_ctrl_state_hang_heat_callback(mqtt_msg_t *mqtt_msg);


static void msg_clim_ctrl_state_roof_heat_init(void);

static bool msg_clim_ctrl_state_roof_heat_callback(mqtt_msg_t *mqtt_msg);


static void msg_clim_ctrl_state_fan_init(void);

static bool msg_clim_ctrl_state_fan_callback(mqtt_msg_t *mqtt_msg);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_clim_ctrl_get_states_init(void)
{
    req_clim_ctrl_get_states_init();

    msg_clim_ctrl_state_type_init();
    msg_clim_ctrl_state_ac_init();
    msg_clim_ctrl_state_hang_heat_init();
    msg_clim_ctrl_state_roof_heat_init();
    msg_clim_ctrl_state_fan_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_req_clim_ctrl_get_states_send(void)
{
    request_ack = false;
    states_update_flag = WAIT_FOR_UPDATE;

    mqtt_request_send_req(req_clim_ctrl_get_states, 
                          REQ_CLIM_CTRL_GET_STATES_REQ_PAYLOAD);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_clim_ctrl_is_type_update(void)
{
    return (request_ack == true) && (states_update_flag & TYPE_UPDATE);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_clim_ctrl_is_ac_update(void)
{
    return (request_ack == true) && (states_update_flag & AC_UPDATE);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_clim_ctrl_is_hangar_heater_update(void)
{
    return (request_ack == true) && (states_update_flag & HANG_HEAT_UPDATE);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_clim_ctrl_is_roof_heater_update(void)
{
    return (request_ack == true) && (states_update_flag & ROOF_HEAT_UPDATE);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_clim_ctrl_is_fan_update(void)
{
    return (request_ack == true) && (states_update_flag & FAN_UPDATE);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_req_clim_ctrl_is_all_states_update(void)
{
    return (request_ack == true) && (states_update_flag == ALL_UPDATE);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void req_clim_ctrl_get_states_init(void)
{
    req_clim_ctrl_get_states = mqtt_request_create();
    
    mqtt_request_init(req_clim_ctrl_get_states, REQ_CLIM_NAME, 
                      hangar_mqtt_client);
    
    mqtt_request_req_msg_init(req_clim_ctrl_get_states, 
                              REQ_CLIM_CTRL_GET_STATES_REQ_TOPIC);
    
    mqtt_request_resp_msg_init(req_clim_ctrl_get_states,
                               REQ_CLIM_CTRL_GET_STATES_RESP_TOPIC,
                               req_clim_ctrl_response_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool req_clim_ctrl_response_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "clim_ctrl [ get_states ] request resp [ %s ]",
                mqtt_msg->payload);

    if(is_request_ack(mqtt_msg->payload) == true)
    {
        request_ack = true;
    }

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_request_ack(char *resp_payload)
{
    return strstr(resp_payload, REQ_ACK_STR);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void msg_clim_ctrl_state_type_init(void)
{
    msg_clim_ctrl_state_type = mqtt_msg_create();
    mqtt_msg_init(msg_clim_ctrl_state_type, hangar_mqtt_client, QOS_2, 
                  CLIM_CTRL_TYPE_MQTT_TOPIC, 
                  msg_clim_ctrl_state_type_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool msg_clim_ctrl_state_type_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "[ %s ] update payload [ %s ]",
                mqtt_msg->topic_path,
                mqtt_msg->payload);

    update_type_state(mqtt_msg->payload);    

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void update_type_state(char *msg_payload)
{
    if(strcmp(msg_payload, "auto") == 0)
    {
        log_debug_m(log_controller, 
                    "clim_ctrl [ type ] update to auto");
        ev_clim_ctrl_type = CLIM_CTRL_TYPE_AUTO;
        states_update_flag = states_update_flag | TYPE_UPDATE;
    }
    else if(strcmp(msg_payload, "manual") == 0)
    {
        log_debug_m(log_controller, 
                    "clim_ctrl [ type ] update to manual");
        ev_clim_ctrl_type = CLIM_CTRL_TYPE_MANUAL;
        states_update_flag = states_update_flag | TYPE_UPDATE;
    }
    else
    {
        log_warning_m(log_controller, 
                      "clim_ctrl [ type ] update invalid value");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void update_enable_disable_state(char *msg_payload, uint8_t *state_ptr, 
                                        char *name, uint8_t update_flag)
{
    if(strcmp(msg_payload, "enable") == 0)
    {
        log_debug_m(log_controller, 
                    "clim_ctrl [ %s ] update to enable",
                    name);

        *state_ptr = CLIM_CTRL_STATE_ENABLE;
        states_update_flag = states_update_flag | update_flag;
    }
    else if(strcmp(msg_payload, "disable") == 0)
    {
        log_debug_m(log_controller, 
                    "clim_ctrl [ %s ] update to enable",
                    name);

        *state_ptr = CLIM_CTRL_STATE_DISABLE;
        states_update_flag = states_update_flag | update_flag;
    }
    else
    {
        log_warning_m(log_controller, 
                      "clim_ctrl [ %s ] update invalid value",
                      name);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void msg_clim_ctrl_state_ac_init(void)
{
    msg_clim_ctrl_state_ac = mqtt_msg_create();
    mqtt_msg_init(msg_clim_ctrl_state_ac, hangar_mqtt_client, QOS_2, 
                  CLIM_CTRL_AC_MQTT_TOPIC, 
                  msg_clim_ctrl_state_ac_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool msg_clim_ctrl_state_ac_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "[ %s ] update payload [ %s ]",
                mqtt_msg->topic_path,
                mqtt_msg->payload);

    update_enable_disable_state(mqtt_msg->payload, &ev_clim_ctrl_ac, "ac", 
                                AC_UPDATE);

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void msg_clim_ctrl_state_hang_heat_init(void)
{
    msg_clim_ctrl_state_hang_heater = mqtt_msg_create();
    mqtt_msg_init(msg_clim_ctrl_state_hang_heater, hangar_mqtt_client, QOS_2, 
                  CLIM_CTRL_HANG_HEATER_MQTT_TOPIC, 
                  msg_clim_ctrl_state_hang_heat_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool msg_clim_ctrl_state_hang_heat_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "[ %s ] update payload [ %s ]",
                mqtt_msg->topic_path,
                mqtt_msg->payload);

    update_enable_disable_state(mqtt_msg->payload, &ev_clim_ctrl_hang_heat, 
                                "hangar_heater", HANG_HEAT_UPDATE);
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void msg_clim_ctrl_state_roof_heat_init(void)
{
    msg_clim_ctrl_state_roof_heater = mqtt_msg_create();
    mqtt_msg_init(msg_clim_ctrl_state_roof_heater, hangar_mqtt_client, QOS_2, 
                  CLIM_CTRL_ROOF_HEATER_MQTT_TOPIC, 
                  msg_clim_ctrl_state_roof_heat_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool msg_clim_ctrl_state_roof_heat_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "[ %s ] update payload [ %s ]",
                mqtt_msg->topic_path,
                mqtt_msg->payload);

    update_enable_disable_state(mqtt_msg->payload, &ev_clim_ctrl_roof_heat,
                                "roof_heater", ROOF_HEAT_UPDATE); 

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void msg_clim_ctrl_state_fan_init(void)
{
    msg_clim_ctrl_state_fan = mqtt_msg_create();
    mqtt_msg_init(msg_clim_ctrl_state_fan, hangar_mqtt_client, QOS_2, 
                  CLIM_CTRL_FAN_MQTT_TOPIC, 
                  msg_clim_ctrl_state_fan_callback);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool msg_clim_ctrl_state_fan_callback(mqtt_msg_t *mqtt_msg)
{
    log_debug_m(log_controller, 
                "[ %s ] update payload [ %s ]",
                mqtt_msg->topic_path,
                mqtt_msg->payload);

    update_enable_disable_state(mqtt_msg->payload, &ev_clim_ctrl_fan,
                                "fan", FAN_UPDATE); 

    return true;
}