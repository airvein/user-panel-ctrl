#include "inc/hangar_ctrl_reset.h"
#include "communication/interface/hangar/inc/hangar_mqtt_client.h"
#include "communication/interface/hangar/inc/hangar_communication.h"
#include "settings_app/communication/settings_mqtt.h"
#include "debug/log_modules/log_modules.h"
#include "peripherals_core/rtc/inc/rtc.h"
#include <string.h>
#include "lwip.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define SUB_TOPIC       MQTT_MODULE_NAME"/cmd/ctrl_reset/call"
#define RESP_TOPIC      MQTT_MODULE_NAME"/cmd/ctrl_reset/resp"
#define PIN_TOPIC       MQTT_MODULE_NAME"/cmd/ctrl_reset/pin"

#define RESET_PAYLOAD   "reset"

#define PASSWD_INIT     -1

#define TIMEOUT_INTERVAL_MS                  100
#define TIMEOUT_SECOND(sec)                  ((1000/TIMEOUT_INTERVAL_MS)*sec)

#define SEND_PIN(pin)\
    do{\
        char resp_payload[50];\
        sprintf(resp_payload, "%d", pin);\
        mqtt_msg_publish(&sv_msg_pin, resp_payload);\
    }while(0);

#define SEND_RESP(resp_val)\
    do{\
        char resp_payload[50];\
        sprintf(resp_payload, "%s/%ld", resp_val, \
                ev_msg_hangar_ctrl_reset.order_id);\
        mqtt_msg_publish(&sv_msg_resp, resp_payload);\
    }while(0);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_msg_t ev_msg_hangar_ctrl_reset = {0};
timeout_t ev_tout_ctrl_reset = {0};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static mqtt_msg_t sv_msg_resp = {0};
static mqtt_msg_t sv_msg_pin = {0};

static int generated_reset_passwd = PASSWD_INIT;

enum { NOT_RESET, RESET_NOW, RESET_WAIT_FOR_ACK };
static uint8_t reset_execute = NOT_RESET;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_ctrl_reset_sub_cb(mqtt_msg_t *msg);
static bool is_reset_msg_incoming(mqtt_msg_t *msg);
static void authenticate_reset_handler(void);
static bool is_correct_incoming_reset_passwd(mqtt_msg_t *msg);
static void reset_ctrl_handler(void);
static void send_reset_ctrl_ack_handler(void);
static bool reset_ack_published(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_ctrl_reset_msg_init(void)
{
    mqtt_msg_init(&ev_msg_hangar_ctrl_reset, hangar_mqtt_client, QOS_2, 
                  SUB_TOPIC, hangar_ctrl_reset_sub_cb);
    mqtt_msg_init(&sv_msg_resp, hangar_mqtt_client, QOS_2, RESP_TOPIC, 
                    mqtt_msg_no_sub_cb);
    mqtt_msg_init(&sv_msg_pin, hangar_mqtt_client, QOS_2, PIN_TOPIC, 
                    mqtt_msg_no_sub_cb);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_ctrl_reset_tout_init(void)
{
    timeout_init(&ev_tout_ctrl_reset, 999, TIMEOUT_SECOND(10));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_ctrl_reset_handler(void)
{
    if(reset_execute == RESET_NOW 
        || (reset_execute == RESET_WAIT_FOR_ACK && reset_ack_published()))
    {
        log_warning_m(log_controller, "Reset controlerr execute!");
        NVIC_SystemReset();
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool hangar_ctrl_reset_sub_cb(mqtt_msg_t *msg)
{
    if(is_reset_msg_incoming(msg) == true)
    {
        authenticate_reset_handler();
    }
    else if(is_correct_incoming_reset_passwd(msg) == true)
    {
        reset_ctrl_handler();
    }

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_reset_msg_incoming(mqtt_msg_t *msg)
{
    return (strcmp(msg->payload, RESET_PAYLOAD) == 0);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void authenticate_reset_handler(void)
{
    log_warning_m(log_controller, "Incoming reset controller command!");
    timeout_reset(&ev_tout_ctrl_reset);
    srand(ev_system_timestamp);
    generated_reset_passwd = rand() % (100000 + 1 - 0) + 0;            
    SEND_PIN(generated_reset_passwd);
    timeout_start(&ev_tout_ctrl_reset);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_incoming_reset_passwd(mqtt_msg_t *msg)
{
    if(timeout_get_state(&ev_tout_ctrl_reset) != TIMEOUT_STATE_RUNNING)
    {
        log_warning_m(log_controller, "Reset ctrl %s" , 
            timeout_get_state(&ev_tout_ctrl_reset) == TIMEOUT_STATE_IDLE ?
            "passwd is not expected!" : "window is time up!");
        SEND_RESP("invalid_value");
        return false;
    }

    int incoming_passwd = atoi(msg->payload);

    if(incoming_passwd == generated_reset_passwd)
    {
        log_warning_m(log_controller, 
            "Receive reset passwd [ %d ] is equal to generated [ %d ]",
            incoming_passwd, generated_reset_passwd);
        return true;
    }
    else
    {
        log_warning_m(log_controller, 
            "Receive reset passwd [ %d ] is incorrect! (generated [ %d ])",
            incoming_passwd, generated_reset_passwd);
        SEND_RESP("refuse");
        timeout_reset(&ev_tout_ctrl_reset);
        return false;
    }    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void reset_ctrl_handler(void)
{
    if(timeout_get_state(&ev_tout_ctrl_reset) == TIMEOUT_STATE_RUNNING)
    {
        send_reset_ctrl_ack_handler();
    }
    else
    {
        log_info_m(log_controller, "Reset controller window time is up! " 
            "Reset cannot be performed!");
        SEND_RESP("refuse");
    }    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void send_reset_ctrl_ack_handler(void)
{
    if(hangar_mqtt_is_connected() == true)
    {
        log_warning_m(log_controller, 
            "Reset controller accepted, sending ack!");
        SEND_RESP("ack");

        reset_execute = RESET_WAIT_FOR_ACK;
    }
    else
    {
        reset_execute = RESET_NOW;

        log_warning_m(log_controller, 
            "MQTT connection lost, can't send ack for reset ctrl!");
    }    
} 

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool reset_ack_published(void)
{
    return sv_msg_resp.state == MSG_STATE_PUBLISHED;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|