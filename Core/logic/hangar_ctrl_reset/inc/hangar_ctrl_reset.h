#ifndef __HANGAR_CTRL_RESET_H
#define __HANGAR_CTRL_RESET_H

#include "communication/protocols/mqtt/inc/mqtt_msg.h"
#include "time/timeout/inc/timeout.h"
#include "settings_app/communication/settings_mqtt.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern mqtt_msg_t ev_msg_hangar_ctrl_reset;
extern timeout_t ev_tout_ctrl_reset;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_ctrl_reset_msg_init(void);

void hangar_ctrl_reset_tout_init(void);

void hangar_ctrl_reset_handler(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __HANGAR_CTRL_RESET_H */