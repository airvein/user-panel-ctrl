#ifndef __RTC_H
#define __RTC_H

#include <stm32f7xx_hal.h>
#include <stdio.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern uint32_t ev_system_timestamp;

extern RTC_HandleTypeDef hrtc;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern void rtc_init();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


#endif /* __RTC_H */
