#include <settings_core/peripherals/settings_plc_module.h>
#include "inc/plc_module.h"
#include "library/inc/x_nucleo_plc01a1.h"
#include <stdlib.h>

#include "debug/log_modules/log_modules.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC()	(log_debug_m(log_plc_module, "%s : %s", \
							__FILE__, __func__))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

plc_module_t *ev_plc_module = NULL;
TIM_HandleTypeDef plc_timer = { 0 };


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                        S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ResetBitInByte(byte, bit_pos) (~(1 << bit_pos) & byte)

#define SetBitInByte(byte, bit_pos) ((1 << bit_pos) | byte)

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static HAL_StatusTypeDef open_spi(plc_module_t *plc);

static void spi_open_clock(SPI_TypeDef *spix);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

plc_module_t *plc_module_create(void)
{
	return (plc_module_t*)malloc(sizeof(plc_module_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_init(plc_module_t *plc, SPI_TypeDef *spix)
{
	LOG_DEBUG_FUNC();
	log_info_m(log_plc_module,
				"Initialize plc module structure");
	plc->hspi = (SPI_HandleTypeDef*)malloc(sizeof(SPI_HandleTypeDef));
	plc->hspi->Instance = spix;

	plc->set_outputs_states = 0x00;
	plc->outputs_states = 0x00;
	plc->inputs_states = 0x00;
	plc->relay_init = 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t plc_module_cfg_current_limiter_cs(plc_module_t *plc,
										  GPIO_TypeDef *current_cs_port,
										  uint16_t current_cs_pin)
{
	
	if(plc != NULL)
	{
		log_debug_m(log_plc_module,
			   		"Configure input SPI CS pin");
		plc->current_limiter_cs_port = current_cs_port;
		plc->current_limiter_cs_pin = current_cs_pin;
		return 1;
	}
	log_error_m(log_plc_module,
			   	"PLC structure is NULL, (input SPI CS pin)");
	return 0;
}									   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t plc_module_cfg_relay_cs(plc_module_t *plc, GPIO_TypeDef *relay_cs_port,
								uint16_t relay_cs_pin)
{
	if(plc != NULL)
	{
		log_debug_m(log_plc_module,
			   		"Configure output SPI CS pin");
		plc->relay_cs_port = relay_cs_port;
		plc->relay_cs_pin = relay_cs_pin;
		return 1;
	}
	log_error_m(log_plc_module,
			   	"PLC structure is NULL, (output SPI CS pin)");
	return 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t plc_module_cfg_relay_en(plc_module_t *plc, GPIO_TypeDef *relay_en_port,
								uint16_t relay_en_pin)
{
	if(plc != NULL)
	{
		log_debug_m(log_plc_module,
			   		"Configure output enable pin");		
		plc->relay_en_out_port = relay_en_port;
		plc->relay_en_out_pin = relay_en_pin;
		return 1;
	}
	log_error_m(log_plc_module,
			   	"PLC structure is NULL, (output enable pin)");	
	return 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t plc_module_cfg_spi_open_callback(plc_module_t *plc, 
										 open_spi_pinout_callback open_spi_cb)
{
	if(plc != NULL && open_spi_cb != NULL)
	{
		log_debug_m(log_plc_module,
			   		"Configure spi open callback");				
		plc->open_spi_io_clbk = open_spi_cb;
		return 1;
	}
	log_error_m(log_plc_module,
			   	"PLC structure is NULL, (spi open callback)");
	return 0;
}										 

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_communication_init(plc_module_t *plc)
{
	LOG_DEBUG_FUNC();
	HAL_StatusTypeDef spi_retval = open_spi(plc);
	ASSERT_LOG_ERROR_M(spi_retval != HAL_OK, log_plc_module, 
					   "SPI open error [ errcode = %d ]", spi_retval);
	ASSERT_LOG_INFO_M(spi_retval == HAL_OK, log_plc_module, 
					  "SPI open succesfull");
	CLT01_38S_IO_Config(plc);	
	VNI8200XP_IO_Config(plc);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_set_outputs(plc_module_t *plc, uint8_t output_states)
{
	plc->set_outputs_states = output_states;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_set_single_output(plc_module_t *plc, relay_out_n_t outn, 
								  uint8_t on_off)
{
	if(on_off == OFF)
	{
		plc->set_outputs_states = ResetBitInByte(plc->set_outputs_states,
												outn);
	}
	else if(on_off == ON)
	{
		plc->set_outputs_states = SetBitInByte(plc->set_outputs_states, outn);
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

RELAY_StatusTypeDef plc_module_send_outputs(plc_module_t *plc)
{
	static uint8_t* resp = NULL;
	resp = BSP_RELAY_SetOutputs(plc, &plc->set_outputs_states);

	plc->relay_state = BSP_GetRelayStatus(resp);
	if(plc->relay_state == RELAY_OK)
	{
		plc->outputs_states = plc->set_outputs_states;
		plc->state = PLC_STATE_OK;
	}
	else
	{
		plc->state = PLC_STATE_RELAY_ERROR;		
	}

	return plc->relay_state;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

RELAY_StatusTypeDef plc_module_set_and_send_outputs(plc_module_t *plc,
													uint8_t output_states)
{
	plc_module_set_outputs(plc, output_states);

	return plc_module_send_outputs(plc);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

RELAY_StatusTypeDef plc_module_set_and_send_single_output(plc_module_t *plc,
										   relay_out_n_t outn, uint8_t on_off)
{
	plc_module_set_single_output(plc, outn, on_off);

	return plc_module_send_outputs(plc);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t plc_module_read_inputs_handler(plc_module_t *plc)
{
  static uint8_t* resp = NULL;
  resp = BSP_CURRENT_LIMITER_Read(plc);

  plc->current_state = BSP_GetCurrentLimiterStatus(resp);

  if(plc->current_state != CURRENT_LIMITER_OK)
  {
	  //log_error_m(log_plc_module, "Input error!");
	  plc->state = PLC_STATE_CURRENT_ERROR;
  }
  else
  {
	  plc->state = PLC_STATE_OK;
  }
  

  plc->inputs_states = *(resp+1);

  return *(resp+1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t plc_module_get_input_state(plc_module_t *plc, current_in_n_t inn)
{
	return ((plc->inputs_states >> inn) & 0x01);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint8_t plc_module_get_output_state(plc_module_t *plc, relay_out_n_t outn)
{
	return ((plc->outputs_states >> outn) & 0x01);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_mirror_signals_handler(plc_module_t *plc)
{
	uint8_t Input_Data = plc_module_read_inputs_handler(plc);
	uint8_t Output_data = BSP_Signal_Mirror(Input_Data);

	plc_module_set_and_send_outputs(plc, Output_data);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_print_inputs(plc_module_t *plc)
{
	if(plc->state == PLC_STATE_OK)
	{
		//TODO: ADD PRINT
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_print_states(plc_module_t *plc)
{
	//TODO: ADD PRINT
//	PLC_PRINT("plc_state %d | relay_state %d | current_state %d",
//			plc->state, plc->relay_state, plc->current_state);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_output_cycling(plc_module_t *plc)
{
	HAL_GPIO_WritePin(plc->current_limiter_cs_port,
			plc->current_limiter_cs_pin, 0);
	HAL_GPIO_WritePin(plc->current_limiter_cs_port,
			plc->current_limiter_cs_pin, 1);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_reset_module(plc_module_t *plc)
{
	BSP_RELAY_Reset(plc);
	HAL_Delay(100);
	BSP_RELAY_EN_Out(plc);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_module_timer_init()
{
	PLC_TIM_CLK_ENABLE();
	plc_timer.Instance = PLC_TIM_INSTANCE;
	plc_timer.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	plc_timer.Init.CounterMode = TIM_COUNTERMODE_UP;
	plc_timer.Init.Prescaler = 16000-1;
	plc_timer.Init.Period = PLC_TIM_INTERVAL_MS-1;
	plc_timer.Init.ClockDivision = 0;

	HAL_TIM_Base_Init(&plc_timer);
	__HAL_TIM_CLEAR_IT(&plc_timer, TIM_IT_UPDATE);

	HAL_NVIC_SetPriority(PLC_TIM_IRQn, 1, 0);
	HAL_NVIC_EnableIRQ(PLC_TIM_IRQn);
	HAL_TIM_Base_Start_IT(&plc_timer);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void PLC_TIM_IRQHandler()
{
	HAL_TIM_IRQHandler(&plc_timer);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void plc_timer_period_elapsed(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == PLC_TIM_INSTANCE)
	{
		plc_module_output_cycling(ev_plc_module);
		plc_module_read_inputs_handler(ev_plc_module);
		plc_module_send_outputs(ev_plc_module);		
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static HAL_StatusTypeDef open_spi(plc_module_t *plc)
{

	HAL_StatusTypeDef ret_val;

	log_info_m(log_plc_module, 
				"Configuring SPI for PLC communication");

	spi_open_clock(plc->hspi->Instance);
	plc->hspi->Init.Mode = SPI_MODE_MASTER;
	plc->hspi->Init.Direction = SPI_DIRECTION_2LINES;
	plc->hspi->Init.DataSize = SPI_DATASIZE_16BIT;
	plc->hspi->Init.CLKPolarity = SPI_POLARITY_LOW;
	plc->hspi->Init.CLKPhase = SPI_PHASE_1EDGE;
	plc->hspi->Init.NSS = SPI_NSS_SOFT;
	plc->hspi->Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_64;
	plc->hspi->Init.FirstBit = SPI_FIRSTBIT_MSB;
	plc->hspi->Init.TIMode = SPI_TIMODE_DISABLED;
	plc->hspi->Init.CRCCalculation = SPI_CRCCALCULATION_DISABLED;

	plc->open_spi_io_clbk();
	ret_val = HAL_SPI_Init(plc->hspi);	

	if (ret_val != HAL_OK)
	{
		return ret_val;
	}
	if (HAL_SPI_GetState(plc->hspi) == HAL_SPI_STATE_READY)
	{
		return HAL_OK;
	}
	else
	{
		return HAL_ERROR;
	}

	return HAL_OK;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void spi_open_clock(SPI_TypeDef *spix)
{
	if(spix == SPI1)
	{
		__SPI1_CLK_ENABLE();
	}
	else if(spix == SPI2)
	{
		__SPI2_CLK_ENABLE();
	}
	else if(spix == SPI3)
	{
		__SPI3_CLK_ENABLE();
	}
	else if(spix == SPI4)
	{
		__SPI4_CLK_ENABLE();
	}
	else if(spix == SPI5)
	{
		__SPI5_CLK_ENABLE();
	}
	else if(spix == SPI6)
	{
		__SPI6_CLK_ENABLE();
	}
}
