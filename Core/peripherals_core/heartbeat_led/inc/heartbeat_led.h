#ifndef __HEARTBEAT_LED_H
#define __HEARTBEAT_LED_H



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void heartbeat_led_init(void);

void heartbeat_led_toggle(void);


#endif /* __HEARTBEAT_LED_H */