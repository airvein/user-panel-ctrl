#ifndef __PERIPHERIALS_INIT_H
#define __PERIPHERIALS_INIT_H

//#include "usb_device.h" /* TODO : usb implementation */
#include "lwip.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void peripherials_init();

void initialize_plc_module(void);

void initialize_plc_communication(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


#endif /* __PERIPHERIALS_INIT_H */
