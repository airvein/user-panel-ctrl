#include <settings_core/peripherals/settings_plc_module.h>
#include "peripherals_core/init/inc/peripherials_init.h"
#include "peripherals_core/rtc/inc/rtc.h"

#include "peripherals_core/plc_module/inc/plc_module.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// static void initialize_plc_module(void);

static void open_spi_plc_module_cb(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void peripherials_init()
{

	rtc_init();
	//initialize_plc_module();

}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void initialize_plc_module(void)
{
	ev_plc_module = plc_module_create();

	plc_module_cfg_current_limiter_cs(ev_plc_module, PLC_CURRENT_CS_GPIO);
	plc_module_cfg_relay_cs(ev_plc_module, PLC_RELAY_CS_GPIO);
	plc_module_cfg_relay_en(ev_plc_module, PLC_RELAY_EN_GPIO);
	plc_module_cfg_spi_open_callback(ev_plc_module, open_spi_plc_module_cb);

	plc_module_init(ev_plc_module, PLC_SPIx);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void initialize_plc_communication(void)
{
	plc_module_communication_init(ev_plc_module);

	plc_module_reset_module(ev_plc_module);
	HAL_Delay(20);

	plc_module_timer_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void open_spi_plc_module_cb(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	__HAL_RCC_SPI1_CLK_ENABLE();

	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	/**SPI1 GPIO Configuration
	PA5     ------> SPI1_SCK
	PA6     ------> SPI1_MISO
	PA7     ------> SPI1_MOSI
	*/
	GPIO_InitStruct.Pin = GPIO_SPI1_SCK_PIN|GPIO_SPI1_MISO_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(GPIO_SPI1_SCK_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_SPI1_MOSI_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(GPIO_SPI1_MOSI_PORT, &GPIO_InitStruct);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
