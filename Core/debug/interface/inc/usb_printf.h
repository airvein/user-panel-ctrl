/**
  ******************************************************************************
  * @file           : usb_printf.h
  * @brief          : Header file for usb_printf
  * @author			: Mateusz Strojek
  ******************************************************************************
  *
  * COPYRIGHT(c) 2020 Cervi Robotics
  */

#ifndef __USB_PRINTF_H
#define __USB_PRINTF_H

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include <stm32f7xx_hal.h>

int usb_printf(const char *fmt, ...);

#endif /* __USB_PRINTF_H */
