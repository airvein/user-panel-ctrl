/**
  ******************************************************************************
  * @file           : usb_printf.c
  * @brief          : Printf functions via USB
  * @author			: Mateusz Strojek
  ******************************************************************************
  *
  * COPYRIGHT(c) 2020 Cervi Robotics
  */

#include "inc/usb_printf.h"
#include <string.h>
#include "usbd_cdc_if.h"



#define SERVICE_DEBUG

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void vprint(const char *fmt, va_list argp)
{
    char string[255];

    if(0 < vsprintf(string,fmt,argp))
    {
        CDC_Transmit_FS((uint8_t*)string, strlen(string));
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

int usb_printf(const char *fmt, ...)
{
	#ifdef SERVICE_DEBUG
		va_list argp;
		va_start(argp, fmt);
		vprint(fmt, argp);
		va_end(argp);
	#endif
	return 1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
