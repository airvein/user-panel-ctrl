/**
  ******************************************************************************
  * @file           : uart_printf.c
  * @brief          : Printf functions via serial (USART)
  * @author			: Mateusz Strojek
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 Cervi Robotics
  */

#include "settings_app/debug/settings_sd.h"
#include "inc/uart_printf.h"
#include "debug/sd_card/inc/sd_debug.h"
#include "string.h"



#define SERVICE_DEBUG

extern UART_HandleTypeDef huart1;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void vprint(const char *fmt, va_list argp)
{
    char string[255];

    if(0 < vsprintf(string,fmt,argp))
    {
        HAL_UART_Transmit(&huart1, (uint8_t*)string, strlen(string), 100);

        #ifdef ENABLE_SD_CARD
        if(is_possible_save_logs == true && strstr(string, "sd_card") == NULL)
        {
            sd_write_to_file(string);
        }
        #endif
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

int uart_printf(const char *fmt, ...)
{
	#ifdef SERVICE_DEBUG
		va_list argp;
		va_start(argp, fmt);
		vprint(fmt, argp);
		va_end(argp);
	#endif
	return 1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|