#include "settings_core/debug/settings_notify_signal.h"
#include "inc/notify_signal.h"

#include "debug/log_modules/log_modules.h"

#include <stdlib.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_notify_sig, "%s : %s", __FILE__, \
                                      __func__))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

enum { QUICK_SIG, SLOW_SIG, PAUSE_QUICK_SIG, PAUSE_SLOW_SIG };

enum { ON_SIG, OFF_SIG };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static TIM_HandleTypeDef notify_timer;
static bool notif_timer_init = false;

static notify_signal_t *enabled_notify = NULL;
static bool notify_enable_flag = false;

uint8_t signal_state = QUICK_SIG;

uint8_t quick_signal_cnt = 0;
uint8_t slow_signal_cnt = 0;
uint8_t pause_signal_cnt = 0;

bool on_off_flag = ON_SIG;

static notify_signal_on_off_cb (*on_signal_cb) = NULL; 
static notify_signal_on_off_cb (*off_signal_cb) = NULL;



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void notify_handler(notify_signal_t *notify);

static void notify_timer_init(void);

static void on_off_switch();

static void notify_timer_enable();

static void notify_timer_disable();

static bool is_correct_init_parameters(notify_signal_t *notify, 
									   uint8_t quick_sig_num, 
									   uint8_t slow_sig_num);

static bool is_notify_initialized(notify_signal_t *notify);

static bool is_possible_register_signal_callback(notify_signal_on_off_cb sig_cb);

static bool is_possible_enable_notify(notify_signal_t *notify);
							   
static void reset_signals_variables();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

notify_signal_t *notify_signal_create(void)
{
	return (notify_signal_t*)malloc(sizeof(notify_signal_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool notify_signal_init(notify_signal_t *notify, uint8_t quick_sig_num, 
                        uint8_t slow_sig_num)
{
	LOG_DEBUG_FUNC();
	if(is_correct_init_parameters(notify, quick_sig_num, 
								  slow_sig_num) == false)
	{
		log_error_m(log_notify_sig, "Incorrect initialize parameters!");
		return false;
	}

	notify->quick_signal_num = quick_sig_num;
	notify->slow_signal_num = slow_sig_num;
	notify->is_init = true;

	return true;
}						

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool notify_signal_enable(notify_signal_t *notify)
{
	LOG_DEBUG_FUNC();
	if(is_possible_enable_notify(notify) == false)
	{
		log_warning_m(log_notify_sig, 
					  "Notify can not be enabled!");
		return false;
	}

	enabled_notify = notify;
	notify_enable_flag = true;
	signal_state = QUICK_SIG;
	reset_signals_variables();
	notify_timer_enable();

	return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool notify_signal_disable(notify_signal_t *notify)
{
	LOG_DEBUG_FUNC();
	if(is_notify_initialized(notify) == false)
	{
		log_warning_m(log_notify_sig, 
					  "Notify can not be disabled [not init correctly!]");
		return false;
	}

	notify_timer_disable();
	notify_enable_flag = false;
	enabled_notify = NULL;
	off_signal_cb();
	
	return true;	
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool notify_signal_register_signal_on_cb(notify_signal_on_off_cb sig_on_cb)
{
	if(is_possible_register_signal_callback(sig_on_cb) == false)
	{
		log_error_m(log_notify_sig, "Can not register sig_on_cb");
		return false;
	}
	log_info_m(log_notify_sig, "Register signal on cb succesfull");
	on_signal_cb = sig_on_cb;
	return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool notify_signal_register_signal_off_cb(notify_signal_on_off_cb sig_off_cb)
{
	if(is_possible_register_signal_callback(sig_off_cb) == false)
	{
		log_error_m(log_notify_sig, "Can not register sig_off_cb");
		return false;
	}
	log_info_m(log_notify_sig, "Register signal off cb succesfull");
	off_signal_cb = sig_off_cb;
	return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void notify_timer_period_elapsed(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == NOTIFY_TIM_INSTANCE)
	{
		notify_handler(enabled_notify);
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void notify_handler(notify_signal_t *notify)
{
	switch (signal_state)
	{
		case QUICK_SIG:
			quick_signal_cnt++;
			on_off_switch();	
			

			if((quick_signal_cnt / 2) >= notify->quick_signal_num)
			{				
				off_signal_cb();
				signal_state = PAUSE_QUICK_SIG;
			}
			break;

		case PAUSE_QUICK_SIG:
			pause_signal_cnt++;
			
			if(pause_signal_cnt >= 6)
			{				
				signal_state = SLOW_SIG;	
				reset_signals_variables();
			}
			break;

		case SLOW_SIG:
			slow_signal_cnt++;

			if(slow_signal_cnt % 2 == 0)
			{
				on_off_switch();
			}

			if((slow_signal_cnt / 4) >= notify->slow_signal_num)
			{
				off_signal_cb();
				on_off_flag = OFF_SIG;
				signal_state = PAUSE_SLOW_SIG;
			}
			break;

		case PAUSE_SLOW_SIG:
			pause_signal_cnt++;
			
			if(pause_signal_cnt >= 6)
			{
				signal_state = QUICK_SIG;	
				reset_signals_variables();
			}
			break;
				
		default:
			break;
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void notify_timer_init(void)
{
	LOG_DEBUG_FUNC();
	NOTIFY_TIM_CLK_ENABLE();
	notify_timer.Instance = NOTIFY_TIM_INSTANCE;
	notify_timer.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	notify_timer.Init.CounterMode = TIM_COUNTERMODE_UP;
	notify_timer.Init.Prescaler = 16000-1;
	notify_timer.Init.Period = NOTIFY_TIM_INTERVAL_MS-1;
	notify_timer.Init.ClockDivision = 0;

	HAL_TIM_Base_Init(&notify_timer);
	HAL_NVIC_SetPriority(NOTIFY_TIM_IRQn, 4, 0);

	notif_timer_init = true;	
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void on_off_switch()
{
	if(on_off_flag == ON_SIG)
	{
		on_signal_cb();		
	}
	else
	{
		off_signal_cb();
	}

	on_off_flag = !on_off_flag;	
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void notify_timer_enable()
{
	LOG_DEBUG_FUNC();
	if(notif_timer_init == false)
	{		
		notify_timer_init();
	}

	__HAL_TIM_CLEAR_IT(&notify_timer, TIM_IT_UPDATE);
	HAL_NVIC_EnableIRQ(NOTIFY_TIM_IRQn);
	HAL_TIM_Base_Start_IT(&notify_timer);	
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void notify_timer_disable()
{
	LOG_DEBUG_FUNC();
	if(notif_timer_init == true)
	{		
		HAL_TIM_Base_Stop_IT(&notify_timer);
		HAL_NVIC_DisableIRQ(NOTIFY_TIM_IRQn);	
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void NOTIFY_TIM_IRQHandler()
{
	HAL_TIM_IRQHandler(&notify_timer);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(notify_signal_t *notify, 
									   uint8_t quick_sig_num, 
									   uint8_t slow_sig_num)
{
	return notify != NULL && (quick_sig_num > 0 || slow_sig_num > 0);
}									   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_notify_initialized(notify_signal_t *notify)
{
	return notify != NULL && notify->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_possible_register_signal_callback(notify_signal_on_off_cb sig_cb)
{
	return sig_cb != NULL && 
		   notify_enable_flag == false && enabled_notify == NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_possible_enable_notify(notify_signal_t *notify)
{
	return is_notify_initialized(notify) == true 
		   && on_signal_cb != NULL
		   && off_signal_cb != NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void reset_signals_variables()
{
	quick_signal_cnt = 0;
	slow_signal_cnt = 0;
	pause_signal_cnt = 0;

	on_off_flag = ON_SIG;
}
