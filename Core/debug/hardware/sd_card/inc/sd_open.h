#ifndef __SD_OPEN_H
#define __SD_OPEN_H

#include <stm32f7xx_hal.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern SD_HandleTypeDef hsd1;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void sd_hardware_init(void);

void sd_hardware_deinit(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


#endif /* __SD_OPEN_H */