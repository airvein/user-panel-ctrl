#ifndef __UART_OPEN_H
#define __UART_OPEN_H

#include <stm32f7xx_hal.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern UART_HandleTypeDef huart3;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern void uart_hardware_init(void);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


#endif /* __UART_OPEN_H */
