#include "settings_core/communication/settings_uart.h"
#include "debug/hardware/uart/inc/uart_open.h"

#include "debug/log/log.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

UART_HandleTypeDef huart3;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void uart_gpio_init();
void uart_peripherials_init();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void uart_hardware_init()
{
	uart_gpio_init();
	uart_peripherials_init();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void uart_gpio_init()
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	__HAL_RCC_USART3_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();

	GPIO_InitStruct.Pin = USART3_TX_PIN | USART3_RX_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART3;

	HAL_GPIO_Init(USART3_PORT, &GPIO_InitStruct);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void uart_peripherials_init()
{
	huart3.Instance = USART3;
	huart3.Init.BaudRate = BAUD_RATE;
	huart3.Init.WordLength = UART_WORDLENGTH_8B;
	huart3.Init.StopBits = UART_STOPBITS_1;
	huart3.Init.Parity = UART_PARITY_NONE;
	huart3.Init.Mode = UART_MODE_TX_RX;
	huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart3.Init.OverSampling = UART_OVERSAMPLING_16;
	huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;

	uint8_t uart_status = HAL_UART_Init(&huart3);

	ASSERT_LOG_INFO_G(uart_status == HAL_OK, "UART init OK!");
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
