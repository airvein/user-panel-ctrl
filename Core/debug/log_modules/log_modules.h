#ifndef __LOG_MODULES_H
#define __LOG_MODULES_H

#include "settings_app/debug/settings_log.h"
#include "debug/log/log.h"


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern log_module_t *log_hangar_mqtt_client;
extern log_module_t *log_mqtt_msg_ctrl;
extern log_module_t *log_mqtt_msg;
extern log_module_t *log_mqtt_request;
extern log_module_t *log_mqtt_request_ctrl;

extern log_module_t *log_procedure;
extern log_module_t *log_procedure_ctrl;
extern log_module_t *log_cmd_procedure;
extern log_module_t *log_cmd_procedure_ctrl;

extern log_module_t *log_usb_msg_ctrl;
extern log_module_t *log_usb_msg;
extern log_module_t *log_usb_request;
extern log_module_t *log_usb_request_ctrl;

extern log_module_t *log_sd_card;

extern log_module_t *log_plc_module;

extern log_module_t *log_diagnostic;
extern log_module_t *log_diagnostic_ctrl;
extern log_module_t *log_notify_sig;
extern log_module_t *log_timeout;
extern log_module_t *log_time_event;

extern log_module_t *log_controller;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void init_log_modules();


#endif /* __LOG_MODULES_H */
