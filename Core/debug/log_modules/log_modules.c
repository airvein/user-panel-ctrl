#include "log_modules.h"
#include "debug/interface/inc/uart_printf.h"
#include "debug/interface/inc/usb_printf.h"
#include <stdio.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_MODULE_INIT(log_module, name, settings)	\
								do { \
									log_module = log_module_create(); \
									 log_module_init(log_module, \
													 name, settings);  \
								} while(0);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

log_module_t *log_hangar_mqtt_client = NULL;
log_module_t *log_mqtt_msg_ctrl = NULL;
log_module_t *log_mqtt_msg = NULL;
log_module_t *log_mqtt_request = NULL;
log_module_t *log_mqtt_request_ctrl = NULL;

log_module_t *log_procedure = NULL;
log_module_t *log_procedure_ctrl = NULL;
log_module_t *log_cmd_procedure = NULL;
log_module_t *log_cmd_procedure_ctrl = NULL;

log_module_t *log_usb_msg_ctrl = NULL;
log_module_t *log_usb_msg = NULL;
log_module_t *log_usb_request = NULL;
log_module_t *log_usb_request_ctrl = NULL;

log_module_t *log_sd_card = NULL;

log_module_t *log_plc_module = NULL;

log_module_t *log_diagnostic = NULL;
log_module_t *log_diagnostic_ctrl = NULL;
log_module_t *log_notify_sig = NULL;
log_module_t *log_timeout = NULL;
log_module_t *log_time_event = NULL;

log_module_t *log_controller = NULL;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void init_log_modules()
{
	log_register_printf_clbk(usb_printf);
	log_set_global_log_level(LOG_GLOBAL_SETTINGS);
	//log_set_timestamp_var_ptr(&ev_system_timestamp);

	LOG_MODULE_INIT(log_hangar_mqtt_client, "hangar_mqtt_client", 
					LOG_HANGAR_MQTT_SETTINGS);

	LOG_MODULE_INIT(log_mqtt_msg_ctrl, "mqtt_msg_ctrl", 
					LOG_MQTT_MSG_CTRL_SETTINGS);
	
	LOG_MODULE_INIT(log_mqtt_msg, "mqtt_msg", LOG_MQTT_MSG_SETTINGS);

	LOG_MODULE_INIT(log_mqtt_request, "mqtt_request", LOG_MQTT_REQ_SETTINGS);

	LOG_MODULE_INIT(log_mqtt_request_ctrl, "mqtt_request_ctrl", 
					LOG_MQTT_REQ_CTRL_SETTINGS);

	LOG_MODULE_INIT(log_procedure, "procedure", LOG_PROCEDURE_SETTINGS);

	LOG_MODULE_INIT(log_procedure_ctrl, "procedure_ctrl", 
					LOG_PROC_CTRL_SETTINGS);

	LOG_MODULE_INIT(log_cmd_procedure, "cmd_procedure", 
					LOG_CMD_PROC_SETTINGS);

	LOG_MODULE_INIT(log_cmd_procedure_ctrl, "cmd_procedure_ctrl", 
					LOG_CMD_PROC_CTRL_SETTINGS);

	LOG_MODULE_INIT(log_usb_msg_ctrl, "usb_msg_ctrl", 
					LOG_USB_MSG_CTRL_SETTINGS);

	LOG_MODULE_INIT(log_usb_msg, "usb_msg", LOG_USB_MSG_SETTINGS);

	LOG_MODULE_INIT(log_usb_request, "usb_request", LOG_USB_REQ_SETTINGS);

	LOG_MODULE_INIT(log_usb_request_ctrl, "usb_request_ctrl", 
					LOG_USB_REQ_CTRL_SETTINGS);

	LOG_MODULE_INIT(log_plc_module, "plc_module", 
					LOG_PLC_MODULE_SETTINGS);

	LOG_MODULE_INIT(log_diagnostic, "diagnostic", LOG_DIAGNOSTIC_SETTINGS);


	LOG_MODULE_INIT(log_diagnostic_ctrl, "diagnostic_ctrl", 
					LOG_DIAGNOSTIC_CTRL_SETTINGS);
	
	LOG_MODULE_INIT(log_notify_sig, "notify", LOG_NOTIFY_SETTINGS);


    LOG_MODULE_INIT(log_sd_card, "sd_card", LOG_SD_CARD_SETTINGS);

	LOG_MODULE_INIT(log_controller, LOG_CTRL_NAME, LOG_CONTROLLER_SETTINGS);

	LOG_MODULE_INIT(log_timeout, "timeout", LOG_TIMEOUT_SETTINGS);

	LOG_MODULE_INIT(log_time_event, "time_event", LOG_TIME_EVENT_SETTINGS);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|