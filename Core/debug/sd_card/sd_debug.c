#include "settings_app/debug/settings_sd.h"
#include "debug/sd_card/inc/sd_debug.h"
#include "debug/log_modules/log_modules.h"
#include "peripherals_core/rtc/inc/rtc.h"
#include "fatfs.h"


#include <string.h>
#include <stdio.h>
#include <time.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                     E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool is_possible_save_logs = 0;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                     S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

FATFS file_sys;
FIL file;
FRESULT res = 0;
UINT write_cout, read_cout = 0;

char current_file_name[MAX_FILE_NAME_LEN] = "STARTSYS";
uint32_t current_day = 0;

bool is_sd_init = 0;
bool is_sd_filesys = 0;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void open_file();
static void close_file();

static bool check_current_day();
static bool create_new_file();
static void change_current_file_name();
static void write_to_file(char *message);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void sd_write_to_file(char *message)
{
    if (BSP_SD_IsDetected() != true)
    {
        log_warning_m(log_sd_card, "No SD card, cant write");
        is_sd_init = false;
        return;
    }

    if(is_sd_init == false )
    {
        sd_init_card();
        
        if(is_sd_filesys == false)
        {
            sd_init_filesys();
        }
    }

    if(check_current_day() != true)
    {
        change_current_file_name();
        create_new_file();
    }

    write_to_file(message);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void sd_init_card()
{
    if(BSP_SD_IsDetected() != true)
    {
        return;
    }

    if(BSP_SD_Init() != 0)
    {
        log_warning_m(log_sd_card, "Cant init SD card");
        is_sd_init = false;
    }
    else
    {
        log_info_m(log_sd_card, "SD card initialized");
        is_sd_init = true;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void sd_init_filesys()
{
    is_possible_save_logs = 1;
    
    if(BSP_SD_IsDetected() != true)
    {
        return;
    }

    if(f_mount(&file_sys, " ", 1) != false)
        {
            log_warning_m(log_sd_card, "Cant init SD filesys");
            is_sd_filesys = 0;
            return;
        }
    log_info_m(log_sd_card, "SD filesys init correctly");
    is_sd_filesys = 1;

    if(check_current_day() != true)
    {
        change_current_file_name();
        create_new_file();
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void open_file()
{
    res = f_open(&file, current_file_name, FA_WRITE | FA_OPEN_APPEND);
    
    if(res != 0)
    {
        log_warning_m(log_sd_card, "Cant open file [%s] with res [%d]",
                      current_file_name, res);
        return;
    }

    // log_debug_m(log_sd_card, "file [%s] opened with res = %d",
    //             current_file_name, res);
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void close_file()
{
    res = f_close(&file);

    if(res != 0)
    {
        log_warning_m(log_sd_card, "Cant close file [%s] with res [%d]",
                                    current_file_name, res);
        return;
    }

    // log_debug_m(log_sd_card, "file [%s] close with res = %d",
    //             current_file_name, res);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool create_new_file()
{
    if(f_open(&file, current_file_name, FA_CREATE_ALWAYS) != 0)
    {
        log_warning_m(log_sd_card, "Cant create new file with name: %s",
                      current_file_name);
        return 1;
    }

    log_info_m(log_sd_card, "New file created with name [%s]",
               current_file_name);

    if(f_close(&file) != 0)
    {
        log_warning_m(log_sd_card, "Cant close file before create");   
        return 1;
    }

    log_info_m(log_sd_card, "Correct closed file after create");
    return 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool check_current_day()
{
    uint32_t cd = ev_system_timestamp / NEW_FILE_EVERY;
    
    if(current_day != cd)
    {
        current_day = cd;
        log_info_m(log_sd_card, "Creating new file...");
        return 0;
    }
    
    // log_debug_m(log_sd_card, "Day is actual");
    return 1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

uint16_t dd = 0;

static void change_current_file_name()
{
    char new_file_name[MAX_FILE_NAME_LEN];

    struct tm datetime;
    time_t ts = ev_system_timestamp;

    datetime = *gmtime(&ts);

    sprintf(new_file_name, "%02d_%02d_%02d",
            dd, datetime.tm_mon+1, datetime.tm_year-100);

    dd++;

    memcpy(current_file_name, new_file_name, MAX_FILE_NAME_LEN);

    log_info_m(log_sd_card, "Change current file name to [%s]",
               current_file_name);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void write_to_file(char *message)
{
    open_file();

    uint16_t message_len = strlen(message);

    read_cout = message_len;
    res = f_write(&file, message, read_cout, &write_cout);

    if(res != 0)
    {
    // log_warning_m(log_sd_card, "Cant write message to file");
    }

    read_cout = strlen(" \r\n");
    res = f_write(&file, " \r\n", read_cout, &write_cout);

    // log_info_m(log_sd_card, "Write message to sd [%s]", all_msg, res);
    
    close_file();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
