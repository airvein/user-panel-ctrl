#ifndef __SD_DEBUG_H
#define __SD_DEBUG_H

#include "stdbool.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                     E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool is_possible_save_logs;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                      D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
      
#define MAX_FILE_NAME_LEN   9

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void sd_write_to_file(char *message);

void sd_init_card();

void sd_init_filesys();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SD_DEBUG_H */