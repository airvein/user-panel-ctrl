#ifndef __SETTINGS_PLC_MODULE_H
#define __SETTINGS_PLC_MODULE_H


#include "settings_core/global_settings_gpio.h"
#include "settings_core/global_settings_timer.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define PLC_ID	                0

#define PLC_SPIx	            SPI1
#define PLC_SPI_SCK_GPIO        GPIO_SPI1_SCK
#define PLC_SPI_MISO_GPIO       GPIO_SPI1_MISO
#define PLC_SPI_MOSI_GPIO       GPIO_SPI1_MOSI

#define PLC_CURRENT_CS_GPIO     GPIO_PLC_CURRENT_CS
#define PLC_RELAY_CS_GPIO       GPIO_PLC_RELAY_CS
#define PLC_RELAY_EN_GPIO       GPIO_PLC_RELAY_EN

// #define PLC_TIM_CLK_ENABLE()        __TIM2_CLK_ENABLE();
// #define PLC_TIM_INSTANCE            TIM2
// #define PLC_TIM_IRQn                TIM2_IRQn
// #define PLC_TIM_IRQHandler          TIM2_IRQHandler
#define PLC_TIM_INTERVAL_MS     50


#endif /* __SETTINGS_PLC_MODULE_H */