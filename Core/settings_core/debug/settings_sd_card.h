#ifndef __SETTINGS_SD_CARD_H
#define __SETTINGS_SD_CARD_H

#include "settings_core/global_settings_gpio.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define SDMCC1_D0_CK_PORT          GPIO_SDMCC1_D0_CK_PORT
#define SDMCC1_D0_PIN              GPIO_SDMCC1_D0_PIN
#define SDMCC1_CK_PIN              GPIO_SDMCC1_CK_PIN

#define SDMCC1_CMD_PORT            GPIO_SDMCC1_CMD_PORT
#define SDMCC1_CMD_PIN             GPIO_SDMCC1_CMD_PIN

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_SD_CARD_H */ 