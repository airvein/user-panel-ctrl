#ifndef __SETTINGS_NOTIFY_SIGNAL_H
#define __SETTINGS_NOTIFY_SIGNAL_H

#include "settings_core/global_settings_timer.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

// #define NOTIFY_TIM_CLK_ENABLE()    __TIM3_CLK_ENABLE();
// #define NOTIFY_TIM_INSTANCE        TIM3
// #define NOTIFY_TIM_IRQn            TIM3_IRQn
// #define NOTIFY_TIM_IRQHandler      TIM3_IRQHandler
#define NOTIFY_TIM_INTERVAL_MS     1000


#endif /* __SETTINGS_NOTIFY_SIGNAL_H */