#ifndef __SETTINGS_UART_H
#define __SETTINGS_UART_H

#include "settings_core/global_settings_gpio.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define BAUD_RATE			115200

#define USART3_TX_PIN		GPIO_USART3_TX_PIN
#define USART3_RX_PIN		GPIO_USART3_RX_PIN

#define USART3_PORT			GPIO_USART3_PORT

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_UART_H */