#ifndef __SETTINGS_ETH_H
#define __SETTINGS_ETH_H

#include "settings_core/global_settings_gpio.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ETH_PORT_A                          GPIO_ETH_PORT_A     
#define ETH_REF_CLK_PIN                     GPIO_ETH_REF_CLK_PIN
#define ETH_MDIO_PIN                        GPIO_ETH_MDIO_PIN   
#define ETH_CRS_DV_PIN                      GPIO_ETH_CRS_DV_PIN 
#define ETH_PORT_B                          GPIO_ETH_PORT_B     
#define ETH_TXD1_PIN                        GPIO_ETH_TXD1_PIN   
#define ETH_PORT_C                          GPIO_ETH_PORT_C     
#define ETH_MDC_PIN                         GPIO_ETH_MDC_PIN    
#define ETH_RXD0_PIN                        GPIO_ETH_RXD0_PIN   
#define ETH_RXD1_PIN                        GPIO_ETH_RXD1_PIN   
#define ETH_PORT_G                          GPIO_ETH_PORT_G     
#define ETH_TX_EN_PIN                       GPIO_ETH_TX_EN_PIN  
#define ETH_TXD0_PIN                        GPIO_ETH_TXD0_PIN   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __SETTINGS_ETH_H */