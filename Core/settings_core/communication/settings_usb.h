#ifndef __SETTINGS_USB_H
#define __SETTINGS_USB_H

#include "settings_core/global_settings_gpio.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define USB_PowerSwitchOn_GPIO_Port         G_USB_PowerSwitchOn_GPIO_Port
#define USB_PowerSwitchOn_Pin               G_USB_PowerSwitchOn_Pin
#define USB_OverCurrent_GPIO_Port           G_USB_OverCurrent_GPIO_Port
#define USB_OverCurrent_Pin                 G_USB_OverCurrent_Pin
#define USB_SOF_GPIO_Port                   G_USB_SOF_GPIO_Port
#define USB_SOF_Pin                         G_USB_SOF_Pin
#define USB_VBUS_GPIO_Port                  G_USB_VBUS_GPIO_Port
#define USB_VBUS_Pin                        G_USB_VBUS_Pin
#define USB_ID_GPIO_Port                    G_USB_ID_GPIO_Port
#define USB_ID_Pin                          G_USB_ID_Pin
#define USB_DM_GPIO_Port                    G_USB_DM_GPIO_Port
#define USB_DM_Pin                          G_USB_DM_Pin
#define USB_DP_GPIO_Port                    G_USB_DP_GPIO_Port
#define USB_DP_Pin                          G_USB_DP_Pin

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#endif /* __SETTINGS_USB_H */