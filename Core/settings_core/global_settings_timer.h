#ifndef __GLOBAL_SETTINGS_TIMER_H
#define __GLOBAL_SETTINGS_TIMER_H

#include "stm32f7xx_hal.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define PLC_TIM_CLK_ENABLE()            __TIM2_CLK_ENABLE();
#define PLC_TIM_INSTANCE                TIM2
#define PLC_TIM_IRQn                    TIM2_IRQn
#define PLC_TIM_IRQHandler              TIM2_IRQHandler


#define NOTIFY_TIM_CLK_ENABLE()         __TIM3_CLK_ENABLE();
#define NOTIFY_TIM_INSTANCE             TIM3
#define NOTIFY_TIM_IRQn                 TIM3_IRQn
#define NOTIFY_TIM_IRQHandler           TIM3_IRQHandler


#define TIMEOUT_TIM_CLK_ENABLE()        __TIM9_CLK_ENABLE();
#define TIMEOUT_TIM_INSTANCE            TIM9
#define TIMEOUT_TIM_IRQn                TIM1_BRK_TIM9_IRQn
#define TIMEOUT_TIM_IRQHandler          TIM1_BRK_TIM9_IRQHandler
#define TIMEOUT_TIM_INTERVAL_MS         1000


#define TIME_EVENT_TIM_CLK_ENABLE()     __TIM10_CLK_ENABLE();
#define TIME_EVENT_TIM_INSTANCE         TIM10
#define TIME_EVENT_TIM_IRQn             TIM1_UP_TIM10_IRQn
#define TIME_EVENT_TIM_IRQHandler       TIM1_UP_TIM10_IRQHandler
#define TIME_EVENT_TIM_INTERVAL_MS      10000


#endif /* __GLOBAL_SETTINGS_TIMER_H */