#ifndef __HANGAR_COMMUNICATION_H
#define __HANGAR_COMMUNICATION_H

#include "communication_interface/hangar/inc/hangar_mqtt_msg.h"
#include "communication_interface/hangar/inc/hangar_mqtt_req.h"
#include "hangar_mqtt_client.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_communication_init(void);

void hangar_communication_subscribe_all_msg();


#endif /* __HANGAR_COMMUNICATION_H */
