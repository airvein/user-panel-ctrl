/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : mqtt_client header file
  * @author			: Mateusz Strojek
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 Cervi Robotics
  */

#ifndef __HANGAR_MQTT_CLIENT_H
#define __HANGAR_MQTT_CLIENT_H

#include "lwip/apps/mqtt.h"
#include <stdbool.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern mqtt_client_t *hangar_mqtt_client;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_mqtt_client_init(void);
void hangar_mqtt_connect(void);
bool hangar_mqtt_is_connected(void);
void hangar_mqtt_disconnect(void);


#endif /* __HANGAR_MQTT_CLIENT_H */
