/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : mqtt_client body
  * @author			: Mateusz Strojek
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 Cervi Robotics
  */

#include "inc/hangar_mqtt_client.h"

#include "communication_interface/hangar/inc/hangar_mqtt_msg.h"
#include "communication_interface/hangar/inc/hangar_mqtt_req.h"

#include "settings_app/communication/settings_mqtt.h"

#include "debug/log_modules/log_modules.h"

#include <stdio.h>
#include <string.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_client_t *hangar_mqtt_client = NULL;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void create_client_object(void);

static void connection_cb(mqtt_client_t *client, void *arg, 
                          mqtt_connection_status_t status);

static void incoming_publish_cb(void *arg, const char *topic, 
                                uint32_t topic_len);

static void incoming_data_cb(void *arg, const uint8_t *data,
                                  uint16_t len, uint8_t flags);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_mqtt_client_init(void)
{
    create_client_object();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_mqtt_connect(void)
{    
    create_client_object();

    ip4_addr_t hangar_broker_ipaddr;
    struct mqtt_connect_client_info_t client_info;
    err_t err;
    IP4_ADDR(&hangar_broker_ipaddr, _BROKER_ADDR1, _BROKER_ADDR2,
    		_BROKER_ADDR3, _BROKER_ADDR4);
    memset(&client_info, 0, sizeof(client_info));

    client_info.client_id = MQTT_MODULE_NAME;
    client_info.client_user = MQTT_MODULE_USER;
    client_info.client_pass = MQTT_MODULE_PSWD;

    err = mqtt_client_connect(hangar_mqtt_client, &hangar_broker_ipaddr, 
                            HANGAR_MQTT_BROKER_PORT, 
                            connection_cb, 0, &client_info);

    ASSERT_LOG_INFO_M(err == ERR_OK, log_hangar_mqtt_client, 
        "Connect configuration OK");
    ASSERT_LOG_ERROR_M(err != ERR_OK, log_hangar_mqtt_client,
        "Connect configuration, err %d!", err);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool hangar_mqtt_is_connected(void)
{
    if(hangar_mqtt_client != NULL)
    {
        return mqtt_client_is_connected(hangar_mqtt_client);
    }
    else
    {
        return false;
    }    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_mqtt_disconnect(void)
{
    if(hangar_mqtt_client != NULL)
    {
        mqtt_disconnect(hangar_mqtt_client);
    }    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void create_client_object(void)
{
    if(hangar_mqtt_client == NULL)
    {
        log_debug_m(log_hangar_mqtt_client, "creating hangar mqtt client...");

        hangar_mqtt_client = mqtt_client_new();
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void connection_cb(mqtt_client_t *client, void *arg, 
                          mqtt_connection_status_t status)
{
    if (status == MQTT_CONNECT_ACCEPTED) 
    {

        log_info_m(log_hangar_mqtt_client, 
                    "Connection: Successfully connected");
        
        mqtt_set_inpub_callback(client, incoming_publish_cb, 
                                incoming_data_cb, arg);
    } 
    else 
    {
        log_warning_m(log_hangar_mqtt_client, 
            "Connection: Disconnected, reason: %d", status);

        mqtt_msg_ctrl_set_all_msg_unsubscribed(&hangar_mqtt_msg_ctrl);
        mqtt_request_ctrl_set_all_msg_unsubscribed(&hangar_mqtt_req_ctrl);

        /* Try reconnect */
        hangar_mqtt_connect();
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void incoming_publish_cb(void *arg, const char *topic, 
                                uint32_t topic_len)
{    
    log_debug_m(log_hangar_mqtt_client,
                "Incoming publish at topic %s with total length %u",
                topic, (unsigned int) topic_len);

    if(mqtt_msg_ctrl_topic_handler(&hangar_mqtt_msg_ctrl,
                                   topic, topic_len) == false)
    {
        mqtt_request_ctrl_topic_handler(&hangar_mqtt_req_ctrl, 
                                        topic, topic_len);
    }    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void incoming_data_cb(void *arg, const uint8_t *data,
                                  uint16_t len, uint8_t flags)
{
    log_debug_m(log_hangar_mqtt_client, 
                "Incoming publish payload with length %d, flags %u",
                len, (unsigned int) flags);

    if (flags & MQTT_DATA_FLAG_LAST) 
    {
        static char payload[255];
        memcpy(payload, (const char*)data, len);

        mqtt_msg_ctrl_payload_handler(&hangar_mqtt_msg_ctrl,
                                      payload, len);
        mqtt_request_ctrl_payload_handler(&hangar_mqtt_req_ctrl, 
                                          payload, len);
    } 
    else 
    {
        /* TODO : sprawdzic kiedy sie wywoluje */
    	log_debug_g("mqtt incoming data idk: %s", (const char * ) data);
    }
}
