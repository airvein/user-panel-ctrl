#include "inc/hangar_communication.h"
#include "logic/hangar_ctrl_reset/inc/hangar_ctrl_reset.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_communication_init(void)
{
	hangar_mqtt_client_init();
    hangar_mqtt_msg_init();
    hangar_mqtt_req_init();
    hangar_mqtt_connect();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void hangar_communication_subscribe_all_msg()
{
    if(hangar_mqtt_is_connected() == true)
    {
        mqtt_msg_ctrl_subscribe_all_msg(&hangar_mqtt_msg_ctrl);
        if(mqtt_msg_ctrl_is_all_msgs_subscribed(&hangar_mqtt_msg_ctrl) == true)
        {
            mqtt_request_ctrl_subscribe_all_msg(&hangar_mqtt_req_ctrl);
        }
    }
    hangar_ctrl_reset_handler();
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|