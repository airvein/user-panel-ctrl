#ifndef __SERVICE_COMMUNICATION_H
#define __SERVICE_COMMUNICATION_H

#include "communication_interface/service/inc/service_usb_msg.h"
#include "communication_interface/service/inc/service_usb_req.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void service_communication_init(void);


#endif /* __SERVICE_COMMUNICATION_H */
