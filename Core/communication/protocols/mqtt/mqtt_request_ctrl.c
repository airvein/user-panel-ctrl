#include "inc/mqtt_request_ctrl.h"

#include "debug/log_modules/log_modules.h"

#include <string.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_mqtt_request_ctrl, "%s : %s", \
                                    __FILE__, __func__))

#define ORDER_REQ_NONE  (int16_t)-1                                    

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(mqtt_request_ctrl_t *mqtt_request_ctrl,
                                        mqtt_request_t **mqtt_request_array, 
                                        uint16_t request_array_size);

static bool is_mqtt_request_ctrl_initialize(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl);  

static msg_state_t get_sub_req_state(mqtt_request_ctrl_t *mqtt_request_ctrl);                                    

static mqtt_msg_t *get_sub_mqtt_req_ptr(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl);                                      

static bool is_all_mqtt_req_subscribed(mqtt_request_ctrl_t *mqtt_request_ctrl);

static bool is_mqtt_msg_incoming(mqtt_msg_t *mqtt_msg, 
                                const char *incoming_topic);

static mqtt_msg_t *get_mqtt_request_resp_msg_ptr(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl,
                                    int req_id);

static bool is_correct_order_req_id(mqtt_request_ctrl_t *mqtt_request_ctrl, 
                                    const char *payload, uint16_t payload_len);

static mqtt_msg_t *get_order_mqtt_req_resp_msg_ptr(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl); 
                                    
static bool is_mqtt_msg_init(mqtt_msg_t *mqtt_msg);                                                                       

static mqtt_request_t *get_order_mqtt_request(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_request_ctrl_t *mqtt_request_ctrl_create(void)
{
    return (mqtt_request_ctrl_t*)malloc(sizeof(mqtt_request_ctrl_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_request_ctrl_init(mqtt_request_ctrl_t *mqtt_request_ctrl,
                            mqtt_request_t **mqtt_request_array, 
                            uint16_t request_array_size)
{
    LOG_DEBUG_FUNC();
    if(is_correct_init_parameters(mqtt_request_ctrl, mqtt_request_array, 
                                  request_array_size) == false)
    {
        log_error_m(log_mqtt_request_ctrl, "Incorrect init parameters!");
        return false;
    }
    //mqtt_request_ctrl->msg_array = malloc(sizeof(mqtt_request_t)*msg_array_size);
    mqtt_request_ctrl->sub_req_id = 0;
    mqtt_request_ctrl->requests_array = mqtt_request_array;
    mqtt_request_ctrl->requests_num = request_array_size;

    mqtt_request_ctrl->order_req_id = ORDER_REQ_NONE;

    mqtt_request_ctrl->is_init = true;
    log_debug_m(log_mqtt_request_ctrl, "Init mqtt request controller correctly!");
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void mqtt_request_ctrl_subscribe_all_msg(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl)
{
    if(mqtt_request_ctrl->sub_req_id < mqtt_request_ctrl->requests_num)
	{
    	LOG_DEBUG_FUNC();
        if(is_mqtt_request_ctrl_initialize(mqtt_request_ctrl) == false)
        {
            log_error_m(log_mqtt_request_ctrl,
                "Subscribe all mqtt request error" 
                "[mqtt_request_ctrl ptr is NULL]");
                
            return ;
        }

        msg_state_t current_sub_req_state = 
                                    get_sub_req_state(mqtt_request_ctrl);
        log_debug_m(log_mqtt_request_ctrl, 
                    "Current req sub [ %s ] state %d", 
                    get_sub_mqtt_req_ptr(mqtt_request_ctrl)->topic_path,
                    current_sub_req_state);

        switch(current_sub_req_state)
        {
            case MSG_STATE_SUBSCRIBING:
                log_debug_m(log_mqtt_request_ctrl, 
                        "mqtt req %s still subcribing..., waiting", 
                        get_sub_mqtt_req_ptr(mqtt_request_ctrl)->topic_path);
                break;

            case MSG_STATE_SUBSCRIBED:                
                mqtt_request_ctrl->sub_req_id++;
                if(is_all_mqtt_req_subscribed(mqtt_request_ctrl) == true)
                {   
                    log_info_m(log_mqtt_request_ctrl, 
                               "All requests are subscribed!");
                    break;
                }
                log_debug_m(log_mqtt_request_ctrl, 
                            "Mqtt req subscribed, increment sub_req_id");
                break;

            case MSG_STATE_UNSUBSCRIBED:
                log_debug_m(log_mqtt_request_ctrl, 
                            "Invoke subscribe mqtt_req [%s]", 
                            get_sub_mqtt_req_ptr(mqtt_request_ctrl)->topic_path);

                err_t res = 
                    mqtt_msg_subscribe(get_sub_mqtt_req_ptr(mqtt_request_ctrl));

                ASSERT_LOG_DEBUG_M(res == ERR_OK, log_mqtt_request_ctrl, 
                        "mqtt req %s subscribe invoke ok", 
                        get_sub_mqtt_req_ptr(mqtt_request_ctrl)->topic_path);

                ASSERT_LOG_ERROR_M(res != ERR_OK, log_mqtt_msg_ctrl, 
                        "mqtt req %s subscribe invoke, err %d", 
                        get_sub_mqtt_req_ptr(mqtt_request_ctrl)->topic_path, res);
                break;

            default:
                log_error_m(log_mqtt_request_ctrl, 
                            "Incorrect switch-case state msg %d",
                            current_sub_req_state);
                break;
        }
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_request_ctrl_is_all_msgs_subscribed(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl)
{
    return mqtt_request_ctrl->sub_req_id >= mqtt_request_ctrl->requests_num;
}                                    

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_request_ctrl_topic_handler(mqtt_request_ctrl_t *mqtt_request_ctrl, 
                                 const char *topic_path, uint32_t topic_len)
{
    LOG_DEBUG_FUNC();
    if(is_mqtt_request_ctrl_initialize(mqtt_request_ctrl) == false)
    {
        return false;
    }

    log_debug_m(log_mqtt_request_ctrl,
               "Start searching mqtt_req [%s]", topic_path);

    for(int req_id = 0; req_id < mqtt_request_ctrl->requests_num; req_id++)
    {
        mqtt_msg_t *msg = get_mqtt_request_resp_msg_ptr(mqtt_request_ctrl,
                                                        req_id);
        if(is_mqtt_msg_incoming(msg, topic_path) == true)
        {
            log_debug_m(log_mqtt_msg_ctrl, "Incoming req [%s] found at %d ID!",
                        msg->topic_path, req_id);
            
            mqtt_request_ctrl->order_req_id = req_id;
            return true;
        }
    }    
    mqtt_request_ctrl->order_req_id = ORDER_REQ_NONE;
    log_info_m(log_mqtt_request_ctrl, "Incoming req [%s] not found!", 
                topic_path);
    return false;
}   

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_request_ctrl_payload_handler(mqtt_request_ctrl_t *mqtt_request_ctrl, 
                                   const char *payload, uint16_t payload_len)
{
    LOG_DEBUG_FUNC();
    if(is_mqtt_request_ctrl_initialize(mqtt_request_ctrl) == false)
    {
        return false;
    }

    if(is_correct_order_req_id(mqtt_request_ctrl, payload, payload_len) 
                                                        == false)
    {
        return false;
    }

    if(get_order_mqtt_request(mqtt_request_ctrl)->state == MQTT_REQ_WAIT_FOR_RESP)
    {
        mqtt_msg_t *order_msg = get_order_mqtt_req_resp_msg_ptr(mqtt_request_ctrl);

        if(is_mqtt_msg_init(order_msg) == true)
        {
            log_debug_m(log_mqtt_request_ctrl, "Order mqtt req [%s] invoke!", 
                        order_msg->topic_path);
            mqtt_msg_set_incoming_payload(order_msg, payload, payload_len);

            log_info_m(log_mqtt_msg_ctrl, 
                    "Incoming req [ %s { %s } ( %d ) ] found!",
                    order_msg->topic_path, order_msg->payload, 
                    order_msg->order_id);
            
            order_msg->sub_data_incoming_cb(order_msg);
            get_order_mqtt_request(mqtt_request_ctrl)->state = MQTT_REQ_IDLE;
            mqtt_request_ctrl->order_req_id = ORDER_REQ_NONE;

            return true;
        }
        else
        {
            log_error_m(log_mqtt_request_ctrl, 
                        "Order mqtt req pointer is not init correctly!");
            mqtt_request_ctrl->order_req_id = ORDER_REQ_NONE;
            return false;
        }
    }
    else
    {
        log_warning_m(log_mqtt_request_ctrl,
                      "mqtt req [ %s ] not expected, skipped!",
            get_order_mqtt_request(mqtt_request_ctrl)->resp_msg.topic_path);
        mqtt_request_ctrl->order_req_id = ORDER_REQ_NONE;
        return false;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_request_ctrl_set_all_msg_unsubscribed(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl)
{                                        
    if(is_mqtt_request_ctrl_initialize(mqtt_request_ctrl) == false)
    {
        return false;
    }

    if(mqtt_request_ctrl->requests_num > 0)
    {
        for(int id = 0; id < mqtt_request_ctrl->requests_num; id++)
        {
            mqtt_request_ctrl->requests_array[id]->resp_msg.state 
                                                    = MSG_STATE_UNSUBSCRIBED;
        }
        mqtt_request_ctrl->sub_req_id = 0;
        log_info_m(log_mqtt_request_ctrl, "Unsubscribed all requests!");
        return true;
    }
    return false;    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(mqtt_request_ctrl_t *mqtt_request_ctrl,
                                        mqtt_request_t **mqtt_request_array, 
                                        uint16_t request_array_size)
{
    return mqtt_request_ctrl != NULL && mqtt_request_array != NULL 
            && request_array_size > 0;
}           

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_mqtt_request_ctrl_initialize(
                                        mqtt_request_ctrl_t *mqtt_request_ctrl)
{
    bool retval = mqtt_request_ctrl != NULL 
                    && mqtt_request_ctrl->is_init == true;

    ASSERT_LOG_DEBUG_M(retval == false, 
                    log_mqtt_request_ctrl, 
                    "mqtt_request_ctrl is not initialized!");
    ASSERT_LOG_DEBUG_M(retval == true,
                    log_mqtt_request_ctrl, 
                    "mqtt_request_ctrl is initialized!");

    return retval;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static msg_state_t get_sub_req_state(mqtt_request_ctrl_t *mqtt_request_ctrl)
{
    return 
    mqtt_request_ctrl->requests_array[
                                mqtt_request_ctrl->sub_req_id]->resp_msg.state;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static mqtt_msg_t *get_sub_mqtt_req_ptr(
                                        mqtt_request_ctrl_t *mqtt_request_ctrl)
{
    return 
    &mqtt_request_ctrl->requests_array[mqtt_request_ctrl->sub_req_id]->resp_msg;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_all_mqtt_req_subscribed(mqtt_request_ctrl_t *mqtt_request_ctrl)
{
    return mqtt_request_ctrl->sub_req_id >= mqtt_request_ctrl->requests_num;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_mqtt_msg_incoming(mqtt_msg_t *mqtt_msg, 
                                const char *incoming_topic)
{
    return (strcmp(mqtt_msg->topic_path, incoming_topic) == 0);
}    

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static mqtt_msg_t *get_mqtt_request_resp_msg_ptr(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl,
                                    int req_id)
{
    return &mqtt_request_ctrl->requests_array[req_id]->resp_msg;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_order_req_id(mqtt_request_ctrl_t *mqtt_request_ctrl, 
                                    const char *payload, uint16_t payload_len)
{
    return mqtt_request_ctrl->order_req_id != ORDER_REQ_NONE 
        && mqtt_request_ctrl->order_req_id < mqtt_request_ctrl->requests_num
        && payload != NULL && payload_len > 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static mqtt_msg_t *get_order_mqtt_req_resp_msg_ptr(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl)
{
    if(mqtt_request_ctrl->requests_array != NULL 
        && mqtt_request_ctrl->requests_num > 0)
    {
        return &mqtt_request_ctrl->requests_array[
                                    mqtt_request_ctrl->order_req_id]->resp_msg;
    }
    else
    {
        return NULL;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_mqtt_msg_init(mqtt_msg_t *mqtt_msg)
{
    return mqtt_msg != NULL && mqtt_msg->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static mqtt_request_t *get_order_mqtt_request(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl)
{
    return mqtt_request_ctrl->requests_array[mqtt_request_ctrl->order_req_id];
}                                    