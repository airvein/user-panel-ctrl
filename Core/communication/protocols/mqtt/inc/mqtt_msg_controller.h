#ifndef __MQTT_MSG_CONTROLLER_H
#define __MQTT_MSG_CONTROLLER_H

#include "communication/protocols/mqtt/inc/mqtt_msg.h"

typedef struct _mqtt_msg_ctrl_s

{   
    mqtt_msg_t **msg_array;
    uint16_t msg_array_size;

    uint16_t sub_msg_id;
    
    int16_t order_msg_id;

    bool is_init;

} mqtt_msg_ctrl_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_msg_ctrl_t *mqtt_msg_ctrl_create(void);

bool mqtt_msg_ctrl_init(mqtt_msg_ctrl_t *mqtt_msg_ctrl,
                        mqtt_msg_t **mqtt_msg_array, uint16_t msg_array_size);

void mqtt_msg_ctrl_subscribe_all_msg(mqtt_msg_ctrl_t *mqtt_msg_ctrl);

bool mqtt_msg_ctrl_is_all_msgs_subscribed(mqtt_msg_ctrl_t *mqtt_msg_ctrl);

bool mqtt_msg_ctrl_topic_handler(mqtt_msg_ctrl_t *mqtt_msg_ctrl, 
                                 const char *topic_path, uint32_t topic_len);

bool mqtt_msg_ctrl_payload_handler(mqtt_msg_ctrl_t *mqtt_msg_ctrl, 
                                   const char *payload, uint16_t payload_len);

bool mqtt_msg_ctrl_set_all_msg_unsubscribed(mqtt_msg_ctrl_t *mqtt_msg_ctrl);


#endif /* __MQTT_MSG_CONTROLLER_H */