#ifndef __MQTT_REQUEST_CTRL_H
#define __MQTT_REQUEST_CTRL_H

#include "mqtt_request.h"
#include <stdbool.h>
#include <stdint.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _mqtt_request_ctrl_s
{
    mqtt_request_t **requests_array;
    uint16_t requests_num;

    uint16_t sub_req_id;
    
    int16_t order_req_id;

    bool is_init;

} mqtt_request_ctrl_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_request_ctrl_t *mqtt_request_ctrl_create(void);

bool mqtt_request_ctrl_init(mqtt_request_ctrl_t *mqtt_request_ctrl,
                            mqtt_request_t **mqtt_request_array, 
                            uint16_t request_array_size);

void mqtt_request_ctrl_subscribe_all_msg(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl);

bool mqtt_request_ctrl_is_all_msgs_subscribed(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl);

bool mqtt_request_ctrl_topic_handler(mqtt_request_ctrl_t *mqtt_request_ctrl, 
                                 const char *topic_path, uint32_t topic_len);

bool mqtt_request_ctrl_payload_handler(mqtt_request_ctrl_t *mqtt_request_ctrl, 
                                   const char *payload, uint16_t payload_len);

bool mqtt_request_ctrl_set_all_msg_unsubscribed(
                                    mqtt_request_ctrl_t *mqtt_request_ctrl);

#endif /* __MQTT_REQUEST_CTRL_H */