#ifndef __MQTT_REQUEST_H
#define __MQTT_REQUEST_H



#include "mqtt_msg.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define MQTT_REQ_NAME_MAX_LEN   20
#define MQTT_REQ_NO_RESP        ""

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _mqtt_request_ mqtt_request_t;

typedef bool (*mqtt_request_response_cb)(mqtt_msg_t*);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _mqtt_request_state_e
{
    MQTT_REQ_IDLE,
    MQTT_REQ_WAIT_FOR_RESP,

} mqtt_request_state_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct _mqtt_request_
{
    char name[MQTT_REQ_NAME_MAX_LEN];
    
    mqtt_client_t *mqtt_client;
    mqtt_msg_t req_msg;
    mqtt_msg_t resp_msg;

    mqtt_request_state_t state;
    
    bool is_init;

};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_request_t* mqtt_request_create();

bool mqtt_request_init(mqtt_request_t *mqtt_request, char *name, 
                        mqtt_client_t *client);

bool mqtt_request_req_msg_init(mqtt_request_t *mqtt_request, char *topic_path);

bool mqtt_request_resp_msg_init(mqtt_request_t *mqtt_request, char *topic_path, 
                                mqtt_request_response_cb resp_cb);

bool mqtt_request_send_req(mqtt_request_t *mqtt_request, char *payload);

void mqtt_request_reset(mqtt_request_t *mqtt_request);

bool mqtt_request_is_response_receive(mqtt_request_t *mqtt_request);


#endif /* __MQTT_REQUEST_H */