#ifndef __MQTT_MSG_H
#define __MQTT_MSG_H

#include "lwip/apps/mqtt.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define MQTT_MSG_PAYLOAD_MAX_LEN    150
#define MQTT_TOPIC_PATH_MAX_LEN     150

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _mqtt_msg_s mqtt_msg_t;

typedef bool (*sub_data_incoming)(mqtt_msg_t*);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum
{
    QOS_0 = 0,
    QOS_1,
    QOS_2

} qos_enum_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum
{
    MSG_STATE_UNSUBSCRIBED,
    MSG_STATE_SUBSCRIBING,
    MSG_STATE_SUBSCRIBED,
    MSG_STATE_UNSUBSCRIBING,

    MSG_STATE_PUBLISHING,
    MSG_STATE_PUBLISHING_ERR,
    MSG_STATE_PUBLISHED,
    MSG_STATE_PUBLISHED_ERR

} msg_state_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct _mqtt_msg_s

{
    mqtt_client_t *mqtt_client_ptr;

    qos_enum_t qos;
    uint8_t retain;
    
    char topic_path[MQTT_TOPIC_PATH_MAX_LEN];
    uint32_t topic_path_len;

    msg_state_t state;
    
    char payload[MQTT_MSG_PAYLOAD_MAX_LEN];
    uint32_t payload_len;

    uint32_t order_id;

    sub_data_incoming sub_data_incoming_cb;

    bool is_init;

};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_msg_t *mqtt_msg_create(void);

bool mqtt_msg_init(mqtt_msg_t *mqtt_msg, mqtt_client_t *client, qos_enum_t qos, 
                   char *topic_path, sub_data_incoming sub_data_incoming_cb);

err_t mqtt_msg_publish(mqtt_msg_t *mqtt_msg, char *payload);

err_t mqtt_msg_subscribe(mqtt_msg_t* mqtt_msg);

err_t mqtt_msg_unsubscribe(mqtt_msg_t *mqtt_msg);

bool mqtt_msg_set_incoming_payload(mqtt_msg_t *mqtt_msg, const char *payload,
                                   uint16_t payload_len);

bool mqtt_msg_no_sub_cb(mqtt_msg_t *mm);


#endif /* __MQTT_MSG_H */
