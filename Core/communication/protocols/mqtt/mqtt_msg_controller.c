#include "communication/protocols/mqtt/inc/mqtt_msg_controller.h"
#include "communication/protocols/mqtt/inc/mqtt_msg.h"
#include "debug/log_modules/log_modules.h"

#include <string.h>


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ORDER_MSG_NONE  (int16_t)-1

#define LOG_DEBUG_FUNC() (log_debug_m(log_mqtt_msg_ctrl, "%s : %s", __FILE__, \
                                    __func__))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parametes(mqtt_msg_ctrl_t *mqtt_msg_ctrl,
                        mqtt_msg_t **mqtt_msg_array, uint16_t msg_array_size);

static bool is_mqtt_msg_ctrl_initialize(mqtt_msg_ctrl_t *mqtt_msg_ctrl);

static bool is_mqtt_msg_incoming(mqtt_msg_t *mqtt_msg, 
                                const char *incoming_topic);

static mqtt_msg_t *get_order_mqtt_msg_ptr(mqtt_msg_ctrl_t *mqtt_msg_ctrl);

static bool is_correct_order_msg_id(mqtt_msg_ctrl_t *mqtt_msg_ctrl,
                                   const char *payload, uint16_t payload_len);

static bool is_mqtt_msg_init(mqtt_msg_t *mqtt_msg);

static msg_state_t get_sub_msg_state(mqtt_msg_ctrl_t *mqtt_msg_ctrl);

static mqtt_msg_t *get_sub_mqtt_msg_ptr(mqtt_msg_ctrl_t *mqtt_msg_ctrl);

static bool is_all_mqtt_msg_subscribed(mqtt_msg_ctrl_t *mqtt_msg_ctrl);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_msg_ctrl_t *mqtt_msg_ctrl_create(void)
{
    return (mqtt_msg_ctrl_t*)malloc(sizeof(mqtt_msg_ctrl_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_msg_ctrl_init(mqtt_msg_ctrl_t *mqtt_msg_ctrl,
                        mqtt_msg_t **mqtt_msg_array, uint16_t msg_array_size)
{
    LOG_DEBUG_FUNC();
    if(is_correct_init_parametes(mqtt_msg_ctrl, mqtt_msg_array, 
                                 msg_array_size) == false)
    {
        log_error_m(log_mqtt_msg_ctrl, "Incorrect init parameters!");
        return false;
    }
    //mqtt_msg_ctrl->msg_array = malloc(sizeof(mqtt_msg_t)*msg_array_size);
    mqtt_msg_ctrl->sub_msg_id = 0;
    mqtt_msg_ctrl->msg_array = mqtt_msg_array;
    mqtt_msg_ctrl->msg_array_size = msg_array_size;

    mqtt_msg_ctrl->order_msg_id = ORDER_MSG_NONE;

    mqtt_msg_ctrl->is_init = true;
    log_debug_m(log_mqtt_msg_ctrl, "Init mqtt msg controller correctly!");
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void mqtt_msg_ctrl_subscribe_all_msg(mqtt_msg_ctrl_t *mqtt_msg_ctrl)
{
    if(mqtt_msg_ctrl_is_all_msgs_subscribed(mqtt_msg_ctrl) == false)
	{
    	LOG_DEBUG_FUNC();
        if(is_mqtt_msg_ctrl_initialize(mqtt_msg_ctrl) == false)
        {
            log_error_m(log_mqtt_msg_ctrl,
                "Subscribe all mqtt msg error [mqtt_msg_ctrl ptr is NULL]");
                
            return ;
        }

        msg_state_t current_sub_msg_state = 
                                    get_sub_msg_state(mqtt_msg_ctrl);
        log_debug_m(log_mqtt_msg_ctrl, 
                    "Current msg sub [ %s ] state %d", 
                    get_sub_mqtt_msg_ptr(mqtt_msg_ctrl)->topic_path,
                    current_sub_msg_state);

        switch(current_sub_msg_state)
        {
            case MSG_STATE_SUBSCRIBING:
                log_debug_m(log_mqtt_msg_ctrl, 
                            "mqtt msg %s still subcribing..., waiting", 
                            get_sub_mqtt_msg_ptr(mqtt_msg_ctrl)->topic_path);
                break;

            case MSG_STATE_SUBSCRIBED:                
                mqtt_msg_ctrl->sub_msg_id++;
                if(is_all_mqtt_msg_subscribed(mqtt_msg_ctrl) == true)
                {   
                    log_info_m(log_mqtt_msg_ctrl, 
                               "All messages are subscribed!");
                    break;
                }
                log_debug_m(log_mqtt_msg_ctrl, 
                            "Mqtt msg subscribed, increment sub_msg_id");
                break;

            case MSG_STATE_UNSUBSCRIBED:
                log_debug_m(log_mqtt_msg_ctrl, "Invoke subscribe mqtt_msg [%s]", 
                            get_sub_mqtt_msg_ptr(mqtt_msg_ctrl)->topic_path);

                err_t res = mqtt_msg_subscribe(get_sub_mqtt_msg_ptr(mqtt_msg_ctrl));

                ASSERT_LOG_DEBUG_M(res == ERR_OK, log_mqtt_msg_ctrl, 
                                    "mqtt msg %s subscribe invoke succes", 
                                    get_sub_mqtt_msg_ptr(mqtt_msg_ctrl)->topic_path);

                ASSERT_LOG_ERROR_M(res != ERR_OK, log_mqtt_msg_ctrl, 
                                "mqtt msg %s subscribe invoke, err %d", 
                                get_sub_mqtt_msg_ptr(mqtt_msg_ctrl)->topic_path, res);
                break;

            default:
                log_error_m(log_mqtt_msg_ctrl, 
                            "Incorrect switch-case state msg %d",
                            current_sub_msg_state);
                break;
        }
	}
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_msg_ctrl_is_all_msgs_subscribed(mqtt_msg_ctrl_t *mqtt_msg_ctrl)
{
    return mqtt_msg_ctrl->sub_msg_id >= mqtt_msg_ctrl->msg_array_size;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_msg_ctrl_topic_handler(mqtt_msg_ctrl_t *mqtt_msg_ctrl, 
                                 const char *topic_path, uint32_t topic_len)
{
    LOG_DEBUG_FUNC();
    if(is_mqtt_msg_ctrl_initialize(mqtt_msg_ctrl) == false)
    {
        return false;
    }

    log_debug_m(log_mqtt_msg_ctrl, 
               "Start searching mqtt_msg [%s]", topic_path);

    for(int msg_id = 0; msg_id < mqtt_msg_ctrl->msg_array_size; msg_id++)
    {
        if(is_mqtt_msg_incoming(mqtt_msg_ctrl->msg_array[msg_id], 
                                topic_path) == true)
        {
            log_debug_m(log_mqtt_msg_ctrl, "Incoming msg [%s] found at %d ID!", 
                        mqtt_msg_ctrl->msg_array[msg_id]->topic_path, msg_id);

            mqtt_msg_ctrl->order_msg_id = msg_id;
            return true;
        }
    }    
    mqtt_msg_ctrl->order_msg_id = ORDER_MSG_NONE;
    log_info_m(log_mqtt_msg_ctrl, "Incoming msg [ %s ] not found!", 
                topic_path);
    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_msg_ctrl_payload_handler(mqtt_msg_ctrl_t *mqtt_msg_ctrl, 
                                   const char *payload, uint16_t payload_len)
{
    LOG_DEBUG_FUNC();
    if(is_mqtt_msg_ctrl_initialize(mqtt_msg_ctrl) == false)
    {
        return false;
    }

    if(is_correct_order_msg_id(mqtt_msg_ctrl, payload, payload_len) == false)
    {
        return false;
    }

    mqtt_msg_t *order_msg = get_order_mqtt_msg_ptr(mqtt_msg_ctrl);

    if(is_mqtt_msg_init(order_msg) == true)
    {
        log_debug_m(log_mqtt_msg_ctrl, "Order mqtt msg [%s] invoke!", 
                    order_msg->topic_path);
        mqtt_msg_set_incoming_payload(order_msg, payload, payload_len);

        log_info_m(log_mqtt_msg_ctrl, 
                    "Incoming msg [ %s { %s } ( %d ) ] found!",
                    order_msg->topic_path, order_msg->payload, 
                    order_msg->order_id);

        order_msg->sub_data_incoming_cb(order_msg);
        mqtt_msg_ctrl->order_msg_id = ORDER_MSG_NONE;
        
        return true;
    }
    else
    {
        log_error_m(log_mqtt_msg_ctrl, 
                    "Order mqtt msg pointer is not init correctly!");
        return false;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_msg_ctrl_set_all_msg_unsubscribed(mqtt_msg_ctrl_t *mqtt_msg_ctrl)
{
    if(is_mqtt_msg_ctrl_initialize(mqtt_msg_ctrl) == false)
    {
        return false;
    }

    if(mqtt_msg_ctrl->msg_array_size > 0)
    {
        for(int id = 0; id < mqtt_msg_ctrl->msg_array_size; id++)
        {
            mqtt_msg_ctrl->msg_array[id]->state = MSG_STATE_UNSUBSCRIBED;
        }
        mqtt_msg_ctrl->sub_msg_id = 0;
        log_info_m(log_mqtt_msg_ctrl, "Unsubscribed all messages!");
        return true;
    }
    return false;    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parametes(mqtt_msg_ctrl_t *mqtt_msg_ctrl,
                        mqtt_msg_t **mqtt_msg_array, uint16_t msg_array_size)
{
    return mqtt_msg_ctrl != NULL && mqtt_msg_array != NULL 
            && msg_array_size > 0;
}                        

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_mqtt_msg_ctrl_initialize(mqtt_msg_ctrl_t *mqtt_msg_ctrl)
{
    bool retval = mqtt_msg_ctrl != NULL && mqtt_msg_ctrl->is_init == true;

    ASSERT_LOG_DEBUG_M(retval == false, 
                    log_mqtt_msg_ctrl, "mqtt_msg_ctrl is not initialized!");
    ASSERT_LOG_DEBUG_M(retval == true,
                        log_mqtt_msg_ctrl, "mqtt_msg_ctrl is initialized!");

    return retval;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_mqtt_msg_incoming(mqtt_msg_t *mqtt_msg, 
                                const char *incoming_topic)
{
    return (strcmp(mqtt_msg->topic_path, incoming_topic) == 0);
}                                

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static mqtt_msg_t *get_order_mqtt_msg_ptr(mqtt_msg_ctrl_t *mqtt_msg_ctrl)
{
    if(mqtt_msg_ctrl->msg_array != NULL && mqtt_msg_ctrl->msg_array_size > 0)
    {
        return mqtt_msg_ctrl->msg_array[mqtt_msg_ctrl->order_msg_id];
    }
    else
    {
        return NULL;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_order_msg_id(mqtt_msg_ctrl_t *mqtt_msg_ctrl, 
                                    const char *payload, uint16_t payload_len)
{
    return mqtt_msg_ctrl->order_msg_id != ORDER_MSG_NONE 
            && mqtt_msg_ctrl->order_msg_id < mqtt_msg_ctrl->msg_array_size
            && payload != NULL && payload_len > 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_mqtt_msg_init(mqtt_msg_t *mqtt_msg)
{
    return mqtt_msg != NULL && mqtt_msg->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static msg_state_t get_sub_msg_state(mqtt_msg_ctrl_t *mqtt_msg_ctrl)
{
    return mqtt_msg_ctrl->msg_array[mqtt_msg_ctrl->sub_msg_id]->state;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static mqtt_msg_t * get_sub_mqtt_msg_ptr(mqtt_msg_ctrl_t *mqtt_msg_ctrl)
{
    return mqtt_msg_ctrl->msg_array[mqtt_msg_ctrl->sub_msg_id];
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_all_mqtt_msg_subscribed(mqtt_msg_ctrl_t *mqtt_msg_ctrl)
{
    return mqtt_msg_ctrl->sub_msg_id >= mqtt_msg_ctrl->msg_array_size;
}
