#include "communication/protocols/mqtt/inc/mqtt_msg.h"

#include "debug/log_modules/log_modules.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parametes(mqtt_msg_t *mqtt_msg, 
                                      mqtt_client_t *client, qos_enum_t qos, 
                                      char *topic_path,
                                      sub_data_incoming clbk);

static bool is_correct_qos_value(qos_enum_t qos);

static bool is_mqtt_msg_initialize(mqtt_msg_t *mqtt_msg);

static bool is_possible_publish_data(mqtt_msg_t *mqtt_msg, char *payload);

static bool is_possible_receive_data(mqtt_msg_t *mqtt_msg,
									  const char *payload,
                                      uint16_t payload_len);

static void mqtt_pub_request_cb(void *arg, err_t result);

static void mqtt_sub_request_cb(void *arg, err_t result);

static void mqtt_unsub_request_cb(void *arg, err_t result);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_msg_t *mqtt_msg_create(void)
{
	log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    return (mqtt_msg_t*)malloc(sizeof(mqtt_msg_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_msg_init(mqtt_msg_t *mqtt_msg, mqtt_client_t *client, qos_enum_t qos, 
                   char *topic_path, sub_data_incoming sub_data_incoming_cb)
{
	log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    if(is_correct_init_parametes(mqtt_msg, client, qos, topic_path,
                                 sub_data_incoming_cb) == false)
    {
        ASSERT_LOG_ERROR_M(topic_path == NULL, log_mqtt_msg, 
                            "Initialize parameters error!");

        ASSERT_LOG_ERROR_M(topic_path != NULL, log_mqtt_msg, 
                            "Initialize parameters error for %s msg!", 
                            topic_path);                            
        return false;
    }
    
    mqtt_msg->order_id = 0;
    mqtt_msg->mqtt_client_ptr = client;

    memset(mqtt_msg->payload, 0, MQTT_MSG_PAYLOAD_MAX_LEN);
    mqtt_msg->payload_len = 0;

    mqtt_msg->sub_data_incoming_cb = sub_data_incoming_cb;

    mqtt_msg->state = MSG_STATE_UNSUBSCRIBED;

    mqtt_msg->qos = qos;

    mqtt_msg->topic_path_len = strlen(topic_path);
    memset(mqtt_msg->topic_path, 0, MQTT_TOPIC_PATH_MAX_LEN);
    strncpy(mqtt_msg->topic_path, topic_path, mqtt_msg->topic_path_len);
    mqtt_msg->topic_path[mqtt_msg->topic_path_len] = '\0';

    mqtt_msg->is_init = true;

    log_info_m(log_mqtt_msg, "Correct init mqtt_msg [ %s ]", 
                mqtt_msg->topic_path);
                
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

err_t mqtt_msg_publish(mqtt_msg_t *mqtt_msg, char *payload)
{
	log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    if(is_possible_publish_data(mqtt_msg, payload) == false)
    {
        ASSERT_LOG_ERROR_M(payload == NULL, log_mqtt_msg, 
                            "Can't publish mqtt msg %s with NULL ptr payload!",
                            mqtt_msg->topic_path);
        ASSERT_LOG_ERROR_M(payload != NULL, log_mqtt_msg, 
                            "Can't publish mqtt msg %s with %s",
                            mqtt_msg->topic_path, payload);
        return ERR_ARG;
    }

    err_t err;
    uint16_t payload_len = strlen(payload);

    err = mqtt_publish(mqtt_msg->mqtt_client_ptr, mqtt_msg->topic_path,
                       payload, payload_len, 
                       mqtt_msg->qos, mqtt_msg->retain, 
                       mqtt_pub_request_cb, mqtt_msg);    
    
    if(err != ERR_OK)
    {
        log_warning_m(log_mqtt_msg, "mqtt msg [ %s ] on [ %s ] " 
                     "publishing error [ %d ]", 
                     payload, mqtt_msg->topic_path, err);

        mqtt_msg->state = MSG_STATE_PUBLISHING_ERR;
    }
    else
    {
        memset(mqtt_msg->payload, 0, MQTT_MSG_PAYLOAD_MAX_LEN);
        memcpy(mqtt_msg->payload, payload, payload_len);
        mqtt_msg->payload_len = payload_len;

        mqtt_msg->state = MSG_STATE_PUBLISHING;

        log_debug_m(log_mqtt_msg, "mqtt msg [ %s ] on [ %s ] start publishing",
                    payload, mqtt_msg->topic_path);
    }
    
    return err; 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

err_t mqtt_msg_subscribe(mqtt_msg_t *mqtt_msg)
{
	log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    if(is_mqtt_msg_initialize(mqtt_msg) == false)
    {
    	log_error_m(log_mqtt_msg, "mqtt_msg_subscribe not initialize");
        return ERR_ARG;
    }

    err_t err;

    err = mqtt_subscribe(mqtt_msg->mqtt_client_ptr, mqtt_msg->topic_path, 
                         mqtt_msg->qos, mqtt_sub_request_cb, 
                         mqtt_msg);
    
    mqtt_msg->state = err == ERR_OK ? 
                    MSG_STATE_SUBSCRIBING : MSG_STATE_UNSUBSCRIBED;

    ASSERT_LOG_WARNING_M(err != ERR_OK, log_mqtt_msg, 
                          "mqtt_msg_subscribe() %s retval is %d", 
                          mqtt_msg->topic_path, err);

    ASSERT_LOG_DEBUG_M(err == ERR_OK, log_mqtt_msg,
                    "mqtt_msg_subscribe() %s retval is %d",
                        mqtt_msg->topic_path, err);
    return err;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

err_t mqtt_msg_unsubscribe(mqtt_msg_t *mqtt_msg)
{
	log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    if(is_mqtt_msg_initialize(mqtt_msg) == false)
    {
        return ERR_ARG;
    }

    err_t err;

    err = mqtt_unsubscribe(mqtt_msg->mqtt_client_ptr, mqtt_msg->topic_path, 
                           mqtt_unsub_request_cb, mqtt_msg);

    mqtt_msg->state = err == ERR_OK ? 
                    MSG_STATE_UNSUBSCRIBING : MSG_STATE_SUBSCRIBED;

    ASSERT_LOG_WARNING_M(err != ERR_OK, log_mqtt_msg, 
                          "mqtt_msg_unsubscribe() %s retval is %d", 
                          mqtt_msg->topic_path, err);
    return err;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_msg_set_incoming_payload(mqtt_msg_t *mqtt_msg, const char *payload,
                                   uint16_t payload_len)
{
	log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    if(is_possible_receive_data(mqtt_msg, payload, payload_len) == true)
    {
    	memset(mqtt_msg->payload, 0, sizeof(mqtt_msg->payload));
        memcpy(mqtt_msg->payload, payload, payload_len);        
        mqtt_msg->payload_len = payload_len;
        mqtt_msg->order_id++;

        log_debug_m(log_mqtt_msg, 
                "Receive mqtt msg [ %s ] with payload [m = %s / len = %d]",
                mqtt_msg->topic_path, mqtt_msg->payload, 
                mqtt_msg->payload_len);
    }

    return true;
}        

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_msg_no_sub_cb(mqtt_msg_t *mm)
{
	return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parametes(mqtt_msg_t *mqtt_msg, 
                                      mqtt_client_t *client, qos_enum_t qos, 
                                      char *topic_path,
                                      sub_data_incoming clbk)
{
	log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    return mqtt_msg != NULL && mqtt_msg != NULL &&
            is_correct_qos_value(qos) == true && topic_path != NULL 
            && clbk != NULL;
}                                               
                                               
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_qos_value(qos_enum_t qos)
{
	log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    return qos >= QOS_0 && qos <= QOS_2;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_mqtt_msg_initialize(mqtt_msg_t *mqtt_msg)
{
	log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    return mqtt_msg != NULL && mqtt_msg->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_possible_publish_data(mqtt_msg_t *mqtt_msg, char *payload)                               
{
	log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    return is_mqtt_msg_initialize(mqtt_msg) == true && (payload != NULL); 
                        /*&& mqtt_msg->state != MSG_STATE_PUBLISHING;*/
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_possible_receive_data(mqtt_msg_t *mqtt_msg,
									  const char *payload,
                                      uint16_t payload_len)
{
	log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    return is_mqtt_msg_initialize(mqtt_msg) == true 
            && payload != NULL && payload_len > 0;
}                                    

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void mqtt_pub_request_cb(void *arg, err_t result)
{
	log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    if(arg == NULL)
    {
        log_warning_m(log_mqtt_msg, "mqtt_pub_request_cb null arg");
    }
    else
    {
        mqtt_msg_t *pub_mqtt_msg = (mqtt_msg_t*)arg;
        pub_mqtt_msg->state = result == ERR_OK ?
                        MSG_STATE_PUBLISHED : MSG_STATE_PUBLISHED_ERR;      
        
        ASSERT_LOG_INFO_M(result == ERR_OK, log_mqtt_msg, 
                        "Publish [ %s ] succes on [ %s ]", 
                        pub_mqtt_msg->payload, pub_mqtt_msg->topic_path);
        ASSERT_LOG_ERROR_M(result != ERR_OK, log_mqtt_msg, 
                       "Publish [ %s ] error on [ %s ] with err_code [ %d ]", 
                        pub_mqtt_msg->payload, pub_mqtt_msg->topic_path,
                        result);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void mqtt_sub_request_cb(void *arg, err_t result)
{
    log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    if(arg != NULL)
    {
        mqtt_msg_t *sub_mqtt_msg = (mqtt_msg_t*)arg;
        sub_mqtt_msg->state = result == ERR_OK ? 
                    MSG_STATE_SUBSCRIBED : MSG_STATE_UNSUBSCRIBED;
	    
        ASSERT_LOG_ERROR_M(result != ERR_OK, log_mqtt_msg, 
                        "Subscribe [ %s ] err, retval %d",
                        sub_mqtt_msg->topic_path,
                        result);
        ASSERT_LOG_INFO_M(result == ERR_OK, log_mqtt_msg, 
                        "Subscribe [ %s ] succes!", 
                        sub_mqtt_msg->topic_path);
    }
    else
    {
        log_error_m(log_mqtt_msg, "Sub clbk arg mqtt msg is null");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void mqtt_unsub_request_cb(void *arg, err_t result)
{
    log_debug_m(log_mqtt_msg, "%s : %s", __FILE__, __func__);
    if(arg != NULL)
    {
        mqtt_msg_t *sub_mqtt_msg = (mqtt_msg_t*)arg;
        sub_mqtt_msg->state = result == ERR_OK ? 
                    MSG_STATE_UNSUBSCRIBED : MSG_STATE_SUBSCRIBED;
	    
        ASSERT_LOG_ERROR_M(result != ERR_OK, log_mqtt_msg, 
                        "Unsubscribe [ %s ] err, retval: %d", 
                        sub_mqtt_msg->topic_path, result);                        
        ASSERT_LOG_INFO_M(result == ERR_OK, log_mqtt_msg, 
                        "Unsubscrie [ %s ] succes", 
                        sub_mqtt_msg->topic_path);                       
    }
    else
    {
        log_error_m(log_mqtt_msg, "Unsub clbk arg mm is null");
    }
}
