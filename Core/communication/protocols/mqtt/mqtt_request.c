#include "inc/mqtt_request.h"
#include "debug/log_modules/log_modules.h"

#include <string.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_mqtt_request, "%s : %s", __FILE__, \
                                      __func__))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_mqtt_request_initialize(mqtt_request_t *mqtt_request);

static bool is_correct_initialize_parameters(mqtt_request_t *mqtt_request, 
                                            char *name, 
                                            mqtt_client_t *client);

static bool mqtt_req_no_subdata_cb(mqtt_msg_t *msg);

static bool is_possible_send_request(mqtt_request_t *mqtt_request, 
                                     char *payload);

static void mqtt_request_publish_cb(void *arg, err_t result);

static bool is_empty_response_msg_payload(mqtt_request_t *mqtt_request);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

mqtt_request_t* mqtt_request_create()
{
    return (mqtt_request_t*)malloc(sizeof(mqtt_request_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_request_init(mqtt_request_t *mqtt_request, char *name, 
                        mqtt_client_t *client)
{
    LOG_DEBUG_FUNC();
    if(is_correct_initialize_parameters(mqtt_request, name, client) == false)
    {
        log_error_m(log_mqtt_request, "Initialize error!");
        return false;
    }

    // uint8_t name_len = strlen(name) > MQTT_REQ_NAME_MAX_LEN ? 
    //                     MQTT_REQ_NAME_MAX_LEN : strlen(name);
    strcpy(mqtt_request->name, name);

    mqtt_request->mqtt_client = client;    

    memset(&mqtt_request->req_msg, 0, sizeof(mqtt_msg_t));
    memset(&mqtt_request->resp_msg, 0, sizeof(mqtt_msg_t));

    mqtt_request->state = MQTT_REQ_IDLE;

    mqtt_request->is_init = true;

    log_info_m(log_mqtt_request, "Correct init mqtt_request [ %s ]",
                mqtt_request->name);
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_request_req_msg_init(mqtt_request_t *mqtt_request, char *topic_path)
{
    LOG_DEBUG_FUNC();
    if(is_mqtt_request_initialize(mqtt_request) == false)
    {
        log_error_m(log_mqtt_request, "Init req_msg error!");
        return false;
    }

    bool retval;
    retval = mqtt_msg_init(&mqtt_request->req_msg, mqtt_request->mqtt_client, 
                           QOS_2, topic_path, mqtt_req_no_subdata_cb);
    ASSERT_LOG_ERROR_M(retval != true, log_mqtt_request, 
                       "request msg [%s] initialize error",
                       topic_path);
    return retval;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_request_resp_msg_init(mqtt_request_t *mqtt_request, char *topic_path,
                                mqtt_request_response_cb resp_cb)
{
    LOG_DEBUG_FUNC();
    if(is_mqtt_request_initialize(mqtt_request) == false)
    {
        log_error_m(log_mqtt_request, "Init resp_msg error!");
        return false;
    }

    bool retval;
    retval = mqtt_msg_init(&mqtt_request->resp_msg, mqtt_request->mqtt_client, 
                           QOS_2, topic_path, resp_cb);
    ASSERT_LOG_ERROR_M(retval != true, log_mqtt_request, 
                       "request msg [%s] initialize error",
                       topic_path);
    return retval;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_request_send_req(mqtt_request_t *mqtt_request, char *payload)
{
    LOG_DEBUG_FUNC();
    if(is_possible_send_request(mqtt_request, payload) == false)
    {
        ASSERT_LOG_ERROR_M(payload == NULL, log_mqtt_request, 
                    "Can't send request mqtt [ %s ] with NULL ptr payload!",
                            mqtt_request->name);
        ASSERT_LOG_ERROR_M(payload != NULL, log_mqtt_request, 
                            "Can't send request mqtt [ %s ] with %s",
                            mqtt_request->name, payload);
        return ERR_ARG;
    }

    err_t err;
    uint16_t payload_len = strlen(payload);

    err = mqtt_publish(mqtt_request->mqtt_client, 
                       mqtt_request->req_msg.topic_path,payload, payload_len, 
                       mqtt_request->req_msg.qos, mqtt_request->req_msg.retain, 
                       mqtt_request_publish_cb, mqtt_request);    
    
    if(err != ERR_OK)
    {
        log_warning_m(log_mqtt_request, 
                    "mqtt request send with [ %s ] on [ %s ] err, retval: %d",
                      payload,
                      mqtt_request->req_msg.topic_path,
                      err);
    }
    else
    {
        memcpy(mqtt_request->req_msg.payload, payload, payload_len);
        mqtt_request->req_msg.payload_len = payload_len;

        memset(mqtt_request->resp_msg.payload, 0, MQTT_MSG_PAYLOAD_MAX_LEN);
        mqtt_request->resp_msg.payload_len = 0;

        log_debug_m(log_mqtt_request, 
                    "mqtt request send with [ %s ] on [ %s ] succes", 
                    payload, mqtt_request->req_msg.topic_path);
    }    
    return err; 
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void mqtt_request_reset(mqtt_request_t *mqtt_request)
{
    if(is_mqtt_request_initialize(mqtt_request) == false)
    {
        return ;
    }
    mqtt_request->state = MQTT_REQ_IDLE;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool mqtt_request_is_response_receive(mqtt_request_t *mqtt_request)
{
    if(is_mqtt_request_initialize(mqtt_request) == false)
    {
        return false;
    }

    return mqtt_request->state != MQTT_REQ_WAIT_FOR_RESP 
        && is_empty_response_msg_payload(mqtt_request) == false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_mqtt_request_initialize(mqtt_request_t *mqtt_request)
{
    LOG_DEBUG_FUNC();
    if(mqtt_request == NULL || mqtt_request->is_init == false)
    {
        log_error_m(log_mqtt_request, "mqtt_request object is not init");
        return false;
    }
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_initialize_parameters(mqtt_request_t *mqtt_request, 
                                            char *name, 
                                            mqtt_client_t *client)
{
    LOG_DEBUG_FUNC();
    if(mqtt_request == NULL)
    {
        log_error_m(log_mqtt_request, "mqtt_request pointer is NULL");
        return false;
    }
    if(client == NULL)
    {
        log_error_m(log_mqtt_request, 
                    "mqtt_request [ %s ] mqtt client ptr is NULL",
                    name);
        return false;
    }
    if(name == NULL)
    {
        log_error_m(log_mqtt_request, "mqtt_request name is NULL");
        return false;
    }
    if(strlen(name) > MQTT_REQ_NAME_MAX_LEN)
    {
        log_warning_m(log_mqtt_request, 
                      "mqtt_request name is longer than MAX_NAME_LEN");
    }    

    return true;
}                                            

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool mqtt_req_no_subdata_cb(mqtt_msg_t *msg)
{
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_possible_send_request(mqtt_request_t *mqtt_request, 
                                     char *payload)
{
    LOG_DEBUG_FUNC();
    if(mqtt_request == NULL || mqtt_request->is_init == false
            || mqtt_request->req_msg.is_init == false 
            || mqtt_request->resp_msg.is_init == false)
    {
        log_error_m(log_mqtt_request, 
                    "Not initialized mqtt_request correctly!");
        return false;
    }
    if(payload == NULL)
    {
        log_error_m(log_mqtt_request, 
                    "Empty mqtt_request [ %s ] payload request msg!",
                    mqtt_request->name);
        return false;
    }
    if(mqtt_request->resp_msg.state != MSG_STATE_SUBSCRIBED)
    {
        log_error_m(log_mqtt_request,
                    "mqtt_request [ %s ] resp msg [ %s ] not subscribed!",
                    mqtt_request->name, mqtt_request->resp_msg.topic_path);
        return false;
    }
    if(mqtt_request->state != MQTT_REQ_IDLE)
    {
        log_warning_m(log_mqtt_request, 
                "mqtt_request [ %s ] already invoke, waiting for response",
                mqtt_request->name);
        return false;
    }
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void mqtt_request_publish_cb(void *arg, err_t result)
{
    LOG_DEBUG_FUNC();
    if(arg == NULL)
    {
        log_warning_m(log_mqtt_request, "mqtt_request_publish_cb null arg");
    }
    else
    {
        mqtt_request_t *mqtt_request = (mqtt_request_t*)arg;
        if(mqtt_request == NULL)
        {
            log_warning_m(log_mqtt_request, 
                "mqtt_request_publish_cb cast mqtt_request from arg error");
        }
        else
        {
            if(result == ERR_OK)
            {
                mqtt_request->state = MQTT_REQ_WAIT_FOR_RESP;
                log_info_m(log_mqtt_request, 
                            "Publish req [ %s ] succes!", 
                            mqtt_request->name, result);
            }
            else
            {
                mqtt_request->state = MQTT_REQ_IDLE;
                log_warning_m(log_mqtt_request, 
                            "Publish req [ %s ] err, result: %d", 
                            mqtt_request->name, result);
            }
        }        
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_empty_response_msg_payload(mqtt_request_t *mqtt_request)
{
    return mqtt_request->resp_msg.payload_len == 0 && 
            strcmp(mqtt_request->resp_msg.payload, MQTT_REQ_NO_RESP) == 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|