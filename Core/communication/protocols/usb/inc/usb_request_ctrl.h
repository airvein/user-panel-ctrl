#ifndef __USB_REQUEST_CTRL_H
#define __USB_REQUEST_CTRL_H

#include "usb_requestt.h"
#include <stdbool.h>
#include <stdint.h>


typedef struct _usb_request_ctrl_s usb_request_ctrl_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct _usb_request_ctrl_s
{
    usb_request_t **requests_array;
    uint16_t requests_num;

    int16_t invoke_req_id;
    
    int16_t order_req_id;

    bool is_init;

};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

usb_request_ctrl_t *usb_request_ctrl_create(void);

bool usb_request_ctrl_init(usb_request_ctrl_t *usb_request_ctrl,
                           usb_request_t **usb_request_array,
                           uint16_t request_array_size);

bool usb_reqest_ctrl_handler(usb_request_ctrl_t *usb_request_ctrl,
                             const char *response, uint16_t response_len);

bool is_request_invoke(usb_request_ctrl_t *usb_request_ctrl);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



#endif /* __USB_REQUEST_CTRL_H */
