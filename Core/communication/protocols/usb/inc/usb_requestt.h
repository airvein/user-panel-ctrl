#ifndef __USB_REQUESTT_H
#define __USB_REQUESTT_H

#include "usb_msg.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define USB_REQ_QUESTION_MAX_LEN    200

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _usb_request_ usb_request_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _usb_request_state_e
{
    USB_REQ_IDLE,
    USB_REQ_WAIT_FOR_RESP,

} usb_request_state_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef enum _usb_response_state_e
{
    USB_NO_RESP,
    USB_Y_RESP,
    USB_N_RESP,

} usb_response_state_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct _usb_request_
{
    char message[USB_REQ_QUESTION_MAX_LEN];
    uint16_t message_len;

    usb_response_state_t response;

    usb_request_state_t state;

    bool is_init;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

usb_request_t* usb_request_create();

bool usb_request_init(usb_request_t *usb_request, char *message);

bool usb_request_send_req(usb_request_t *usb_request);

bool usb_request_is_response_recived(usb_request_t *usb_request);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#endif /* __USB_REQUESTT_H */
