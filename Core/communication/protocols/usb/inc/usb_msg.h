#ifndef __USB_MSG_H
#define __USB_MSG_H

#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define USB_MSG_PAYLOAD_MAX_LEN     128

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _usb_msg_s usb_msg_t;
typedef bool (*data_incoming)(usb_msg_t*);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

struct _usb_msg_s
{
    char *command;
    uint32_t command_len;

    char payload[USB_MSG_PAYLOAD_MAX_LEN];
    uint32_t payload_len;

    uint32_t order_id;

    data_incoming data_incoming_cb;

    bool is_init;
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

usb_msg_t *usb_msg_create(void);

bool usb_msg_init(usb_msg_t *usb_msg, char *command,
                  data_incoming data_incoming_cb);

bool usb_msg_set_incoming_payload(usb_msg_t *usb_msg, const char *payload,
                                  uint16_t payload_len);

void set_usb_plug_status_on();

void set_usb_plug_status_off();

bool is_usb_plug();



#endif /* __USB_MSG_H */
