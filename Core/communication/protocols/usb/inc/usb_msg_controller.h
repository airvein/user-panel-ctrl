#ifndef __USB_MSG_CONTROLLER_H
#define __USB_MSG_CONTROLLER_H

#include "usb_msg.h"


#define MESSAGE_MAX_LEN 256

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                             T Y P E D E F S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

typedef struct _usb_msg_ctrl_s
{

    usb_msg_t **msg_array;
    uint16_t msg_array_size;

    uint16_t msg_id;

    int16_t order_msg_id;

    bool is_init;

} usb_msg_ctrl_t;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

usb_msg_ctrl_t *usb_msg_ctrl_create(void);

bool usb_msg_ctrl_init(usb_msg_ctrl_t *usb_msg_ctrl,
                       usb_msg_t **msg_array, uint16_t msg_array_size);

void usb_msg_ctrl_message_handler(usb_msg_ctrl_t *usb_msg_ctrl,
                                  const char *message, uint32_t message_len);


#endif /* __USB_MSG_CONTROLLER_H */
