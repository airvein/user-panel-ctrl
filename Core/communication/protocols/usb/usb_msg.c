#include "inc/usb_msg.h"
#include "debug/log_modules/log_modules.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_usb_msg, "%s : %s", __FILE__, \
                                    __func__))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool usb_plug = false;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(usb_msg_t *usb_msg, char *command,
                                data_incoming data_incoming_cb);

static bool is_possible_recive_data(usb_msg_t *usb_msg, const char *payload,
                             uint16_t payload_len);

static bool is_usb_msg_initialize(usb_msg_t *usb_msg);

static bool usb_msg_clear_payload(usb_msg_t *usb_msg);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

usb_msg_t *usb_msg_create(void)
{
    LOG_DEBUG_FUNC();
    return (usb_msg_t*)malloc(sizeof(usb_msg_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool usb_msg_init(usb_msg_t *usb_msg, char *command,
                  data_incoming data_incoming_cb)
{
    LOG_DEBUG_FUNC();
    
    if(is_correct_init_parameters(usb_msg, command, data_incoming_cb) == false)
    {
        ASSERT_LOG_ERROR_M(command == NULL, log_usb_msg,
                           "Initialize parameters error");
        
        ASSERT_LOG_ERROR_M(command != NULL, log_usb_msg,
                           "Initialize parameters error for %s msg!", command);

        return false;
    }

    usb_msg->order_id = 0;
    
    memset(usb_msg->payload, 0, USB_MSG_PAYLOAD_MAX_LEN);
    usb_msg->payload_len = 0;
    usb_msg->data_incoming_cb = data_incoming_cb;
    usb_msg->command_len = strlen(command);
    usb_msg->command = malloc(sizeof(char) * (usb_msg->command_len) + 1);
    strncpy(usb_msg->command, command, usb_msg->command_len);
    usb_msg->command[usb_msg->command_len] = '\0';
    usb_msg->is_init = true;

    log_info_m(log_usb_msg, "Correct init usb_msg structure %s",
                usb_msg->command);
    
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool usb_msg_set_incoming_payload(usb_msg_t *usb_msg, const char *payload,
                                  uint16_t payload_len)
{
    LOG_DEBUG_FUNC();
    if(is_possible_recive_data(usb_msg, payload, payload_len) == true)
    {
    	usb_msg_clear_payload(usb_msg);
        memcpy(usb_msg->payload, payload, payload_len);
        usb_msg->payload_len = payload_len;
        usb_msg->order_id++;

        log_info_m(log_usb_msg, "Receive usb msg %s [m = %s / len = %d]",
                    usb_msg->command, usb_msg->payload, usb_msg->payload_len);
    }
    return true;
}

void set_usb_plug_status_on()
{
    usb_plug = true;
}

void set_usb_plug_status_off()
{
    usb_plug = false;
}

bool is_usb_plug()
{
    return usb_plug;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(usb_msg_t *usb_msg, char *command,
                                data_incoming data_incoming_cb)
{
    LOG_DEBUG_FUNC();
    return usb_msg != NULL && command != NULL && data_incoming_cb != NULL;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_possible_recive_data(usb_msg_t *usb_msg, const char *payload,
                             uint16_t payload_len)
{
	LOG_DEBUG_FUNC();
    return is_usb_msg_initialize(usb_msg) == true 
            && payload != NULL && payload_len > 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_usb_msg_initialize(usb_msg_t *usb_msg)
{
	LOG_DEBUG_FUNC();
    return usb_msg != NULL && usb_msg->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool usb_msg_clear_payload(usb_msg_t *usb_msg)
{
	memset(usb_msg->payload, 0, sizeof(usb_msg->payload));
	return 1;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
