#include "inc/usb_requestt.h"
#include "debug/log_modules/log_modules.h"

#include <string.h>
#include <stdlib.h>

//#include "usbd_cdc_if.h"



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_usb_request, "%s : %s", __FILE__, \
                                      __func__))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_usb_request_initialize(usb_request_t *usb_request);

static bool is_correct_initialize_parameters(usb_request_t *usb_request,
                                             char *message);

static bool is_empty_response_msg_payload(usb_request_t *usb_request);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

usb_request_t* usb_request_create()
{
    return (usb_request_t*)malloc(sizeof(usb_request_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool usb_request_init(usb_request_t *usb_request, char *message)
{
    LOG_DEBUG_FUNC();
    if(is_correct_initialize_parameters(usb_request, message) == false)
    {
        log_error_m(log_usb_request, "Inirialize error!");
        return false;
    }

    strcpy(usb_request->message, message);

    usb_request->message_len = strlen(message);

    usb_request->response = USB_NO_RESP;

    usb_request->state = USB_REQ_IDLE;

    usb_request->is_init = true;

    log_info_m(log_usb_request, "Usb request initialized!");

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool usb_request_send_req(usb_request_t *usb_request)
{
    LOG_DEBUG_FUNC();
    if(is_usb_request_initialize(usb_request) == false)
    {
        log_error_m(log_usb_request, "Can't send service request [ %s ]");
        return false;
    }

    uint8_t err;

//    err = CDC_Transmit_FS(usb_request->message, usb_request->message_len);
    usb_request->response = USB_NO_RESP;

//    if(err != 1 /*USBD_OK*/)
    {
        usb_request->state = USB_REQ_IDLE;
        log_warning_m(log_usb_request,
                      "usb_request_send_req() return %d", err);
        return false;
    }

    usb_request->state = USB_REQ_WAIT_FOR_RESP;
    log_info_m(log_usb_request, "usb_request send request: %s",
               usb_request->message);

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool usb_request_is_response_recived(usb_request_t *usb_request)
{
    if(is_usb_request_initialize(usb_request) == false)
    {
        return false;
    }

    if (usb_request->state != USB_REQ_WAIT_FOR_RESP
           && is_empty_response_msg_payload(usb_request) == false)
    {
        return true;
    }

    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_usb_request_initialize(usb_request_t *usb_request)
{
    LOG_DEBUG_FUNC();
    if(usb_request == NULL || usb_request->is_init == false)
    {
        log_error_m(log_usb_request, "usb_request object is not init!");
        return false;
    }
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_initialize_parameters(usb_request_t *usb_request,
                                             char *message)
{
    LOG_DEBUG_FUNC();
    if(usb_request == NULL)
    {
        log_error_m(log_usb_request, "usb_request pointer  is NULL");
        return false;
    }
    
    if(message == NULL)
    {
        log_error_m(log_usb_request, "mqtt_request name is NULL");
        return false;
    }
    if(strlen(message) > USB_REQ_QUESTION_MAX_LEN)
    {
        log_warning_m(log_usb_request, 
                      "usb_request question is longer than MAX_QUESTION_LEN");
        return false;
    }    

    return true;
}                                            

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_empty_response_msg_payload(usb_request_t *usb_request)
{
    return usb_request->message_len == 0 && 
           strcmp(usb_request->message, "") == 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
