#include "inc/usb_request_ctrl.h"

#include "debug/log_modules/log_modules.h"

//#include "usbd_cdc_if.h"

#include <string.h>



// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define LOG_DEBUG_FUNC() (log_debug_m(log_usb_request_ctrl, "%s : %s", \
                                    __FILE__, __func__))

#define ORDER_REQ_NONE  (int16_t)-1     

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(usb_request_ctrl_t *usb_request_ctrl,
                           usb_request_t **usb_request_array, 
                           uint16_t request_array_size);

static bool is_usb_request_ctrl_initialize(
                                        usb_request_ctrl_t *usb_request_ctrl);

static bool is_correct_order_req_id(usb_request_ctrl_t *usb_request_ctrl,
                                const char *response, uint16_t response_len);

static usb_request_t *get_order_usb_request(
                                        usb_request_ctrl_t *usb_request_ctrl);

static usb_response_state_t usb_msg_check_icoming_payload(const char *response,
														uint16_t response_len);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

usb_request_ctrl_t *usb_request_ctrl_create(void)
{
    return (usb_request_ctrl_t*)malloc(sizeof(usb_request_ctrl_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool usb_request_ctrl_init(usb_request_ctrl_t *usb_request_ctrl,
                           usb_request_t **usb_request_array,
                           uint16_t request_array_size)
{
    LOG_DEBUG_FUNC();
    if(is_correct_init_parameters(usb_request_ctrl, usb_request_array,
                                  request_array_size) == false)
    {
        log_error_m(log_usb_request_ctrl, "Incorrect init parameters!");
        return false;
    }

    usb_request_ctrl->invoke_req_id = 0;
    usb_request_ctrl->requests_array = usb_request_array;
    usb_request_ctrl->requests_num = request_array_size;

    usb_request_ctrl->order_req_id = ORDER_REQ_NONE;

    usb_request_ctrl->is_init = true;

    log_debug_m(log_usb_request_ctrl, "Init usb request controller correctly!");

    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool usb_reqest_ctrl_handler(usb_request_ctrl_t *usb_request_ctrl,
                             const char *response, uint16_t response_len)
{
    LOG_DEBUG_FUNC();
    if(is_usb_request_ctrl_initialize(usb_request_ctrl) == false)
    {
        log_warning_m(log_usb_msg_ctrl, "usb req no init!");
        return false;
    }

    if(get_order_usb_request(usb_request_ctrl)->state == USB_REQ_WAIT_FOR_RESP)
    {
        log_debug_m(log_usb_msg_ctrl, "Order usb req invoke!");

        usb_response_state_t resp;
        resp = usb_msg_check_icoming_payload(response, response_len);

        if(resp == USB_NO_RESP)
        {
            log_warning_m(log_usb_msg_ctrl,
                          "Wrong response! [expected y or n]");
            
            //TODO dopisac wyswietlenie bledu na usb

            get_order_usb_request(usb_request_ctrl)->response = USB_N_RESP;
        }
        if(resp == USB_Y_RESP)
        {
            log_debug_m(log_usb_msg_ctrl,
                          "resp from usb [ y ]");
            get_order_usb_request(usb_request_ctrl)->response = USB_Y_RESP;
        }
        if(resp == USB_N_RESP)
        {
            log_warning_m(log_usb_msg_ctrl,
                          "resp from usb [ n ]");
            get_order_usb_request(usb_request_ctrl)->response = USB_N_RESP;
        }

        get_order_usb_request(usb_request_ctrl)->state = USB_REQ_IDLE;
        usb_request_ctrl->order_req_id = ORDER_REQ_NONE;
        return true;

    }
    else
    {
        log_warning_m(log_usb_msg_ctrl,
                      "usb req not expected, skipped!");
        return false;
    }
}

bool is_request_invoke(usb_request_ctrl_t *usb_request_ctrl)
{
    if(get_order_usb_request(usb_request_ctrl)->state == USB_REQ_WAIT_FOR_RESP)
    {
        log_debug_g("REQ!");
        return true;
    }
    log_debug_g("NO REQ!");
    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(usb_request_ctrl_t *usb_request_ctrl,
                           usb_request_t **usb_request_array, 
                           uint16_t request_array_size)
{
    return usb_request_ctrl != NULL && usb_request_array != NULL
           && request_array_size > 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_usb_request_ctrl_initialize(
                                        usb_request_ctrl_t *usb_request_ctrl)
{
    return usb_request_ctrl != NULL && usb_request_ctrl->is_init;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_order_req_id(usb_request_ctrl_t *usb_request_ctrl,
                                const char *response, uint16_t response_len)
{
    if( usb_request_ctrl->order_req_id != ORDER_REQ_NONE
           && usb_request_ctrl->order_req_id < usb_request_ctrl->requests_num
           && response != NULL && response_len > 0
    )
    {
        return true;
    }
    return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static usb_request_t *get_order_usb_request(
                                        usb_request_ctrl_t *usb_request_ctrl)
{
    return usb_request_ctrl->requests_array[usb_request_ctrl->order_req_id];
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static usb_response_state_t usb_msg_check_icoming_payload(const char *response,
														uint16_t response_len)
{
	if(strcmp(response , "y") == 0 && response_len == 1)
    {
        return USB_Y_RESP;
    }
    else if (strcmp(response , "n") == 0 && response_len == 1)
    {
        return USB_N_RESP;
    }
    else
    {
        return USB_NO_RESP;
    }   
}
