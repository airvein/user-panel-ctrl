#include "inc/usb_msg_controller.h"
#include "debug/log_modules/log_modules.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    D E F I N E S   /   M A C R O S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

#define ORDER_MSG_NONE  (int16_t)-1

#define MESSAGE_MAX_LEN 256
#define COMMAND_MAX_LEN 128

#define LOG_DEBUG_FUNC() (log_debug_m(log_usb_msg_ctrl, "%s : %s", __FILE__, \
                                    __func__))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         E X T E R N   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                         S T A T I C   V A R I A B L E S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|


char inc_command[COMMAND_MAX_LEN];
uint16_t inc_command_len = 0;
char payload[COMMAND_MAX_LEN];
uint16_t payload_len = 0;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E C L A R A T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(usb_msg_ctrl_t *usb_msg_ctrl,
                                usb_msg_t **msg_array,
                                uint16_t msg_array_size);

static bool is_usb_msg_ctrl_initialize(usb_msg_ctrl_t *usb_msg_ctrl);

static bool is_correct_order_msg_id(usb_msg_ctrl_t *usb_msg_ctrl,
                                    char *command, uint16_t command_len);

static bool is_usb_msg_incoming(usb_msg_t *usb_msg, 
                                const char *incoming_topic);

static bool serch_command_on_msg_array(usb_msg_ctrl_t *usb_msg_ctrl,
                                       char *command);

static bool is_usb_msg_init(usb_msg_t *usb_msg);

static usb_msg_t *get_order_usb_msg_ptr(usb_msg_ctrl_t *usb_msg_ctrl);

static void clear_char(char *msg);

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//                    F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

usb_msg_ctrl_t *usb_msg_ctrl_create(void)
{
    LOG_DEBUG_FUNC();
    return (usb_msg_ctrl_t*)malloc(sizeof(usb_msg_ctrl_t));
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

bool usb_msg_ctrl_init(usb_msg_ctrl_t *usb_msg_ctrl,
                       usb_msg_t **msg_array, uint16_t msg_array_size)
{
    LOG_DEBUG_FUNC();
    if(is_correct_init_parameters(usb_msg_ctrl, msg_array,
                                  msg_array_size == false))
    {
        log_error_m(log_usb_msg_ctrl, "Incorrect init parameter");
        return false;
    }
    usb_msg_ctrl->msg_id = 0;
    usb_msg_ctrl->order_msg_id = ORDER_MSG_NONE;

    usb_msg_ctrl->msg_array = msg_array;
    usb_msg_ctrl->msg_array_size = msg_array_size;

    usb_msg_ctrl->is_init = true;
    log_info_m(log_usb_msg_ctrl,"Init usb msg controller correctly!");
    return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

void usb_msg_ctrl_message_handler(usb_msg_ctrl_t *usb_msg_ctrl,
                                  const char *message, uint32_t message_len)
{
    LOG_DEBUG_FUNC();
    log_info_m(log_usb_msg_ctrl, "message: [%s] | with len: [%d]", message, message_len);

    if(is_usb_msg_ctrl_initialize(usb_msg_ctrl) == false)
    {
        return;
    }

    clear_char(inc_command);
    clear_char(payload);

    log_info_m(log_usb_msg_ctrl, "Extracting command from message...");
    uint16_t i = 0;
    uint16_t o = 0;
    while(message[i] != ' ' && i < MESSAGE_MAX_LEN)
    {
        inc_command[i] = message[i];
        i++;
    }
    i++;
    inc_command_len = strlen(inc_command);
    log_info_m(log_usb_msg_ctrl,
                "Extracted command [%s] from message with lenght [%d]",
                 inc_command, inc_command_len);
    log_debug_m(log_usb_msg_ctrl, "Extracting payload from message...");
    
    while(message[i] != '\0' && i < MESSAGE_MAX_LEN)
    {
        payload[o] = message[i];
        i++;
        o++;
    }
    payload_len = strlen(payload);
    log_info_m(log_usb_msg_ctrl, 
                "Extracted payload [%s] from message with lenght [%d]",
                payload, payload_len);

    log_debug_m(log_usb_msg_ctrl,
                "Start serching command [%s] on msg array", inc_command);

    if(serch_command_on_msg_array(usb_msg_ctrl, inc_command) == 0)
    {
        return;
    }
    
    if(is_correct_order_msg_id(usb_msg_ctrl, inc_command, inc_command_len)
                               == false)
    {
        log_warning_m(log_usb_msg_ctrl, "Incorrect command id: %d!",
                      usb_msg_ctrl->order_msg_id);
        return;                  
    }

    usb_msg_t *order_msg = get_order_usb_msg_ptr(usb_msg_ctrl);

    if(is_usb_msg_init(order_msg) == true)
    {
        log_debug_m(log_usb_msg_ctrl, "Order usb command [%s] invoke!",
                    order_msg->command);
        usb_msg_set_incoming_payload(order_msg, payload, payload_len);
        order_msg->data_incoming_cb(order_msg);
    }
    else
    {
        log_error_m(log_usb_msg_ctrl,
                    "Order usb msg pointer is not init correctly!");
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
//             S T A T I C   F U N C T I O N   D E F I N I T I O N S
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_init_parameters(usb_msg_ctrl_t *usb_msg_ctrl,
                                usb_msg_t **msg_array, uint16_t msg_array_size)
{
    return usb_msg_ctrl != NULL && msg_array != NULL && msg_array_size > 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_usb_msg_ctrl_initialize(usb_msg_ctrl_t *usb_msg_ctrl)
{
    bool retval = usb_msg_ctrl != NULL && usb_msg_ctrl->is_init == true;

    ASSERT_LOG_ERROR_M(retval == false, log_usb_msg_ctrl,
                       "usb_msg_ctrl is not initialized!");
    ASSERT_LOG_DEBUG_M(retval == true, log_usb_msg,
                       "usb_msg_ctrl is initialized!");

    return retval;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_correct_order_msg_id(usb_msg_ctrl_t *usb_msg_ctrl,
                                    char *command, uint16_t command_len)
{
    return usb_msg_ctrl->order_msg_id != ORDER_MSG_NONE
           && usb_msg_ctrl->order_msg_id < usb_msg_ctrl->msg_array_size
           && command != NULL && command_len > 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_usb_msg_incoming(usb_msg_t *usb_msg, 
                                const char *incoming_topic)
{
    return (strcmp(usb_msg->command, incoming_topic) == 0);
}        

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool serch_command_on_msg_array(usb_msg_ctrl_t *usb_msg_ctrl,
                                       char *command)
{

    for(int msg_id = 0; msg_id < usb_msg_ctrl->msg_array_size; msg_id++)
    {
        if(is_usb_msg_incoming(usb_msg_ctrl->msg_array[msg_id], 
                                inc_command) == true)
        {
            log_info_m(log_usb_msg_ctrl, "Incoming command [%s] found at %d ID!", 
                        usb_msg_ctrl->msg_array[msg_id]->command, msg_id);

            usb_msg_ctrl->order_msg_id = msg_id;
            return 1;
        }
    }
    log_warning_m(log_usb_msg_ctrl, "Incoming command [%s] not found!",
                                 inc_command);
    usb_msg_ctrl->order_msg_id = ORDER_MSG_NONE;
    return 0;    
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static bool is_usb_msg_init(usb_msg_t *usb_msg)
{
    return usb_msg != NULL && usb_msg->is_init == true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static usb_msg_t *get_order_usb_msg_ptr(usb_msg_ctrl_t *usb_msg_ctrl)
{
    if(usb_msg_ctrl->msg_array != NULL && usb_msg_ctrl->msg_array_size > 0)
    {
        return usb_msg_ctrl->msg_array[usb_msg_ctrl->order_msg_id];
    }
    else
    {
        return NULL;
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

static void clear_char(char *msg)
{
    memset(msg, 0, COMMAND_MAX_LEN);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
