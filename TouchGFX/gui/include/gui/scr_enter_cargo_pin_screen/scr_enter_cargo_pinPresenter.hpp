#ifndef SCR_ENTER_CARGO_PINPRESENTER_HPP
#define SCR_ENTER_CARGO_PINPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_enter_cargo_pinView;

class scr_enter_cargo_pinPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_enter_cargo_pinPresenter(scr_enter_cargo_pinView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_enter_cargo_pinPresenter() {};

    void setPinInputCharNumbers(int pin_num);

private:
    scr_enter_cargo_pinPresenter();

    scr_enter_cargo_pinView& view;
};

#endif // SCR_ENTER_CARGO_PINPRESENTER_HPP
