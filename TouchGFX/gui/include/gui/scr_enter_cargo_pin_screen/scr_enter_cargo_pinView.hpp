#ifndef SCR_ENTER_CARGO_PINVIEW_HPP
#define SCR_ENTER_CARGO_PINVIEW_HPP

#include <gui_generated/scr_enter_cargo_pin_screen/scr_enter_cargo_pinViewBase.hpp>
#include <gui/scr_enter_cargo_pin_screen/scr_enter_cargo_pinPresenter.hpp>

class scr_enter_cargo_pinView : public scr_enter_cargo_pinViewBase
{
public:
    scr_enter_cargo_pinView();
    virtual ~scr_enter_cargo_pinView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();

    void updateEnterPin(uint8_t pin_cnt);

protected:
};

#endif // SCR_ENTER_CARGO_PINVIEW_HPP
