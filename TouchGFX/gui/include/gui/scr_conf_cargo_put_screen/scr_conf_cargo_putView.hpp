#ifndef SCR_CONF_CARGO_PUTVIEW_HPP
#define SCR_CONF_CARGO_PUTVIEW_HPP

#include <gui_generated/scr_conf_cargo_put_screen/scr_conf_cargo_putViewBase.hpp>
#include <gui/scr_conf_cargo_put_screen/scr_conf_cargo_putPresenter.hpp>

class scr_conf_cargo_putView : public scr_conf_cargo_putViewBase
{
public:
    scr_conf_cargo_putView();
    virtual ~scr_conf_cargo_putView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_CONF_CARGO_PUTVIEW_HPP
