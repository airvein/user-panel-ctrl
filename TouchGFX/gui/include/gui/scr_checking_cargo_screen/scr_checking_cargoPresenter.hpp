#ifndef SCR_CHECKING_CARGOPRESENTER_HPP
#define SCR_CHECKING_CARGOPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_checking_cargoView;

class scr_checking_cargoPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_checking_cargoPresenter(scr_checking_cargoView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_checking_cargoPresenter() {};

private:
    scr_checking_cargoPresenter();

    scr_checking_cargoView& view;
};

#endif // SCR_CHECKING_CARGOPRESENTER_HPP
