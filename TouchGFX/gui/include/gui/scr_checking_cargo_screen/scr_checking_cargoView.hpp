#ifndef SCR_CHECKING_CARGOVIEW_HPP
#define SCR_CHECKING_CARGOVIEW_HPP

#include <gui_generated/scr_checking_cargo_screen/scr_checking_cargoViewBase.hpp>
#include <gui/scr_checking_cargo_screen/scr_checking_cargoPresenter.hpp>

class scr_checking_cargoView : public scr_checking_cargoViewBase
{
public:
    scr_checking_cargoView();
    virtual ~scr_checking_cargoView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_CHECKING_CARGOVIEW_HPP
