#ifndef SCR_WINDOW_CLOSINGVIEW_HPP
#define SCR_WINDOW_CLOSINGVIEW_HPP

#include <gui_generated/scr_window_closing_screen/scr_window_closingViewBase.hpp>
#include <gui/scr_window_closing_screen/scr_window_closingPresenter.hpp>

class scr_window_closingView : public scr_window_closingViewBase
{
public:
    scr_window_closingView();
    virtual ~scr_window_closingView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_WINDOW_CLOSINGVIEW_HPP
