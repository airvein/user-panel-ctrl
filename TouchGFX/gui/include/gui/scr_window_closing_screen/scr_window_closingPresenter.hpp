#ifndef SCR_WINDOW_CLOSINGPRESENTER_HPP
#define SCR_WINDOW_CLOSINGPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_window_closingView;

class scr_window_closingPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_window_closingPresenter(scr_window_closingView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_window_closingPresenter() {};

private:
    scr_window_closingPresenter();

    scr_window_closingView& view;
};

#endif // SCR_WINDOW_CLOSINGPRESENTER_HPP
