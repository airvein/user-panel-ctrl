#ifndef SCR_CARGO_PIN_INCORRECTVIEW_HPP
#define SCR_CARGO_PIN_INCORRECTVIEW_HPP

#include <gui_generated/scr_cargo_pin_incorrect_screen/scr_cargo_pin_incorrectViewBase.hpp>
#include <gui/scr_cargo_pin_incorrect_screen/scr_cargo_pin_incorrectPresenter.hpp>

class scr_cargo_pin_incorrectView : public scr_cargo_pin_incorrectViewBase
{
public:
    scr_cargo_pin_incorrectView();
    virtual ~scr_cargo_pin_incorrectView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_CARGO_PIN_INCORRECTVIEW_HPP
