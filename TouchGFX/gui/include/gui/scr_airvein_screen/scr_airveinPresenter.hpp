#ifndef SCR_AIRVEINPRESENTER_HPP
#define SCR_AIRVEINPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_airveinView;

class scr_airveinPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_airveinPresenter(scr_airveinView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_airveinPresenter() {};

private:
    scr_airveinPresenter();

    scr_airveinView& view;
};

#endif // SCR_AIRVEINPRESENTER_HPP
