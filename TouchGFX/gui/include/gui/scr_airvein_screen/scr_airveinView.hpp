#ifndef SCR_AIRVEINVIEW_HPP
#define SCR_AIRVEINVIEW_HPP

#include <gui_generated/scr_airvein_screen/scr_airveinViewBase.hpp>
#include <gui/scr_airvein_screen/scr_airveinPresenter.hpp>

class scr_airveinView : public scr_airveinViewBase
{
public:
    scr_airveinView();
    virtual ~scr_airveinView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_AIRVEINVIEW_HPP
