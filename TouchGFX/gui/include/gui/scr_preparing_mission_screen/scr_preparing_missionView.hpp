#ifndef SCR_PREPARING_MISSIONVIEW_HPP
#define SCR_PREPARING_MISSIONVIEW_HPP

#include <gui_generated/scr_preparing_mission_screen/scr_preparing_missionViewBase.hpp>
#include <gui/scr_preparing_mission_screen/scr_preparing_missionPresenter.hpp>

class scr_preparing_missionView : public scr_preparing_missionViewBase
{
public:
    scr_preparing_missionView();
    virtual ~scr_preparing_missionView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_PREPARING_MISSIONVIEW_HPP
