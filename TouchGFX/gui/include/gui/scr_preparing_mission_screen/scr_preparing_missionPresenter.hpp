#ifndef SCR_PREPARING_MISSIONPRESENTER_HPP
#define SCR_PREPARING_MISSIONPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_preparing_missionView;

class scr_preparing_missionPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_preparing_missionPresenter(scr_preparing_missionView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_preparing_missionPresenter() {};

private:
    scr_preparing_missionPresenter();

    scr_preparing_missionView& view;
};

#endif // SCR_PREPARING_MISSIONPRESENTER_HPP
