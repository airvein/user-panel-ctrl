#ifndef SCR_CARGO_PIN_CORRECTVIEW_HPP
#define SCR_CARGO_PIN_CORRECTVIEW_HPP

#include <gui_generated/scr_cargo_pin_correct_screen/scr_cargo_pin_correctViewBase.hpp>
#include <gui/scr_cargo_pin_correct_screen/scr_cargo_pin_correctPresenter.hpp>

class scr_cargo_pin_correctView : public scr_cargo_pin_correctViewBase
{
public:
    scr_cargo_pin_correctView();
    virtual ~scr_cargo_pin_correctView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_CARGO_PIN_CORRECTVIEW_HPP
