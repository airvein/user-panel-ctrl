#ifndef SCR_CARGO_TAKE_OUTVIEW_HPP
#define SCR_CARGO_TAKE_OUTVIEW_HPP

#include <gui_generated/scr_cargo_take_out_screen/scr_cargo_take_outViewBase.hpp>
#include <gui/scr_cargo_take_out_screen/scr_cargo_take_outPresenter.hpp>

class scr_cargo_take_outView : public scr_cargo_take_outViewBase
{
public:
    scr_cargo_take_outView();
    virtual ~scr_cargo_take_outView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_CARGO_TAKE_OUTVIEW_HPP
