#ifndef SCR_CARGO_TAKE_OUTPRESENTER_HPP
#define SCR_CARGO_TAKE_OUTPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_cargo_take_outView;

class scr_cargo_take_outPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_cargo_take_outPresenter(scr_cargo_take_outView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_cargo_take_outPresenter() {};

private:
    scr_cargo_take_outPresenter();

    scr_cargo_take_outView& view;
};

#endif // SCR_CARGO_TAKE_OUTPRESENTER_HPP
