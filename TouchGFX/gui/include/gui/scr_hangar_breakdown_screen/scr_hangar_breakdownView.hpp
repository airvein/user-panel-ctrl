#ifndef SCR_HANGAR_BREAKDOWNVIEW_HPP
#define SCR_HANGAR_BREAKDOWNVIEW_HPP

#include <gui_generated/scr_hangar_breakdown_screen/scr_hangar_breakdownViewBase.hpp>
#include <gui/scr_hangar_breakdown_screen/scr_hangar_breakdownPresenter.hpp>

class scr_hangar_breakdownView : public scr_hangar_breakdownViewBase
{
public:
    scr_hangar_breakdownView();
    virtual ~scr_hangar_breakdownView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_HANGAR_BREAKDOWNVIEW_HPP
