#ifndef SCR_HANGAR_BREAKDOWNPRESENTER_HPP
#define SCR_HANGAR_BREAKDOWNPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_hangar_breakdownView;

class scr_hangar_breakdownPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_hangar_breakdownPresenter(scr_hangar_breakdownView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_hangar_breakdownPresenter() {};

private:
    scr_hangar_breakdownPresenter();

    scr_hangar_breakdownView& view;
};

#endif // SCR_HANGAR_BREAKDOWNPRESENTER_HPP
