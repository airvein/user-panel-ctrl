#ifndef SCR_CARGO_INCORRECTVIEW_HPP
#define SCR_CARGO_INCORRECTVIEW_HPP

#include <gui_generated/scr_cargo_incorrect_screen/scr_cargo_incorrectViewBase.hpp>
#include <gui/scr_cargo_incorrect_screen/scr_cargo_incorrectPresenter.hpp>

class scr_cargo_incorrectView : public scr_cargo_incorrectViewBase
{
public:
    scr_cargo_incorrectView();
    virtual ~scr_cargo_incorrectView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_CARGO_INCORRECTVIEW_HPP
