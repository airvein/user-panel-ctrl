#ifndef SCR_WINDOW_OPENINGVIEW_HPP
#define SCR_WINDOW_OPENINGVIEW_HPP

#include <gui_generated/scr_window_opening_screen/scr_window_openingViewBase.hpp>
#include <gui/scr_window_opening_screen/scr_window_openingPresenter.hpp>

class scr_window_openingView : public scr_window_openingViewBase
{
public:
    scr_window_openingView();
    virtual ~scr_window_openingView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_WINDOW_OPENINGVIEW_HPP
