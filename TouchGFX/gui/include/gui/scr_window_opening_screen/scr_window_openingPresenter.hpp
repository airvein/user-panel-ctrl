#ifndef SCR_WINDOW_OPENINGPRESENTER_HPP
#define SCR_WINDOW_OPENINGPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_window_openingView;

class scr_window_openingPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_window_openingPresenter(scr_window_openingView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_window_openingPresenter() {};

private:
    scr_window_openingPresenter();

    scr_window_openingView& view;
};

#endif // SCR_WINDOW_OPENINGPRESENTER_HPP
