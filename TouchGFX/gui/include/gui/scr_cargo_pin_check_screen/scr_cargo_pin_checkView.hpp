#ifndef SCR_CARGO_PIN_CHECKVIEW_HPP
#define SCR_CARGO_PIN_CHECKVIEW_HPP

#include <gui_generated/scr_cargo_pin_check_screen/scr_cargo_pin_checkViewBase.hpp>
#include <gui/scr_cargo_pin_check_screen/scr_cargo_pin_checkPresenter.hpp>

class scr_cargo_pin_checkView : public scr_cargo_pin_checkViewBase
{
public:
    scr_cargo_pin_checkView();
    virtual ~scr_cargo_pin_checkView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_CARGO_PIN_CHECKVIEW_HPP
