#ifndef SCR_CARGO_PIN_CHECKPRESENTER_HPP
#define SCR_CARGO_PIN_CHECKPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_cargo_pin_checkView;

class scr_cargo_pin_checkPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_cargo_pin_checkPresenter(scr_cargo_pin_checkView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_cargo_pin_checkPresenter() {};

private:
    scr_cargo_pin_checkPresenter();

    scr_cargo_pin_checkView& view;
};

#endif // SCR_CARGO_PIN_CHECKPRESENTER_HPP
