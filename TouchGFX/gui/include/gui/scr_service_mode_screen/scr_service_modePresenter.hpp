#ifndef SCR_SERVICE_MODEPRESENTER_HPP
#define SCR_SERVICE_MODEPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_service_modeView;

class scr_service_modePresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_service_modePresenter(scr_service_modeView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_service_modePresenter() {};

private:
    scr_service_modePresenter();

    scr_service_modeView& view;
};

#endif // SCR_SERVICE_MODEPRESENTER_HPP
