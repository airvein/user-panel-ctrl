#ifndef SCR_SERVICE_MODEVIEW_HPP
#define SCR_SERVICE_MODEVIEW_HPP

#include <gui_generated/scr_service_mode_screen/scr_service_modeViewBase.hpp>
#include <gui/scr_service_mode_screen/scr_service_modePresenter.hpp>

class scr_service_modeView : public scr_service_modeViewBase
{
public:
    scr_service_modeView();
    virtual ~scr_service_modeView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_SERVICE_MODEVIEW_HPP
