#ifndef SCR_CARGO_PIN_TOUTVIEW_HPP
#define SCR_CARGO_PIN_TOUTVIEW_HPP

#include <gui_generated/scr_cargo_pin_tout_screen/scr_cargo_pin_toutViewBase.hpp>
#include <gui/scr_cargo_pin_tout_screen/scr_cargo_pin_toutPresenter.hpp>

class scr_cargo_pin_toutView : public scr_cargo_pin_toutViewBase
{
public:
    scr_cargo_pin_toutView();
    virtual ~scr_cargo_pin_toutView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_CARGO_PIN_TOUTVIEW_HPP
