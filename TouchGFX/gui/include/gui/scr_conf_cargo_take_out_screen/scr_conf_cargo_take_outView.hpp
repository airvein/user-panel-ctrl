#ifndef SCR_CONF_CARGO_TAKE_OUTVIEW_HPP
#define SCR_CONF_CARGO_TAKE_OUTVIEW_HPP

#include <gui_generated/scr_conf_cargo_take_out_screen/scr_conf_cargo_take_outViewBase.hpp>
#include <gui/scr_conf_cargo_take_out_screen/scr_conf_cargo_take_outPresenter.hpp>

class scr_conf_cargo_take_outView : public scr_conf_cargo_take_outViewBase
{
public:
    scr_conf_cargo_take_outView();
    virtual ~scr_conf_cargo_take_outView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_CONF_CARGO_TAKE_OUTVIEW_HPP
