#ifndef SCR_CARGO_CORRECTPRESENTER_HPP
#define SCR_CARGO_CORRECTPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_cargo_correctView;

class scr_cargo_correctPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_cargo_correctPresenter(scr_cargo_correctView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_cargo_correctPresenter() {};

private:
    scr_cargo_correctPresenter();

    scr_cargo_correctView& view;
};

#endif // SCR_CARGO_CORRECTPRESENTER_HPP
