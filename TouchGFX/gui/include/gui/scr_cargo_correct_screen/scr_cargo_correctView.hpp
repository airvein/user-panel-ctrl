#ifndef SCR_CARGO_CORRECTVIEW_HPP
#define SCR_CARGO_CORRECTVIEW_HPP

#include <gui_generated/scr_cargo_correct_screen/scr_cargo_correctViewBase.hpp>
#include <gui/scr_cargo_correct_screen/scr_cargo_correctPresenter.hpp>

class scr_cargo_correctView : public scr_cargo_correctViewBase
{
public:
    scr_cargo_correctView();
    virtual ~scr_cargo_correctView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_CARGO_CORRECTVIEW_HPP
