#ifndef SCR_THANK_YOUPRESENTER_HPP
#define SCR_THANK_YOUPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_thank_youView;

class scr_thank_youPresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_thank_youPresenter(scr_thank_youView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_thank_youPresenter() {};

private:
    scr_thank_youPresenter();

    scr_thank_youView& view;
};

#endif // SCR_THANK_YOUPRESENTER_HPP
