#ifndef SCR_THANK_YOUVIEW_HPP
#define SCR_THANK_YOUVIEW_HPP

#include <gui_generated/scr_thank_you_screen/scr_thank_youViewBase.hpp>
#include <gui/scr_thank_you_screen/scr_thank_youPresenter.hpp>

class scr_thank_youView : public scr_thank_youViewBase
{
public:
    scr_thank_youView();
    virtual ~scr_thank_youView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_THANK_YOUVIEW_HPP
