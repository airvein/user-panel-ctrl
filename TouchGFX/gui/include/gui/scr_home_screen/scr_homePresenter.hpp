#ifndef SCR_HOMEPRESENTER_HPP
#define SCR_HOMEPRESENTER_HPP

#include <gui/model/ModelListener.hpp>
#include <mvp/Presenter.hpp>

using namespace touchgfx;

class scr_homeView;

class scr_homePresenter : public touchgfx::Presenter, public ModelListener
{
public:
    scr_homePresenter(scr_homeView& v);

    /**
     * The activate function is called automatically when this screen is "switched in"
     * (ie. made active). Initialization logic can be placed here.
     */
    virtual void activate();

    /**
     * The deactivate function is called automatically when this screen is "switched out"
     * (ie. made inactive). Teardown functionality can be placed here.
     */
    virtual void deactivate();

    virtual ~scr_homePresenter() {};

private:
    scr_homePresenter();

    scr_homeView& view;
};

#endif // SCR_HOMEPRESENTER_HPP
