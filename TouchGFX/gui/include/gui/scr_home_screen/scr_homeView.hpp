#ifndef SCR_HOMEVIEW_HPP
#define SCR_HOMEVIEW_HPP

#include <gui_generated/scr_home_screen/scr_homeViewBase.hpp>
#include <gui/scr_home_screen/scr_homePresenter.hpp>

class scr_homeView : public scr_homeViewBase
{
public:
    scr_homeView();
    virtual ~scr_homeView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
protected:
};

#endif // SCR_HOMEVIEW_HPP
