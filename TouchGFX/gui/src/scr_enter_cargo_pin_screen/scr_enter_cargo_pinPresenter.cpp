#include <gui/scr_enter_cargo_pin_screen/scr_enter_cargo_pinView.hpp>
#include <gui/scr_enter_cargo_pin_screen/scr_enter_cargo_pinPresenter.hpp>

scr_enter_cargo_pinPresenter::scr_enter_cargo_pinPresenter(scr_enter_cargo_pinView& v)
    : view(v)
{

}

void scr_enter_cargo_pinPresenter::activate()
{

}

void scr_enter_cargo_pinPresenter::deactivate()
{

}

void scr_enter_cargo_pinPresenter::setPinInputCharNumbers(int pin_num)
{
    view.updateEnterPin(pin_num);
}