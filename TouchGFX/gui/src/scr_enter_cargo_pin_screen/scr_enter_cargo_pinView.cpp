#include <gui/scr_enter_cargo_pin_screen/scr_enter_cargo_pinView.hpp>
#include "BitmapDatabase.hpp"
#include <texts/TextKeysAndLanguages.hpp>

scr_enter_cargo_pinView::scr_enter_cargo_pinView()
{

}

void scr_enter_cargo_pinView::setupScreen()
{
    scr_enter_cargo_pinViewBase::setupScreen();
}

void scr_enter_cargo_pinView::tearDownScreen()
{
    scr_enter_cargo_pinViewBase::tearDownScreen();
}

void scr_enter_cargo_pinView::updateEnterPin(uint8_t pin_cnt)
{
    switch(pin_cnt)
    {
        case 0:
            Unicode::snprintf(auth_pinBuffer, 12, "%s", touchgfx::TypedText(T_RSRC_PIN0).getText());
            break;
        case 1:
            Unicode::snprintf(auth_pinBuffer, 12, "%s", touchgfx::TypedText(T_RSRC_PIN1).getText());
            break;
        case 2:
            Unicode::snprintf(auth_pinBuffer, 12, "%s", touchgfx::TypedText(T_RSRC_PIN2).getText());
            break;
        case 3:
            Unicode::snprintf(auth_pinBuffer, 12, "%s", touchgfx::TypedText(T_RSRC_PIN3).getText());
            break;
        case 4:
            Unicode::snprintf(auth_pinBuffer, 12, "%s", touchgfx::TypedText(T_RSRC_PIN4).getText());
            break;
        case 5:
            Unicode::snprintf(auth_pinBuffer, 12, "%s", touchgfx::TypedText(T_RSRC_PIN5).getText());
            break;
        case 6:
            Unicode::snprintf(auth_pinBuffer, 12, "%s", touchgfx::TypedText(T_RSRC_PIN6).getText());
            break;
        default:            
            break;

    }
    auth_pin.invalidate();
}
