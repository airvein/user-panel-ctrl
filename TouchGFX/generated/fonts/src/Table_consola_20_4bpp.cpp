// Autogenerated, do not edit

#include <fonts/GeneratedFont.hpp>

FONT_TABLE_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::GlyphNode glyphs_consola_20_4bpp[] FONT_TABLE_LOCATION_FLASH_ATTRIBUTE =
{
    {     0, 0x0020,   0,   0,   0,   0,  11,   0,   0, 0x00 },
    {     0, 0x0023,  11,  13,  13,   0,  11,   0,   0, 0x00 },
    {    78, 0x002A,   9,   9,  14,   1,  11,   0,   0, 0x00 },
    {   123, 0x003F,   7,  14,  14,   2,  11,   0,   0, 0x00 },
    {   179, 0x005B,   6,  18,  14,   3,  11,   0,   0, 0x00 },
    {   233, 0x005D,   6,  18,  14,   2,  11,   0,   0, 0x00 },
    {   287, 0x0063,   9,  10,  10,   1,  11,   0,   0, 0x00 },
    {   337, 0x0066,  11,  14,  14,   0,  11,   0,   0, 0x00 },
    {   421, 0x0069,   9,  14,  14,   1,  11,   0,   0, 0x00 },
    {   491, 0x006A,   8,  18,  14,   1,  11,   0,   0, 0x00 },
    {   563, 0x006E,   9,  10,  10,   1,  11,   0,   0, 0x00 },
    {   613, 0x006F,  11,  10,  10,   0,  11,   0,   0, 0x00 },
    {   673, 0x0077,  11,  10,  10,   0,  11,   0,   0, 0x00 },
    {   733, 0x0079,  11,  14,  10,   0,  11,   0,   0, 0x00 },
    {   817, 0x007A,   9,  10,  10,   1,  11,   0,   0, 0x00 },
    {   867, 0x0107,   9,  14,  14,   1,  11,   0,   0, 0x00 },
    {   937, 0x015B,   9,  14,  14,   1,  11,   0,   0, 0x00 }
};

// consola_20_4bpp
extern const touchgfx::GlyphNode glyphs_consola_20_4bpp[];
extern const uint8_t unicodes_consola_20_4bpp_0[];
extern const uint8_t* const unicodes_consola_20_4bpp[] =
{
    unicodes_consola_20_4bpp_0
};
extern const touchgfx::KerningNode kerning_consola_20_4bpp[];
touchgfx::GeneratedFont& getFont_consola_20_4bpp();

touchgfx::GeneratedFont& getFont_consola_20_4bpp()
{
    static touchgfx::GeneratedFont consola_20_4bpp(glyphs_consola_20_4bpp, 17, 20, 4, 4, 1, 0, 0, unicodes_consola_20_4bpp, kerning_consola_20_4bpp, 63, 0, 0);
    return consola_20_4bpp;
}
