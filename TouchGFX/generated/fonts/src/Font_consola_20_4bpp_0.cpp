#include <touchgfx/hal/Types.hpp>

FONT_GLYPH_LOCATION_FLASH_PRAGMA
KEEP extern const uint8_t unicodes_consola_20_4bpp_0[] FONT_GLYPH_LOCATION_FLASH_ATTRIBUTE =
{
    // Unicode: [0x0020, ]
    // (Has no glyph data)
    // Unicode: [0x0023, ]
    0x00, 0x90, 0x0C, 0xA0, 0x0B, 0x00, 0x00, 0xB0, 0x0B, 0xC0, 0x0A, 0x00, 0x00, 0xD0, 0x09, 0xE0,
    0x08, 0x00, 0xF1, 0xFF, 0xFF, 0xFF, 0xFF, 0x09, 0x40, 0xF5, 0x48, 0xF5, 0x48, 0x02, 0x00, 0xF2,
    0x05, 0xF3, 0x03, 0x00, 0x00, 0xF3, 0x03, 0xF4, 0x02, 0x00, 0x00, 0xF5, 0x02, 0xF6, 0x01, 0x00,
    0xF9, 0xFF, 0xFF, 0xFF, 0xFF, 0x01, 0x32, 0xEA, 0x33, 0xDA, 0x33, 0x00, 0x00, 0xC9, 0x00, 0xBA,
    0x00, 0x00, 0x00, 0xBB, 0x00, 0xAC, 0x00, 0x00, 0x00, 0x9C, 0x00, 0x8D, 0x00, 0x00,
    // Unicode: [0x002A, ]
    0x00, 0x20, 0x2F, 0x00, 0x00, 0x40, 0x10, 0x1F, 0x40, 0x00, 0xF3, 0x2A, 0x2F, 0xFA, 0x03, 0x20,
    0xE9, 0xEF, 0x29, 0x00, 0x00, 0xD5, 0xDF, 0x05, 0x00, 0xC2, 0x5D, 0x5F, 0xCD, 0x02, 0x80, 0x11,
    0x1F, 0x81, 0x00, 0x00, 0x20, 0x2F, 0x00, 0x00, 0x00, 0x10, 0x14, 0x00, 0x00,
    // Unicode: [0x003F, ]
    0xF1, 0xAD, 0x04, 0x00, 0x90, 0xEA, 0x9F, 0x00, 0x00, 0x10, 0xFB, 0x07, 0x00, 0x00, 0xE1, 0x0D,
    0x00, 0x00, 0xD0, 0x0E, 0x00, 0x21, 0xF7, 0x0B, 0x10, 0xFF, 0xCF, 0x02, 0x00, 0xAF, 0x02, 0x00,
    0x00, 0x8F, 0x00, 0x00, 0x00, 0x7E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x47, 0x00, 0x00,
    0x70, 0xEF, 0x00, 0x00, 0x30, 0xAE, 0x00, 0x00,
    // Unicode: [0x005B, ]
    0xF9, 0xFF, 0x3F, 0xF9, 0x55, 0x15, 0xF9, 0x00, 0x00, 0xF9, 0x00, 0x00, 0xF9, 0x00, 0x00, 0xF9,
    0x00, 0x00, 0xF9, 0x00, 0x00, 0xF9, 0x00, 0x00, 0xF9, 0x00, 0x00, 0xF9, 0x00, 0x00, 0xF9, 0x00,
    0x00, 0xF9, 0x00, 0x00, 0xF9, 0x00, 0x00, 0xF9, 0x00, 0x00, 0xF9, 0x00, 0x00, 0xF9, 0x00, 0x00,
    0xF9, 0x66, 0x16, 0xF9, 0xFF, 0x3F,
    // Unicode: [0x005D, ]
    0xF3, 0xFF, 0x9F, 0x51, 0x65, 0x9F, 0x00, 0x00, 0x9F, 0x00, 0x00, 0x9F, 0x00, 0x00, 0x9F, 0x00,
    0x00, 0x9F, 0x00, 0x00, 0x9F, 0x00, 0x00, 0x9F, 0x00, 0x00, 0x9F, 0x00, 0x00, 0x9F, 0x00, 0x00,
    0x9F, 0x00, 0x00, 0x9F, 0x00, 0x00, 0x9F, 0x00, 0x00, 0x9F, 0x00, 0x00, 0x9F, 0x00, 0x00, 0x9F,
    0x61, 0x66, 0x9F, 0xF3, 0xFF, 0x9F,
    // Unicode: [0x0063, ]
    0x00, 0x92, 0xFD, 0xBE, 0x02, 0x30, 0xFE, 0x7A, 0xC8, 0x03, 0xC0, 0x4F, 0x00, 0x00, 0x00, 0xF3,
    0x0A, 0x00, 0x00, 0x00, 0xF6, 0x06, 0x00, 0x00, 0x00, 0xF6, 0x06, 0x00, 0x00, 0x00, 0xF5, 0x09,
    0x00, 0x00, 0x00, 0xE1, 0x3F, 0x00, 0x00, 0x00, 0x60, 0xFF, 0x8A, 0xD9, 0x03, 0x00, 0xB4, 0xFE,
    0xAE, 0x01,
    // Unicode: [0x0066, ]
    0x00, 0x00, 0x50, 0xEC, 0xDE, 0x03, 0x00, 0x00, 0xF6, 0x7D, 0x97, 0x03, 0x00, 0x00, 0xEC, 0x01,
    0x00, 0x00, 0x00, 0x00, 0xBF, 0x00, 0x00, 0x00, 0x00, 0x10, 0xAF, 0x00, 0x00, 0x00, 0x20, 0x22,
    0xBF, 0x22, 0x22, 0x00, 0xF3, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x41, 0x54, 0xCF, 0x44, 0x44, 0x00,
    0x00, 0x10, 0xAF, 0x00, 0x00, 0x00, 0x00, 0x10, 0xAF, 0x00, 0x00, 0x00, 0x00, 0x10, 0xAF, 0x00,
    0x00, 0x00, 0x00, 0x10, 0xAF, 0x00, 0x00, 0x00, 0x00, 0x10, 0xAF, 0x00, 0x00, 0x00, 0x00, 0x10,
    0xAF, 0x00, 0x00, 0x00,
    // Unicode: [0x0069, ]
    0x00, 0x70, 0x7E, 0x00, 0x00, 0x00, 0xC0, 0xCF, 0x00, 0x00, 0x00, 0x30, 0x3A, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xF0, 0xFF, 0x9F, 0x00, 0x00, 0x60, 0x76, 0x9F, 0x00, 0x00, 0x00, 0x10,
    0x9F, 0x00, 0x00, 0x00, 0x10, 0x9F, 0x00, 0x00, 0x00, 0x10, 0x9F, 0x00, 0x00, 0x00, 0x10, 0x9F,
    0x00, 0x00, 0x00, 0x10, 0x9F, 0x00, 0x00, 0x00, 0x10, 0x9F, 0x00, 0x00, 0x62, 0x76, 0xCF, 0x66,
    0x03, 0xF5, 0xFF, 0xFF, 0xFF, 0x08,
    // Unicode: [0x006A, ]
    0x00, 0x00, 0xB0, 0x3E, 0x00, 0x00, 0xF2, 0x7F, 0x00, 0x00, 0x70, 0x19, 0x00, 0x00, 0x00, 0x00,
    0xF5, 0xFF, 0xFF, 0x3F, 0x62, 0x66, 0xB6, 0x3F, 0x00, 0x00, 0x80, 0x3F, 0x00, 0x00, 0x80, 0x3F,
    0x00, 0x00, 0x80, 0x3F, 0x00, 0x00, 0x80, 0x3F, 0x00, 0x00, 0x80, 0x3F, 0x00, 0x00, 0x80, 0x3F,
    0x00, 0x00, 0x80, 0x3F, 0x00, 0x00, 0x80, 0x3F, 0x00, 0x00, 0x90, 0x2F, 0x00, 0x00, 0xE1, 0x0D,
    0xAA, 0x87, 0xFD, 0x05, 0xD7, 0xEE, 0x5C, 0x00,
    // Unicode: [0x006E, ]
    0xF7, 0x72, 0xED, 0x3C, 0x00, 0xF7, 0xDC, 0x66, 0xEE, 0x01, 0xF7, 0x2D, 0x00, 0xF6, 0x05, 0xF7,
    0x05, 0x00, 0xF4, 0x06, 0xF7, 0x04, 0x00, 0xF4, 0x07, 0xF7, 0x04, 0x00, 0xF4, 0x07, 0xF7, 0x04,
    0x00, 0xF4, 0x07, 0xF7, 0x04, 0x00, 0xF4, 0x07, 0xF7, 0x04, 0x00, 0xF4, 0x07, 0xF7, 0x04, 0x00,
    0xF4, 0x07,
    // Unicode: [0x006F, ]
    0x00, 0x81, 0xFD, 0xAE, 0x02, 0x00, 0x10, 0xFD, 0x7A, 0xFA, 0x2E, 0x00, 0x80, 0x6F, 0x00, 0x60,
    0xAF, 0x00, 0xD0, 0x0D, 0x00, 0x00, 0xED, 0x00, 0xF1, 0x0A, 0x00, 0x00, 0xFB, 0x01, 0xF1, 0x0A,
    0x00, 0x00, 0xFB, 0x01, 0xE0, 0x0D, 0x00, 0x00, 0xDD, 0x00, 0xA0, 0x5F, 0x00, 0x60, 0x8F, 0x00,
    0x20, 0xFE, 0x7A, 0xF9, 0x1C, 0x00, 0x00, 0xA2, 0xFE, 0x8D, 0x01, 0x00,
    // Unicode: [0x0077, ]
    0xF9, 0x01, 0x00, 0x00, 0xF1, 0x09, 0xF6, 0x03, 0x00, 0x00, 0xF3, 0x06, 0xF4, 0x05, 0x82, 0x01,
    0xF5, 0x04, 0xF2, 0x07, 0xF7, 0x06, 0xF6, 0x02, 0xF0, 0x08, 0xFC, 0x0C, 0xF8, 0x00, 0xD0, 0x2A,
    0x9F, 0x2F, 0xDA, 0x00, 0xB0, 0x6C, 0x1E, 0x7F, 0xBB, 0x00, 0x90, 0xBE, 0x09, 0xCA, 0x9D, 0x00,
    0x70, 0xFF, 0x04, 0xF5, 0x6F, 0x00, 0x40, 0xEF, 0x00, 0xE1, 0x4F, 0x00,
    // Unicode: [0x0079, ]
    0xF2, 0x0C, 0x00, 0x00, 0xFB, 0x02, 0xC0, 0x2F, 0x00, 0x20, 0xBF, 0x00, 0x60, 0x8F, 0x00, 0x70,
    0x5F, 0x00, 0x10, 0xDE, 0x00, 0xC0, 0x0E, 0x00, 0x00, 0xF9, 0x04, 0xF2, 0x09, 0x00, 0x00, 0xF3,
    0x0A, 0xF8, 0x03, 0x00, 0x00, 0xC0, 0x1F, 0xCD, 0x00, 0x00, 0x00, 0x60, 0x9F, 0x7F, 0x00, 0x00,
    0x00, 0x10, 0xFE, 0x1F, 0x00, 0x00, 0x00, 0x00, 0xF9, 0x0A, 0x00, 0x00, 0x00, 0x00, 0xFB, 0x03,
    0x00, 0x00, 0x00, 0x70, 0xAF, 0x00, 0x00, 0x00, 0x83, 0xFB, 0x1D, 0x00, 0x00, 0x00, 0xF6, 0x9D,
    0x01, 0x00, 0x00, 0x00,
    // Unicode: [0x007A, ]
    0xF4, 0xFF, 0xFF, 0xFF, 0x03, 0x62, 0x66, 0x76, 0xEF, 0x01, 0x00, 0x00, 0xA0, 0x5F, 0x00, 0x00,
    0x00, 0xF6, 0x08, 0x00, 0x00, 0x30, 0xCF, 0x00, 0x00, 0x00, 0xD1, 0x2E, 0x00, 0x00, 0x00, 0xFA,
    0x04, 0x00, 0x00, 0x60, 0x8F, 0x00, 0x00, 0x00, 0xF3, 0x7E, 0x77, 0x77, 0x03, 0xF6, 0xFF, 0xFF,
    0xFF, 0x08,
    // Unicode: [0x0107, ]
    0x00, 0x00, 0x40, 0xDF, 0x03, 0x00, 0x00, 0xE4, 0x1B, 0x00, 0x00, 0x00, 0x66, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x92, 0xFD, 0xBE, 0x02, 0x30, 0xFE, 0x7A, 0xC8, 0x03, 0xC0, 0x4F,
    0x00, 0x00, 0x00, 0xF3, 0x0A, 0x00, 0x00, 0x00, 0xF6, 0x06, 0x00, 0x00, 0x00, 0xF6, 0x06, 0x00,
    0x00, 0x00, 0xF5, 0x09, 0x00, 0x00, 0x00, 0xE1, 0x3F, 0x00, 0x00, 0x00, 0x60, 0xFF, 0x8A, 0xD9,
    0x03, 0x00, 0xB4, 0xFE, 0xAE, 0x01,
    // Unicode: [0x015B, ]
    0x00, 0x00, 0x90, 0xAF, 0x01, 0x00, 0x00, 0xF9, 0x07, 0x00, 0x00, 0x20, 0x47, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0xD7, 0xFF, 0x8D, 0x00, 0xA0, 0xAF, 0x77, 0x89, 0x00, 0xF1, 0x0B,
    0x00, 0x00, 0x00, 0xE0, 0x4E, 0x00, 0x00, 0x00, 0x40, 0xFE, 0x8D, 0x03, 0x00, 0x00, 0x71, 0xFC,
    0x8F, 0x00, 0x00, 0x00, 0x20, 0xFC, 0x03, 0x00, 0x00, 0x00, 0xF8, 0x04, 0xC3, 0x79, 0x97, 0xDF,
    0x00, 0xB2, 0xFE, 0xDF, 0x19, 0x00
};
