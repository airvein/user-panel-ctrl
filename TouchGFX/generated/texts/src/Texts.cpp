/* DO NOT EDIT THIS FILE */
/* This file is autogenerated by the text-database code generator */

#include <stdarg.h>
#include <touchgfx/Texts.hpp>
#include <touchgfx/hal/HAL.hpp>
#include <touchgfx/TypedText.hpp>
#include <texts/TypedTextDatabase.hpp>
#include <touchgfx/lcd/LCD.hpp>
#include <touchgfx/TextProvider.hpp>

touchgfx::Font::StringWidthFunctionPointer touchgfx::Font::getStringWidthFunction = &touchgfx::Font::getStringWidthLTR;
touchgfx::LCD::DrawStringFunctionPointer touchgfx::LCD::drawStringFunction = &touchgfx::LCD::drawStringLTR;
touchgfx::TextProvider::UnicodeConverterInitFunctionPointer touchgfx::TextProvider::unicodeConverterInitFunction = static_cast<touchgfx::TextProvider::UnicodeConverterInitFunctionPointer>(0);
touchgfx::TextProvider::UnicodeConverterFunctionPointer touchgfx::TextProvider::unicodeConverterFunction = static_cast<touchgfx::TextProvider::UnicodeConverterFunctionPointer>(0);

//Default typed text database
extern const touchgfx::TypedText::TypedTextData* const typedTextDatabaseArray[];

TEXT_LOCATION_FLASH_PRAGMA
KEEP extern const touchgfx::Unicode::UnicodeChar texts_all_languages[] TEXT_LOCATION_FLASH_ATTRIBUTE =
{
    0x21, 0x20, 0x55, 0x77, 0x61, 0x67, 0x61, 0x20, 0x21, 0xa, 0x4f, 0x74, 0x77, 0x69, 0x65, 0x72, 0x61, 0x6e, 0x69, 0x65, 0x20, 0x6f, 0x6b, 0x69, 0x65, 0x6e, 0x6b, 0x61, 0x2c, 0xa, 0x7a, 0x61, 0x63, 0x68, 0x6f, 0x77, 0x61, 0x6a, 0x20, 0x62, 0x65, 0x7a, 0x70, 0x69, 0x65, 0x63, 0x7a, 0x6e, 0x105, 0x20, 0xa, 0x6f, 0x64, 0x6c, 0x65, 0x67, 0x142, 0x6f, 0x15b, 0x107, 0x0, // @0 "! Uwaga !?Otwieranie okienka,?zachowaj bezpieczn? ?odleg?o??"
    0x21, 0x20, 0x55, 0x77, 0x61, 0x67, 0x61, 0x20, 0x21, 0xa, 0x5a, 0x61, 0x6d, 0x79, 0x6b, 0x61, 0x6e, 0x69, 0x65, 0x20, 0x6f, 0x6b, 0x69, 0x65, 0x6e, 0x6b, 0x61, 0x2c, 0xa, 0x7a, 0x61, 0x63, 0x68, 0x6f, 0x77, 0x61, 0x6a, 0x20, 0x62, 0x65, 0x7a, 0x70, 0x69, 0x65, 0x63, 0x7a, 0x6e, 0x105, 0x20, 0xa, 0x6f, 0x64, 0x6c, 0x65, 0x67, 0x142, 0x6f, 0x15b, 0x107, 0x0, // @61 "! Uwaga !?Zamykanie okienka,?zachowaj bezpieczn? ?odleg?o??"
    0x50, 0x6f, 0x74, 0x77, 0x69, 0x65, 0x72, 0x64, 0x17a, 0x20, 0x75, 0x6d, 0x69, 0x65, 0x73, 0x7a, 0x63, 0x7a, 0x65, 0x6e, 0x69, 0x65, 0xa, 0x70, 0x61, 0x63, 0x7a, 0x6b, 0x69, 0x2c, 0xa, 0x77, 0x63, 0x69, 0x15b, 0x6e, 0x69, 0x6a, 0x20, 0x5b, 0x20, 0x23, 0x20, 0x5d, 0x0, // @121 "Potwierd? umieszczenie?paczki,?wci?nij [ # ]"
    0x50, 0x61, 0x63, 0x7a, 0x6b, 0x61, 0x20, 0x7a, 0x62, 0x79, 0x74, 0x20, 0x63, 0x69, 0x119, 0x17c, 0x6b, 0x61, 0xa, 0x6c, 0x75, 0x62, 0xa, 0x6e, 0x69, 0x65, 0x70, 0x6f, 0x70, 0x72, 0x61, 0x77, 0x6e, 0x69, 0x65, 0x20, 0x77, 0x142, 0x6f, 0x17c, 0x6f, 0x6e, 0x61, 0x0, // @166 "Paczka zbyt ci??ka?lub?niepoprawnie w?o?ona"
    0x50, 0x72, 0x7a, 0x65, 0x6b, 0x72, 0x6f, 0x63, 0x7a, 0x6f, 0x6e, 0x6f, 0x20, 0x63, 0x7a, 0x61, 0x73, 0xa, 0x77, 0x70, 0x72, 0x6f, 0x77, 0x61, 0x64, 0x7a, 0x61, 0x6e, 0x69, 0x61, 0x20, 0x70, 0x69, 0x6e, 0x75, 0xa, 0x70, 0x61, 0x63, 0x7a, 0x6b, 0x69, 0x0, // @210 "Przekroczono czas?wprowadzania pinu?paczki"
    0x57, 0x70, 0x72, 0x6f, 0x77, 0x61, 0x64, 0x17a, 0x20, 0x70, 0x69, 0x6e, 0x20, 0x70, 0x61, 0x63, 0x7a, 0x6b, 0x69, 0xa, 0x64, 0x6f, 0x20, 0x77, 0x79, 0x73, 0x142, 0x61, 0x6e, 0x69, 0x61, 0x3a, 0xa, 0x2, 0x0, // @253 "Wprowad? pin paczki?do wys?ania:?<>"
    0x50, 0x6f, 0x74, 0x77, 0x69, 0x65, 0x72, 0x64, 0x17a, 0x20, 0x77, 0x79, 0x6a, 0x119, 0x63, 0x69, 0x65, 0xa, 0x70, 0x61, 0x63, 0x7a, 0x6b, 0x69, 0x2c, 0xa, 0x77, 0x63, 0x69, 0x15b, 0x6e, 0x69, 0x6a, 0x20, 0x5b, 0x20, 0x23, 0x20, 0x5d, 0x0, // @288 "Potwierd? wyj?cie?paczki,?wci?nij [ # ]"
    0x57, 0x63, 0x69, 0x15b, 0x6e, 0x69, 0x6a, 0x20, 0x5b, 0x20, 0x23, 0x20, 0x5d, 0x2c, 0x20, 0x61, 0x62, 0x79, 0xa, 0x72, 0x6f, 0x7a, 0x70, 0x6f, 0x63, 0x7a, 0x105, 0x107, 0x2e, 0x2e, 0x2e, 0x0, // @328 "Wci?nij [ # ], aby?rozpocz??..."
    0x53, 0x70, 0x72, 0x61, 0x77, 0x64, 0x7a, 0x61, 0x6e, 0x69, 0x65, 0x20, 0x70, 0x69, 0x6e, 0x75, 0x20, 0x20, 0xa, 0x70, 0x61, 0x63, 0x7a, 0x6b, 0x69, 0x2e, 0x2e, 0x2e, 0x0, // @360 "Sprawdzanie pinu  ?paczki..."
    0x44, 0x7a, 0x69, 0x119, 0x6b, 0x75, 0x6a, 0x119, 0x2c, 0xa, 0x70, 0x72, 0x6f, 0x63, 0x65, 0x73, 0x20, 0x7a, 0x61, 0x6b, 0x6f, 0x144, 0x63, 0x7a, 0x6f, 0x6e, 0x79, 0x0, // @389 "Dzi?kuj?,?proces zako?czony"
    0x50, 0x72, 0x7a, 0x79, 0x67, 0x6f, 0x74, 0x6f, 0x77, 0x79, 0x77, 0x61, 0x6e, 0x69, 0x65, 0x20, 0x6d, 0x69, 0x73, 0x6a, 0x69, 0x2e, 0x2e, 0x2e, 0x0, // @417 "Przygotowywanie misji..."
    0x4e, 0x69, 0x65, 0x70, 0x6f, 0x70, 0x72, 0x61, 0x77, 0x6e, 0x79, 0x20, 0x70, 0x69, 0x6e, 0x20, 0x70, 0x61, 0x63, 0x7a, 0x6b, 0x69, 0x0, // @442 "Niepoprawny pin paczki"
    0x53, 0x70, 0x72, 0x61, 0x77, 0x64, 0x7a, 0x61, 0x6e, 0x69, 0x65, 0x20, 0x70, 0x61, 0x63, 0x7a, 0x6b, 0x69, 0x2e, 0x2e, 0x2e, 0x0, // @465 "Sprawdzanie paczki..."
    0x57, 0x61, 0x67, 0x61, 0x20, 0x70, 0x61, 0x63, 0x7a, 0x6b, 0x69, 0x20, 0x70, 0x6f, 0x70, 0x72, 0x61, 0x77, 0x6e, 0x61, 0x0, // @487 "Waga paczki poprawna"
    0x50, 0x69, 0x6e, 0x20, 0x70, 0x61, 0x63, 0x7a, 0x6b, 0x69, 0x20, 0x70, 0x6f, 0x70, 0x72, 0x61, 0x77, 0x6e, 0x79, 0x0, // @508 "Pin paczki poprawny"
    0x48, 0x41, 0x4e, 0x47, 0x41, 0x52, 0x20, 0x55, 0x53, 0x5a, 0x4b, 0x4f, 0x44, 0x5a, 0x4f, 0x4e, 0x59, 0x0, // @528 "HANGAR USZKODZONY"
    0x50, 0x61, 0x63, 0x7a, 0x6b, 0x61, 0x20, 0x64, 0x6f, 0x20, 0x6f, 0x64, 0x62, 0x69, 0x6f, 0x72, 0x75, 0x0, // @546 "Paczka do odbioru"
    0x54, 0x52, 0x59, 0x42, 0x20, 0x53, 0x45, 0x52, 0x57, 0x49, 0x53, 0x4f, 0x57, 0x59, 0x0, // @564 "TRYB SERWISOWY"
    0x2a, 0x20, 0x2a, 0x20, 0x2a, 0x20, 0x2d, 0x20, 0x2d, 0x20, 0x2d, 0x0, // @579 "* * * - - -"
    0x2a, 0x20, 0x2a, 0x20, 0x2a, 0x20, 0x2a, 0x20, 0x2a, 0x20, 0x2d, 0x0, // @591 "* * * * * -"
    0x2a, 0x20, 0x2a, 0x20, 0x2a, 0x20, 0x2a, 0x20, 0x2a, 0x20, 0x2a, 0x0, // @603 "* * * * * *"
    0x2a, 0x20, 0x2d, 0x20, 0x2d, 0x20, 0x2d, 0x20, 0x2d, 0x20, 0x2d, 0x0, // @615 "* - - - - -"
    0x2a, 0x20, 0x2a, 0x20, 0x2d, 0x20, 0x2d, 0x20, 0x2d, 0x20, 0x2d, 0x0, // @627 "* * - - - -"
    0x77, 0x79, 0x63, 0x7a, 0x79, 0x15b, 0x107, 0x20, 0x5b, 0x23, 0x5d, 0x0, // @639 "wyczy?? [#]"
    0x2a, 0x20, 0x2a, 0x20, 0x2a, 0x20, 0x2a, 0x20, 0x2d, 0x20, 0x2d, 0x0, // @651 "* * * * - -"
    0x2d, 0x20, 0x2d, 0x20, 0x2d, 0x20, 0x2d, 0x20, 0x2d, 0x20, 0x2d, 0x0, // @663 "- - - - - -"
    0x5b, 0x2a, 0x5d, 0x20, 0x63, 0x6f, 0x66, 0x6e, 0x69, 0x6a, 0x0 // @675 "[*] cofnij"
};
extern uint32_t const indicesPl[];

//array holding dynamically installed languages
struct TranslationHeader
{
    uint32_t offset_to_texts;
    uint32_t offset_to_indices;
    uint32_t offset_to_typedtext;
};
static const TranslationHeader* languagesArray[1] = { 0 };

//Compiled and linked in languages
static const uint32_t* const staticLanguageIndices[] =
{
    indicesPl
};

touchgfx::LanguageId touchgfx::Texts::currentLanguage = static_cast<touchgfx::LanguageId>(0);
static const touchgfx::Unicode::UnicodeChar* currentLanguagePtr = 0;
static const uint32_t* currentLanguageIndices = 0;

void touchgfx::Texts::setLanguage(touchgfx::LanguageId id)
{
    const touchgfx::TypedText::TypedTextData* currentLanguageTypedText = 0;
    if (id < 1)
    {
        if (languagesArray[id] != 0)
        {
            //dynamic translation is added
            const TranslationHeader* translation = languagesArray[id];
            currentLanguagePtr = (const touchgfx::Unicode::UnicodeChar*)(((const uint8_t*)translation) + translation->offset_to_texts);
            currentLanguageIndices = (const uint32_t*)(((const uint8_t*)translation) + translation->offset_to_indices);
            currentLanguageTypedText = (const touchgfx::TypedText::TypedTextData*)(((const uint8_t*)translation) + translation->offset_to_typedtext);
        }
        else
        {
            //compiled and linked in languages
            currentLanguagePtr = texts_all_languages;
            currentLanguageIndices = staticLanguageIndices[id];
            currentLanguageTypedText = typedTextDatabaseArray[id];
        }
    }

    if (currentLanguageTypedText)
    {
        currentLanguage = id;
        touchgfx::TypedText::registerTypedTextDatabase(currentLanguageTypedText,
                                                       TypedTextDatabase::getFonts(), TypedTextDatabase::getInstanceSize());
    }
}

void touchgfx::Texts::setTranslation(touchgfx::LanguageId id, const void* translation)
{
    languagesArray[id] = (const TranslationHeader*)translation;
}

const touchgfx::Unicode::UnicodeChar* touchgfx::Texts::getText(TypedTextId id) const
{
    return &currentLanguagePtr[currentLanguageIndices[id]];
}

