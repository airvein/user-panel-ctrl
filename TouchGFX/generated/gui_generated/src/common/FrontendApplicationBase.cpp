/*********************************************************************************/
/********** THIS FILE IS GENERATED BY TOUCHGFX DESIGNER, DO NOT MODIFY ***********/
/*********************************************************************************/
#include <new>
#include <gui_generated/common/FrontendApplicationBase.hpp>
#include <gui/common/FrontendHeap.hpp>
#include <touchgfx/transitions/NoTransition.hpp>
#include <texts/TextKeysAndLanguages.hpp>
#include <touchgfx/Texts.hpp>
#include <touchgfx/hal/HAL.hpp>
#include<platform/driver/lcd/LCD24bpp.hpp>
#include <gui/scr_airvein_screen/scr_airveinView.hpp>
#include <gui/scr_airvein_screen/scr_airveinPresenter.hpp>
#include <gui/scr_home_screen/scr_homeView.hpp>
#include <gui/scr_home_screen/scr_homePresenter.hpp>
#include <gui/scr_conf_cargo_take_out_screen/scr_conf_cargo_take_outView.hpp>
#include <gui/scr_conf_cargo_take_out_screen/scr_conf_cargo_take_outPresenter.hpp>
#include <gui/scr_conf_cargo_put_screen/scr_conf_cargo_putView.hpp>
#include <gui/scr_conf_cargo_put_screen/scr_conf_cargo_putPresenter.hpp>
#include <gui/scr_cargo_take_out_screen/scr_cargo_take_outView.hpp>
#include <gui/scr_cargo_take_out_screen/scr_cargo_take_outPresenter.hpp>
#include <gui/scr_enter_cargo_pin_screen/scr_enter_cargo_pinView.hpp>
#include <gui/scr_enter_cargo_pin_screen/scr_enter_cargo_pinPresenter.hpp>
#include <gui/scr_checking_cargo_screen/scr_checking_cargoView.hpp>
#include <gui/scr_checking_cargo_screen/scr_checking_cargoPresenter.hpp>
#include <gui/scr_service_mode_screen/scr_service_modeView.hpp>
#include <gui/scr_service_mode_screen/scr_service_modePresenter.hpp>
#include <gui/scr_hangar_breakdown_screen/scr_hangar_breakdownView.hpp>
#include <gui/scr_hangar_breakdown_screen/scr_hangar_breakdownPresenter.hpp>
#include <gui/scr_preparing_mission_screen/scr_preparing_missionView.hpp>
#include <gui/scr_preparing_mission_screen/scr_preparing_missionPresenter.hpp>
#include <gui/scr_cargo_incorrect_screen/scr_cargo_incorrectView.hpp>
#include <gui/scr_cargo_incorrect_screen/scr_cargo_incorrectPresenter.hpp>
#include <gui/scr_cargo_correct_screen/scr_cargo_correctView.hpp>
#include <gui/scr_cargo_correct_screen/scr_cargo_correctPresenter.hpp>
#include <gui/scr_cargo_pin_correct_screen/scr_cargo_pin_correctView.hpp>
#include <gui/scr_cargo_pin_correct_screen/scr_cargo_pin_correctPresenter.hpp>
#include <gui/scr_cargo_pin_incorrect_screen/scr_cargo_pin_incorrectView.hpp>
#include <gui/scr_cargo_pin_incorrect_screen/scr_cargo_pin_incorrectPresenter.hpp>
#include <gui/scr_window_closing_screen/scr_window_closingView.hpp>
#include <gui/scr_window_closing_screen/scr_window_closingPresenter.hpp>
#include <gui/scr_window_opening_screen/scr_window_openingView.hpp>
#include <gui/scr_window_opening_screen/scr_window_openingPresenter.hpp>
#include <gui/scr_thank_you_screen/scr_thank_youView.hpp>
#include <gui/scr_thank_you_screen/scr_thank_youPresenter.hpp>
#include <gui/scr_cargo_pin_check_screen/scr_cargo_pin_checkView.hpp>
#include <gui/scr_cargo_pin_check_screen/scr_cargo_pin_checkPresenter.hpp>
#include <gui/scr_cargo_pin_tout_screen/scr_cargo_pin_toutView.hpp>
#include <gui/scr_cargo_pin_tout_screen/scr_cargo_pin_toutPresenter.hpp>

using namespace touchgfx;

FrontendApplicationBase::FrontendApplicationBase(Model& m, FrontendHeap& heap)
    : touchgfx::MVPApplication(),
      transitionCallback(),
      frontendHeap(heap),
      model(m)
{
    touchgfx::HAL::getInstance()->setDisplayOrientation(touchgfx::ORIENTATION_LANDSCAPE);
    touchgfx::Texts::setLanguage(PL);
    reinterpret_cast<touchgfx::LCD24bpp&>(touchgfx::HAL::lcd()).enableTextureMapperAll();
}
