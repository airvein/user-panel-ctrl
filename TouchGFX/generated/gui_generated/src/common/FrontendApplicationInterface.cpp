#include <new>
#include <gui_generated/common/FrontendApplicationBase.hpp>
#include <gui/common/FrontendHeap.hpp>
#include <touchgfx/transitions/NoTransition.hpp>
#include <texts/TextKeysAndLanguages.hpp>
#include <touchgfx/Texts.hpp>
#include <touchgfx/hal/HAL.hpp>
#include<platform/driver/lcd/LCD24bpp.hpp>

#include <gui/scr_airvein_screen/scr_airveinView.hpp>
#include <gui/scr_airvein_screen/scr_airveinPresenter.hpp>
#include <gui/scr_home_screen/scr_homeView.hpp>
#include <gui/scr_home_screen/scr_homePresenter.hpp>
#include <gui/scr_conf_cargo_take_out_screen/scr_conf_cargo_take_outView.hpp>
#include <gui/scr_conf_cargo_take_out_screen/scr_conf_cargo_take_outPresenter.hpp>
#include <gui/scr_conf_cargo_put_screen/scr_conf_cargo_putView.hpp>
#include <gui/scr_conf_cargo_put_screen/scr_conf_cargo_putPresenter.hpp>
#include <gui/scr_cargo_take_out_screen/scr_cargo_take_outView.hpp>
#include <gui/scr_cargo_take_out_screen/scr_cargo_take_outPresenter.hpp>
#include <gui/scr_enter_cargo_pin_screen/scr_enter_cargo_pinView.hpp>
#include <gui/scr_enter_cargo_pin_screen/scr_enter_cargo_pinPresenter.hpp>
#include <gui/scr_checking_cargo_screen/scr_checking_cargoView.hpp>
#include <gui/scr_checking_cargo_screen/scr_checking_cargoPresenter.hpp>
#include <gui/scr_service_mode_screen/scr_service_modeView.hpp>
#include <gui/scr_service_mode_screen/scr_service_modePresenter.hpp>
#include <gui/scr_hangar_breakdown_screen/scr_hangar_breakdownView.hpp>
#include <gui/scr_hangar_breakdown_screen/scr_hangar_breakdownPresenter.hpp>
#include <gui/scr_preparing_mission_screen/scr_preparing_missionView.hpp>
#include <gui/scr_preparing_mission_screen/scr_preparing_missionPresenter.hpp>
#include <gui/scr_cargo_incorrect_screen/scr_cargo_incorrectView.hpp>
#include <gui/scr_cargo_incorrect_screen/scr_cargo_incorrectPresenter.hpp>
#include <gui/scr_cargo_correct_screen/scr_cargo_correctView.hpp>
#include <gui/scr_cargo_correct_screen/scr_cargo_correctPresenter.hpp>
#include <gui/scr_cargo_pin_incorrect_screen/scr_cargo_pin_incorrectView.hpp>
#include <gui/scr_cargo_pin_incorrect_screen/scr_cargo_pin_incorrectPresenter.hpp>
#include <gui/scr_cargo_pin_correct_screen/scr_cargo_pin_correctView.hpp>
#include <gui/scr_cargo_pin_correct_screen/scr_cargo_pin_correctPresenter.hpp>
#include <gui/scr_window_opening_screen/scr_window_openingView.hpp>
#include <gui/scr_window_opening_screen/scr_window_openingPresenter.hpp>
#include <gui/scr_window_closing_screen/scr_window_closingView.hpp>
#include <gui/scr_window_closing_screen/scr_window_closingPresenter.hpp>
#include <gui/scr_thank_you_screen/scr_thank_youView.hpp>
#include <gui/scr_thank_you_screen/scr_thank_youPresenter.hpp>
#include <gui/scr_cargo_pin_check_screen/scr_cargo_pin_checkView.hpp>
#include <gui/scr_cargo_pin_check_screen/scr_cargo_pin_checkPresenter.hpp>
#include <gui/scr_cargo_pin_tout_screen/scr_cargo_pin_toutView.hpp>
#include <gui/scr_cargo_pin_tout_screen/scr_cargo_pin_toutPresenter.hpp>

using namespace touchgfx;

/*
 * Screen Transition Declarations
 */

// scr_home

void FrontendApplicationBase::gotoscr_airveinScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_airveinScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_airveinScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_airveinView, scr_airveinPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_home

void FrontendApplicationBase::gotoscr_homeScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_homeScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_homeScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_homeView, scr_homePresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_conf_cargo_take_out

void FrontendApplicationBase::gotoscr_conf_cargo_take_outScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_conf_cargo_take_outScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_conf_cargo_take_outScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_conf_cargo_take_outView, scr_conf_cargo_take_outPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_conf_cargo_put

void FrontendApplicationBase::gotoscr_conf_cargo_putScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_conf_cargo_putScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_conf_cargo_putScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_conf_cargo_putView, scr_conf_cargo_putPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_cargo_take_out

void FrontendApplicationBase::gotoscr_cargo_take_outScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_cargo_take_outScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_cargo_take_outScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_cargo_take_outView, scr_cargo_take_outPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_enter_cargo_pin

void FrontendApplicationBase::gotoscr_enter_cargo_pinScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_enter_cargo_pinScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_enter_cargo_pinScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_enter_cargo_pinView, scr_enter_cargo_pinPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

void FrontendApplicationBase::updatePin(int pin_cnt)
{
    scr_enter_cargo_pinPresenter *p = static_cast<scr_enter_cargo_pinPresenter*>(currentPresenter);
    p->setPinInputCharNumbers(pin_cnt);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_checking_cargo

void FrontendApplicationBase::gotoscr_checking_cargoScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_checking_cargoScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_checking_cargoScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_checking_cargoView, scr_checking_cargoPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_service_mode

void FrontendApplicationBase::gotoscr_service_modeScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_service_modeScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_service_modeScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_service_modeView, scr_service_modePresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);   
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_hangar_breakdown

void FrontendApplicationBase::gotoscr_hangar_breakdownScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_hangar_breakdownScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_hangar_breakdownScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_hangar_breakdownView, scr_hangar_breakdownPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_preparing_mission

void FrontendApplicationBase::gotoscr_preparing_missionScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_preparing_missionScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_preparing_missionScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_preparing_missionView, scr_preparing_missionPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_cargo_incorrect

void FrontendApplicationBase::gotoscr_cargo_incorrectScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_cargo_incorrectScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_cargo_incorrectScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_cargo_incorrectView, scr_cargo_incorrectPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_cargo_correct

void FrontendApplicationBase::gotoscr_cargo_correctScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_cargo_correctScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_cargo_correctScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_cargo_correctView, scr_cargo_correctPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_cargo_pin_correct

void FrontendApplicationBase::gotoscr_cargo_pin_correctScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_cargo_pin_correctScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_cargo_pin_correctScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_cargo_pin_correctView, scr_cargo_pin_correctPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_cargo_pin_incorrect

void FrontendApplicationBase::gotoscr_cargo_pin_incorrectScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_cargo_pin_incorrectScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_cargo_pin_incorrectScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_cargo_pin_incorrectView, scr_cargo_pin_incorrectPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_window_closing

void FrontendApplicationBase::gotoscr_window_closingScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_window_closingScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_window_closingScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_window_closingView, scr_window_closingPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_window_opening

void FrontendApplicationBase::gotoscr_window_openingScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_window_openingScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_window_openingScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_window_openingView, scr_window_openingPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_thank_you

void FrontendApplicationBase::gotoscr_thank_youScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_thank_youScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_thank_youScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_thank_youView, scr_thank_youPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_cargo_pin_check

void FrontendApplicationBase::gotoscr_cargo_pin_checkScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_cargo_pin_checkScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_cargo_pin_checkScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_cargo_pin_checkView, scr_cargo_pin_checkPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
// scr_cargo_pin_tout

void FrontendApplicationBase::gotoscr_cargo_pin_toutScreenNoTransition()
{
    transitionCallback = touchgfx::Callback<FrontendApplicationBase>(this, &FrontendApplication::gotoscr_cargo_pin_toutScreenNoTransitionImpl);
    pendingScreenTransitionCallback = &transitionCallback;
}

void FrontendApplicationBase::gotoscr_cargo_pin_toutScreenNoTransitionImpl()
{
    touchgfx::makeTransition<scr_cargo_pin_toutView, scr_cargo_pin_toutPresenter, touchgfx::NoTransition, Model >(&currentScreen, &currentPresenter, frontendHeap, &currentTransition, &model);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

enum 
{   
    SCREEN_HOME,
    SCREEN_CONF_CARGO_TAKE_OUT,
    SCREEN_CONF_CARGO_PUT,
    SCREEN_CARGO_TAKE_OUT,
    SCREEN_ENTER_CARGO_PIN,
    SCREEN_CHECKING_CARGO,
    SCREEN_SERVICE_MODE,
    SCREEN_HANGAR_BREAKDOWN,
    SCREEN_PREPARING_MISSION,
    SCREEN_CARGO_INCORRECT,
    SCREEN_CARGO_CORRECT,
    SCREEN_CARGO_PIN_INCORRECT,
    SCREEN_CARGO_PIN_CORRECT,
    SCREEN_WINDOW_OPENING,
    SCREEN_WINDOW_CLOSING,
    SCREEN_THANK_YOU,
    SCREEN_CARGO_PIN_CHECK,
    SCREEN_CARGO_PIN_TOUT        
}; 

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

extern "C"
{
    void frontend_application_set_screen(uint8_t screen_enum)
    {
        FrontendApplication *app = static_cast<FrontendApplication*>
                                        (touchgfx::Application::getInstance());
        switch (screen_enum)
        {
            case SCREEN_HOME:
                app->gotoscr_homeScreenNoTransition();
                break;
            case SCREEN_CONF_CARGO_TAKE_OUT:
                app->gotoscr_conf_cargo_take_outScreenNoTransition();
                break;
            case SCREEN_CONF_CARGO_PUT:
                app->gotoscr_conf_cargo_putScreenNoTransition();
                break;
            case SCREEN_CARGO_TAKE_OUT:
                app->gotoscr_cargo_take_outScreenNoTransition();
                break;
            case SCREEN_ENTER_CARGO_PIN:
                app->gotoscr_enter_cargo_pinScreenNoTransition();
                break;
            case SCREEN_CHECKING_CARGO:
                app->gotoscr_checking_cargoScreenNoTransition();
                break;
            case SCREEN_SERVICE_MODE:
                app->gotoscr_service_modeScreenNoTransition();
                break;
            case SCREEN_HANGAR_BREAKDOWN:
                app->gotoscr_hangar_breakdownScreenNoTransition();
                break;            
            case SCREEN_PREPARING_MISSION:
                app->gotoscr_preparing_missionScreenNoTransition();
                break;
            case SCREEN_CARGO_INCORRECT:
                app->gotoscr_cargo_incorrectScreenNoTransition();
                break;
            case SCREEN_CARGO_CORRECT:
                app->gotoscr_cargo_correctScreenNoTransition();
                break;
            case SCREEN_CARGO_PIN_INCORRECT:
                app->gotoscr_cargo_pin_incorrectScreenNoTransition();
                break;
            case SCREEN_CARGO_PIN_CORRECT:
                app->gotoscr_cargo_pin_correctScreenNoTransition();
                break;            
            case SCREEN_WINDOW_OPENING:
                app->gotoscr_window_openingScreenNoTransition();
                break;
            case SCREEN_WINDOW_CLOSING:
                app->gotoscr_window_closingScreenNoTransition();
                break;        
            case SCREEN_THANK_YOU:
                app->gotoscr_thank_youScreenNoTransition();
                break;        
            case SCREEN_CARGO_PIN_CHECK:
                app->gotoscr_cargo_pin_checkScreenNoTransition();
                break;        
            case SCREEN_CARGO_PIN_TOUT:
                app->gotoscr_cargo_pin_toutScreenNoTransition();
                break;       

        default:
            break;
        }
    }

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|

    void update_pin(int pin_cnt)
    {
        FrontendApplication *app = static_cast<FrontendApplication*>
                                        (touchgfx::Application::getInstance());
        
        app->updatePin(pin_cnt);
    }
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -|
