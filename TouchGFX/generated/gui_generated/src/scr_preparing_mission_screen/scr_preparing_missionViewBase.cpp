/*********************************************************************************/
/********** THIS FILE IS GENERATED BY TOUCHGFX DESIGNER, DO NOT MODIFY ***********/
/*********************************************************************************/
#include <gui_generated/scr_preparing_mission_screen/scr_preparing_missionViewBase.hpp>
#include "BitmapDatabase.hpp"
#include <texts/TextKeysAndLanguages.hpp>
#include <touchgfx/Color.hpp>

scr_preparing_missionViewBase::scr_preparing_missionViewBase()
{

    image1_1.setXY(0, 0);
    image1_1.setBitmap(touchgfx::Bitmap(BITMAP_TEXT_VIEW_ID));

    textArea1.setXY(9, 88);
    textArea1.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
    textArea1.setLinespacing(0);
    textArea1.setTypedText(touchgfx::TypedText(T_SINGLEUSEID9));

    add(image1_1);
    add(textArea1);
}

void scr_preparing_missionViewBase::setupScreen()
{

}
