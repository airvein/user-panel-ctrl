/*********************************************************************************/
/********** THIS FILE IS GENERATED BY TOUCHGFX DESIGNER, DO NOT MODIFY ***********/
/*********************************************************************************/
#ifndef SCR_WINDOW_OPENINGVIEWBASE_HPP
#define SCR_WINDOW_OPENINGVIEWBASE_HPP

#include <gui/common/FrontendApplication.hpp>
#include <mvp/View.hpp>
#include <gui/scr_window_opening_screen/scr_window_openingPresenter.hpp>
#include <touchgfx/widgets/Image.hpp>
#include <touchgfx/widgets/TextArea.hpp>

class scr_window_openingViewBase : public touchgfx::View<scr_window_openingPresenter>
{
public:
    scr_window_openingViewBase();
    virtual ~scr_window_openingViewBase() {}
    virtual void setupScreen();

protected:
    FrontendApplication& application() {
        return *static_cast<FrontendApplication*>(touchgfx::Application::getInstance());
    }

    /*
     * Member Declarations
     */
    touchgfx::Image image1;
    touchgfx::TextArea textArea1;

private:

};

#endif // SCR_WINDOW_OPENINGVIEWBASE_HPP
