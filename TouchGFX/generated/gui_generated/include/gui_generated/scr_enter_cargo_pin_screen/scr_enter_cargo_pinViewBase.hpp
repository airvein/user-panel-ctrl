/*********************************************************************************/
/********** THIS FILE IS GENERATED BY TOUCHGFX DESIGNER, DO NOT MODIFY ***********/
/*********************************************************************************/
#ifndef SCR_ENTER_CARGO_PINVIEWBASE_HPP
#define SCR_ENTER_CARGO_PINVIEWBASE_HPP

#include <gui/common/FrontendApplication.hpp>
#include <mvp/View.hpp>
#include <gui/scr_enter_cargo_pin_screen/scr_enter_cargo_pinPresenter.hpp>
#include <touchgfx/widgets/Image.hpp>
#include <touchgfx/widgets/TextAreaWithWildcard.hpp>
#include <touchgfx/widgets/TextArea.hpp>

class scr_enter_cargo_pinViewBase : public touchgfx::View<scr_enter_cargo_pinPresenter>
{
public:
    scr_enter_cargo_pinViewBase();
    virtual ~scr_enter_cargo_pinViewBase() {}
    virtual void setupScreen();

protected:
    FrontendApplication& application() {
        return *static_cast<FrontendApplication*>(touchgfx::Application::getInstance());
    }

    /*
     * Member Declarations
     */
    touchgfx::Image image1;
    touchgfx::TextAreaWithOneWildcard auth_pin;
    touchgfx::TextArea textArea1;
    touchgfx::TextArea textArea1_1;

    /*
     * Wildcard Buffers
     */
    static const uint16_t AUTH_PIN_SIZE = 12;
    touchgfx::Unicode::UnicodeChar auth_pinBuffer[AUTH_PIN_SIZE];

private:

};

#endif // SCR_ENTER_CARGO_PINVIEWBASE_HPP
