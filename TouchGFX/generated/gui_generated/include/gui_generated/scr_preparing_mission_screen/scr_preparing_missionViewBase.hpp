/*********************************************************************************/
/********** THIS FILE IS GENERATED BY TOUCHGFX DESIGNER, DO NOT MODIFY ***********/
/*********************************************************************************/
#ifndef SCR_PREPARING_MISSIONVIEWBASE_HPP
#define SCR_PREPARING_MISSIONVIEWBASE_HPP

#include <gui/common/FrontendApplication.hpp>
#include <mvp/View.hpp>
#include <gui/scr_preparing_mission_screen/scr_preparing_missionPresenter.hpp>
#include <touchgfx/widgets/Image.hpp>
#include <touchgfx/widgets/TextArea.hpp>

class scr_preparing_missionViewBase : public touchgfx::View<scr_preparing_missionPresenter>
{
public:
    scr_preparing_missionViewBase();
    virtual ~scr_preparing_missionViewBase() {}
    virtual void setupScreen();

protected:
    FrontendApplication& application() {
        return *static_cast<FrontendApplication*>(touchgfx::Application::getInstance());
    }

    /*
     * Member Declarations
     */
    touchgfx::Image image1_1;
    touchgfx::TextArea textArea1;

private:

};

#endif // SCR_PREPARING_MISSIONVIEWBASE_HPP
