/*********************************************************************************/
/********** THIS FILE IS GENERATED BY TOUCHGFX DESIGNER, DO NOT MODIFY ***********/
/*********************************************************************************/
#ifndef SCR_CARGO_INCORRECTVIEWBASE_HPP
#define SCR_CARGO_INCORRECTVIEWBASE_HPP

#include <gui/common/FrontendApplication.hpp>
#include <mvp/View.hpp>
#include <gui/scr_cargo_incorrect_screen/scr_cargo_incorrectPresenter.hpp>
#include <touchgfx/widgets/Image.hpp>
#include <touchgfx/widgets/TextArea.hpp>

class scr_cargo_incorrectViewBase : public touchgfx::View<scr_cargo_incorrectPresenter>
{
public:
    scr_cargo_incorrectViewBase();
    virtual ~scr_cargo_incorrectViewBase() {}
    virtual void setupScreen();

protected:
    FrontendApplication& application() {
        return *static_cast<FrontendApplication*>(touchgfx::Application::getInstance());
    }

    /*
     * Member Declarations
     */
    touchgfx::Image image1_1;
    touchgfx::TextArea textArea1;

private:

};

#endif // SCR_CARGO_INCORRECTVIEWBASE_HPP
